// swift-tools-version:5.3
import PackageDescription
let package = Package(
    name: "SharedOkoCrm",
    platforms: [
        .iOS(.v11)
    ],
    products: [
        .library(
            name: "SharedOkoCrm",
            targets: ["SharedOkoCrm"])
    ],
    targets: [
        .binaryTarget(
            name: "SharedOkoCrm",
            path: "SharedOkoCrm.xcframework")
    ])
