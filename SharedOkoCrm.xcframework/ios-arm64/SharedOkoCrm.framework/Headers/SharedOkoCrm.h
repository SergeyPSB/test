#import <Foundation/NSArray.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSError.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSSet.h>
#import <Foundation/NSString.h>
#import <Foundation/NSValue.h>

@class SOCMvvm_coreViewModel, SOCUser, SOCStatusRequest<__covariant T>, SOCMvvm_livedataMutableLiveData<T>, SOCNote, SOCKotlinPair<__covariant A, __covariant B>, SOCMultiplatformFile, SOCDialog, SOCField, SOCKotlinx_serialization_jsonJson, SOCStorage, SOCKtor_httpHeadersBuilder, SOCStatusRequestError<T>, SOCKotlinThrowable, SOCKtor_httpCookie, SOCKtor_client_coreHttpStatement, SOCKotlinx_serialization_jsonJsonElement, SOCBaseNetworkSource, SOCKtor_client_coreHttpClient, SOCData<T>, SOCCityCompanion, SOCCity, SOCSite, SOCMessage, SOCDialogCompanion, SOCEmailCompanion, SOCEmail, SOCErrorData, SOCIntegrationCompanion, SOCIntegration, SOCMessenger, SOCMessageCompanion, SOCShortIntegration, SOCMessengerCompanion, SOCEnumCompanion, SOCEnum, SOCStage, SOCPipelineCompanion, SOCPipeline, SOCShortIntegrationCompanion, SOCSiteCompanion, SOCStageCompanion, SOCStatusRequestLoading<T>, SOCStatusRequestSuccess<T>, SOCUserCompanion, SOCNoteCompanion, SOCDataCompanion, SOCKotlinArray<T>, SOCKotlinByteArray, SOCKotlinx_serialization_jsonJsonContentPolymorphicSerializer<T>, SOCEnumSerializer, SOCVarType, SOCFieldCompanion, SOCInfoFieldCompanion, SOCInfoField, SOCVarTypeCompanion, SOCNetworkSettings, SOCPreferences, SOCUtils, SOCMainSDK, SOCKoin_coreKoin, SOCdiMain, SOCdiRepositories, SOCAuthRepository, SOCNoteRepository, SOCUnsortedRepository, SOCKoin_coreModule, SOCKotlinCancellationException, SOCKotlinUnit, SOCMvvm_livedataLiveData<T>, SOCKotlinx_serialization_coreSerializersModule, SOCKotlinx_serialization_jsonJsonConfiguration, SOCKotlinx_serialization_jsonJsonDefault, SOCKotlinException, SOCKotlinRuntimeException, SOCKotlinIllegalStateException, SOCKtor_utilsStringValuesBuilder, SOCKtor_httpCookieEncoding, SOCKtor_utilsGMTDate, SOCKtor_client_coreHttpRequestBuilder, SOCKtor_client_coreHttpResponse, SOCKotlinx_serialization_jsonJsonElementCompanion, SOCKtor_client_coreHttpClientEngineConfig, SOCKtor_client_coreHttpClientConfig<T>, SOCKtor_client_coreHttpClientCall, SOCKotlinx_coroutines_coreCoroutineDispatcher, SOCKtor_client_coreHttpReceivePipeline, SOCKtor_client_coreHttpRequestPipeline, SOCKtor_client_coreHttpResponsePipeline, SOCKtor_client_coreHttpSendPipeline, SOCKotlinByteIterator, SOCKotlinx_serialization_coreSerialKind, SOCKotlinNothing, SOCKoin_coreScope, SOCKoin_coreParametersHolder, SOCKotlinLazyThreadSafetyMode, SOCKoin_coreLogger, SOCKoin_coreInstanceRegistry, SOCKoin_corePropertyRegistry, SOCKoin_coreScopeRegistry, SOCKoin_coreInstanceFactory<T>, SOCKoin_coreScopeDSL, SOCKoin_coreSingleInstanceFactory<T>, SOCKotlinEnumCompanion, SOCKotlinEnum<E>, SOCKtor_utilsGMTDateCompanion, SOCKtor_utilsWeekDay, SOCKtor_utilsMonth, SOCKtor_client_coreHttpRequestBuilderCompanion, SOCKtor_client_coreHttpRequestData, SOCKtor_httpURLBuilder, SOCKtor_httpHttpMethod, SOCKtor_httpHttpStatusCode, SOCKtor_httpHttpProtocolVersion, SOCKtor_client_coreHttpResponseData, SOCKtor_client_coreProxyConfig, SOCKtor_client_coreHttpClientCallCompanion, SOCKtor_client_coreTypeInfo, SOCKtor_utilsAttributeKey<T>, SOCKotlinAbstractCoroutineContextElement, SOCKotlinx_coroutines_coreCoroutineDispatcherKey, SOCKtor_utilsPipelinePhase, SOCKtor_utilsPipeline<TSubject, TContext>, SOCKtor_client_coreHttpReceivePipelinePhases, SOCKtor_client_coreHttpRequestPipelinePhases, SOCKtor_client_coreHttpResponsePipelinePhases, SOCKtor_client_coreHttpResponseContainer, SOCKtor_client_coreHttpSendPipelinePhases, SOCKoin_coreParametersHolderCompanion, SOCKoin_coreLevel, SOCKoin_coreScopeRegistryCompanion, SOCKoin_coreBeanDefinition<T>, SOCKoin_coreInstanceFactoryCompanion, SOCKoin_coreInstanceContext, SOCKotlinx_coroutines_coreAtomicDesc, SOCKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp, SOCKtor_utilsWeekDayCompanion, SOCKtor_utilsMonthCompanion, SOCKtor_httpUrl, SOCKtor_httpOutgoingContent, SOCKtor_httpURLProtocol, SOCKtor_httpParametersBuilder, SOCKtor_httpURLBuilderCompanion, SOCKtor_httpHttpMethodCompanion, SOCKtor_ioMemory, SOCKtor_ioIoBuffer, SOCKtor_ioByteReadPacket, SOCKtor_ioByteOrder, SOCKtor_httpHttpStatusCodeCompanion, SOCKtor_httpHttpProtocolVersionCompanion, SOCKotlinAbstractCoroutineContextKey<B, E>, SOCKoin_coreKind, SOCKoin_coreCallbacks<T>, SOCKotlinx_coroutines_coreAtomicOp<__contravariant T>, SOCKotlinx_coroutines_coreOpDescriptor, SOCKotlinx_coroutines_coreLockFreeLinkedListNode, SOCKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc, SOCKtor_httpUrlCompanion, SOCKtor_httpContentType, SOCKtor_httpURLProtocolCompanion, SOCKtor_httpUrlEncodingOption, SOCKtor_ioMemoryCompanion, SOCKtor_ioBufferCompanion, SOCKtor_ioBuffer, SOCKtor_ioChunkBuffer, SOCKtor_ioChunkBufferCompanion, SOCKotlinCharArray, SOCKtor_ioIoBufferCompanion, SOCKtor_ioAbstractInputCompanion, SOCKtor_ioAbstractInput, SOCKtor_ioByteReadPacketBaseCompanion, SOCKtor_ioByteReadPacketBase, SOCKtor_ioByteReadPacketPlatformBase, SOCKtor_ioByteReadPacketCompanion, SOCKtor_ioByteOrderCompanion, SOCKotlinKTypeProjection, SOCKotlinx_coroutines_coreLockFreeLinkedListNodeAddLastDesc<T>, SOCKotlinx_coroutines_coreLockFreeLinkedListNodeRemoveFirstDesc<T>, SOCKtor_httpHeaderValueParam, SOCKtor_httpHeaderValueWithParametersCompanion, SOCKtor_httpHeaderValueWithParameters, SOCKtor_httpContentTypeCompanion, SOCKotlinCharIterator, SOCKotlinKVariance, SOCKotlinKTypeProjectionCompanion;

@protocol SOCKotlinx_coroutines_coreCoroutineScope, SOCKotlinx_coroutines_coreJob, SOCDownloadListener, SOCIAuthNetworkSource, SOCINoteNetworkSource, SOCIUnsortedNetworkSource, SOCKotlinx_serialization_coreKSerializer, SOCKotlinx_serialization_coreEncoder, SOCKotlinx_serialization_coreSerialDescriptor, SOCKotlinx_serialization_coreSerializationStrategy, SOCKotlinx_serialization_coreDecoder, SOCKotlinx_serialization_coreDeserializationStrategy, SOCKotlinKClass, SOCKtor_client_coreHttpClientEngineFactory, SOCKoin_coreKoinComponent, SOCKotlinCoroutineContext, SOCKotlinx_coroutines_coreChildHandle, SOCKotlinx_coroutines_coreChildJob, SOCKotlinx_coroutines_coreDisposableHandle, SOCKotlinSequence, SOCKotlinx_coroutines_coreSelectClause0, SOCKotlinCoroutineContextKey, SOCKotlinCoroutineContextElement, SOCKotlinx_serialization_coreSerialFormat, SOCKotlinx_serialization_coreStringFormat, SOCKtor_utilsStringValues, SOCKotlinMapEntry, SOCKtor_httpHeaders, SOCKotlinSuspendFunction1, SOCKtor_ioCloseable, SOCKtor_client_coreHttpClientEngine, SOCKtor_client_coreHttpClientEngineCapability, SOCKtor_utilsAttributes, SOCKotlinIterator, SOCKotlinx_serialization_coreCompositeEncoder, SOCKotlinAnnotation, SOCKotlinx_serialization_coreCompositeDecoder, SOCKotlinKDeclarationContainer, SOCKotlinKAnnotatedElement, SOCKotlinKClassifier, SOCKoin_coreKoinScopeComponent, SOCKoin_coreQualifier, SOCKotlinLazy, SOCKotlinx_coroutines_coreParentJob, SOCKotlinx_coroutines_coreSelectInstance, SOCKotlinSuspendFunction0, SOCKotlinx_serialization_coreSerializersModuleCollector, SOCKotlinComparable, SOCKtor_httpHttpMessageBuilder, SOCKtor_httpHttpMessage, SOCKtor_ioByteReadChannel, SOCKotlinFunction, SOCKtor_client_coreHttpClientFeature, SOCKtor_utilsTypeInfo, SOCKtor_client_coreHttpRequest, SOCKotlinContinuation, SOCKotlinContinuationInterceptor, SOCKotlinx_coroutines_coreRunnable, SOCKotlinSuspendFunction2, SOCKoin_coreScopeCallback, SOCKtor_ioReadSession, SOCKotlinAppendable, SOCKotlinKType, SOCKtor_httpParameters, SOCKtor_ioObjectPool, SOCKtor_ioInput, SOCKtor_ioOutput;

NS_ASSUME_NONNULL_BEGIN
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-warning-option"
#pragma clang diagnostic ignored "-Wincompatible-property-type"
#pragma clang diagnostic ignored "-Wnullability"

#pragma push_macro("_Nullable_result")
#if !__has_feature(nullability_nullable_result)
#undef _Nullable_result
#define _Nullable_result _Nullable
#endif

__attribute__((swift_name("KotlinBase")))
@interface SOCBase : NSObject
- (instancetype)init __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (void)initialize __attribute__((objc_requires_super));
@end;

@interface SOCBase (SOCBaseCopying) <NSCopying>
@end;

__attribute__((swift_name("KotlinMutableSet")))
@interface SOCMutableSet<ObjectType> : NSMutableSet<ObjectType>
@end;

__attribute__((swift_name("KotlinMutableDictionary")))
@interface SOCMutableDictionary<KeyType, ObjectType> : NSMutableDictionary<KeyType, ObjectType>
@end;

@interface NSError (NSErrorSOCKotlinException)
@property (readonly) id _Nullable kotlinException;
@end;

__attribute__((swift_name("KotlinNumber")))
@interface SOCNumber : NSNumber
- (instancetype)initWithChar:(char)value __attribute__((unavailable));
- (instancetype)initWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
- (instancetype)initWithShort:(short)value __attribute__((unavailable));
- (instancetype)initWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
- (instancetype)initWithInt:(int)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
- (instancetype)initWithLong:(long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
- (instancetype)initWithLongLong:(long long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
- (instancetype)initWithFloat:(float)value __attribute__((unavailable));
- (instancetype)initWithDouble:(double)value __attribute__((unavailable));
- (instancetype)initWithBool:(BOOL)value __attribute__((unavailable));
- (instancetype)initWithInteger:(NSInteger)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
+ (instancetype)numberWithChar:(char)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
+ (instancetype)numberWithShort:(short)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
+ (instancetype)numberWithInt:(int)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
+ (instancetype)numberWithLong:(long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
+ (instancetype)numberWithLongLong:(long long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
+ (instancetype)numberWithFloat:(float)value __attribute__((unavailable));
+ (instancetype)numberWithDouble:(double)value __attribute__((unavailable));
+ (instancetype)numberWithBool:(BOOL)value __attribute__((unavailable));
+ (instancetype)numberWithInteger:(NSInteger)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
@end;

__attribute__((swift_name("KotlinByte")))
@interface SOCByte : SOCNumber
- (instancetype)initWithChar:(char)value;
+ (instancetype)numberWithChar:(char)value;
@end;

__attribute__((swift_name("KotlinUByte")))
@interface SOCUByte : SOCNumber
- (instancetype)initWithUnsignedChar:(unsigned char)value;
+ (instancetype)numberWithUnsignedChar:(unsigned char)value;
@end;

__attribute__((swift_name("KotlinShort")))
@interface SOCShort : SOCNumber
- (instancetype)initWithShort:(short)value;
+ (instancetype)numberWithShort:(short)value;
@end;

__attribute__((swift_name("KotlinUShort")))
@interface SOCUShort : SOCNumber
- (instancetype)initWithUnsignedShort:(unsigned short)value;
+ (instancetype)numberWithUnsignedShort:(unsigned short)value;
@end;

__attribute__((swift_name("KotlinInt")))
@interface SOCInt : SOCNumber
- (instancetype)initWithInt:(int)value;
+ (instancetype)numberWithInt:(int)value;
@end;

__attribute__((swift_name("KotlinUInt")))
@interface SOCUInt : SOCNumber
- (instancetype)initWithUnsignedInt:(unsigned int)value;
+ (instancetype)numberWithUnsignedInt:(unsigned int)value;
@end;

__attribute__((swift_name("KotlinLong")))
@interface SOCLong : SOCNumber
- (instancetype)initWithLongLong:(long long)value;
+ (instancetype)numberWithLongLong:(long long)value;
@end;

__attribute__((swift_name("KotlinULong")))
@interface SOCULong : SOCNumber
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value;
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value;
@end;

__attribute__((swift_name("KotlinFloat")))
@interface SOCFloat : SOCNumber
- (instancetype)initWithFloat:(float)value;
+ (instancetype)numberWithFloat:(float)value;
@end;

__attribute__((swift_name("KotlinDouble")))
@interface SOCDouble : SOCNumber
- (instancetype)initWithDouble:(double)value;
+ (instancetype)numberWithDouble:(double)value;
@end;

__attribute__((swift_name("KotlinBoolean")))
@interface SOCBoolean : SOCNumber
- (instancetype)initWithBool:(BOOL)value;
+ (instancetype)numberWithBool:(BOOL)value;
@end;

__attribute__((swift_name("Mvvm_coreViewModel")))
@interface SOCMvvm_coreViewModel : SOCBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)onCleared __attribute__((swift_name("onCleared()")));
@property (readonly) id<SOCKotlinx_coroutines_coreCoroutineScope> viewModelScope __attribute__((swift_name("viewModelScope")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuthViewModel")))
@interface SOCAuthViewModel : SOCMvvm_coreViewModel
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (id<SOCKotlinx_coroutines_coreJob>)authEmail:(NSString *)email domain:(NSString *)domain password:(NSString *)password __attribute__((swift_name("auth(email:domain:password:)")));
- (id<SOCKotlinx_coroutines_coreJob>)fetchToken __attribute__((swift_name("fetchToken()")));
- (id<SOCKotlinx_coroutines_coreJob>)logout __attribute__((swift_name("logout()")));
- (void)onCleared __attribute__((swift_name("onCleared()")));
- (id<SOCKotlinx_coroutines_coreJob>)sendFcmTokenToken:(NSString *)token __attribute__((swift_name("sendFcmToken(token:)")));
@property SOCMvvm_livedataMutableLiveData<SOCStatusRequest<SOCUser *> *> *authLiveData __attribute__((swift_name("authLiveData")));
@property SOCMvvm_livedataMutableLiveData<SOCStatusRequest<SOCBoolean *> *> *isLogged __attribute__((swift_name("isLogged")));
@property SOCMvvm_livedataMutableLiveData<NSString *> *tokenLiveData __attribute__((swift_name("tokenLiveData")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("NoteViewModel")))
@interface SOCNoteViewModel : SOCMvvm_coreViewModel
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (id<SOCKotlinx_coroutines_coreJob>)getEmailEmailId:(int32_t)emailId __attribute__((swift_name("getEmail(emailId:)")));
- (void)onCleared __attribute__((swift_name("onCleared()")));
@property (readonly) SOCMvvm_livedataMutableLiveData<SOCStatusRequest<SOCNote *> *> *emailLiveData __attribute__((swift_name("emailLiveData")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UnsortedViewModel")))
@interface SOCUnsortedViewModel : SOCMvvm_coreViewModel
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (id<SOCKotlinx_coroutines_coreJob>)acceptDialogDialogId:(int32_t)dialogId __attribute__((swift_name("acceptDialog(dialogId:)")));
- (id<SOCKotlinx_coroutines_coreJob>)changeStatusValue:(int32_t)value __attribute__((swift_name("changeStatus(value:)")));
- (void)clear __attribute__((swift_name("clear()")));
- (void)clearDialogs __attribute__((swift_name("clearDialogs()")));
- (id<SOCKotlinx_coroutines_coreJob>)createLeadDialogId:(int32_t)dialogId dialogName:(NSString *)dialogName listParameters:(NSArray<SOCKotlinPair<NSString *, NSString *> *> *)listParameters __attribute__((swift_name("createLead(dialogId:dialogName:listParameters:)")));
- (id<SOCKotlinx_coroutines_coreJob>)downloadFileFileName:(NSString *)fileName fileUrl:(NSString *)fileUrl downloadListener:(id<SOCDownloadListener> _Nullable)downloadListener __attribute__((swift_name("downloadFile(fileName:fileUrl:downloadListener:)")));
- (id<SOCKotlinx_coroutines_coreJob>)getContactInfoDialogId:(int32_t)dialogId __attribute__((swift_name("getContactInfo(dialogId:)")));
- (id<SOCKotlinx_coroutines_coreJob>)getDialogDialogId:(int32_t)dialogId __attribute__((swift_name("getDialog(dialogId:)")));
- (id<SOCKotlinx_coroutines_coreJob>)getDialogsPage:(int32_t)page __attribute__((swift_name("getDialogs(page:)")));
- (id<SOCKotlinx_coroutines_coreJob>)getFields __attribute__((swift_name("getFields()")));
- (id<SOCKotlinx_coroutines_coreJob>)getUsers __attribute__((swift_name("getUsers()")));
- (void)onCleared __attribute__((swift_name("onCleared()")));
- (id<SOCKotlinx_coroutines_coreJob>)removeDialogDialogId:(int32_t)dialogId __attribute__((swift_name("removeDialog(dialogId:)")));
- (void)removeFileFileName:(NSString *)fileName __attribute__((swift_name("removeFile(fileName:)")));
- (id<SOCKotlinx_coroutines_coreJob>)sendMessageDialogId:(int32_t)dialogId message:(NSString *)message integrationId:(int32_t)integrationId __attribute__((swift_name("sendMessage(dialogId:message:integrationId:)")));
- (void)setFilesList:(NSArray<SOCMultiplatformFile *> *)list __attribute__((swift_name("setFiles(list:)")));
- (void)socketGetDialogJsonStr:(NSString *)jsonStr __attribute__((swift_name("socketGetDialog(jsonStr:)")));
- (void)socketGetMessageDialogId:(int32_t)dialogId jsonStr:(NSString *)jsonStr __attribute__((swift_name("socketGetMessage(dialogId:jsonStr:)")));
- (id<SOCKotlinx_coroutines_coreJob>)transferDialogDialogId:(int32_t)dialogId userId:(int32_t)userId __attribute__((swift_name("transferDialog(dialogId:userId:)")));
- (void)notifyUpdated:(SOCMvvm_livedataMutableLiveData<id> *)receiver __attribute__((swift_name("notifyUpdated(_:)")));
@property SOCInt * _Nullable activeDialogId __attribute__((swift_name("activeDialogId")));
@property int32_t currentPage __attribute__((swift_name("currentPage")));
@property SOCMvvm_livedataMutableLiveData<SOCStatusRequest<NSArray<SOCDialog *> *> *> *dialogsLiveData __attribute__((swift_name("dialogsLiveData")));
@property SOCMvvm_livedataMutableLiveData<SOCStatusRequest<SOCMultiplatformFile *> *> *downloadFileLiveData __attribute__((swift_name("downloadFileLiveData")));
@property SOCMvvm_livedataMutableLiveData<SOCStatusRequest<id> *> *errorLiveData __attribute__((swift_name("errorLiveData")));
@property SOCMvvm_livedataMutableLiveData<SOCStatusRequest<NSArray<SOCField *> *> *> *fieldsLiveData __attribute__((swift_name("fieldsLiveData")));
@property SOCMvvm_livedataMutableLiveData<NSArray<SOCMultiplatformFile *> *> *filesLiveData __attribute__((swift_name("filesLiveData")));
@property SOCMvvm_livedataMutableLiveData<SOCStatusRequest<SOCBoolean *> *> *isChangeStatus __attribute__((swift_name("isChangeStatus")));
@property SOCMvvm_livedataMutableLiveData<SOCStatusRequest<SOCBoolean *> *> *isDialogRemoved __attribute__((swift_name("isDialogRemoved")));
@property SOCMvvm_livedataMutableLiveData<SOCStatusRequest<SOCBoolean *> *> *isLeadCreatedLiveData __attribute__((swift_name("isLeadCreatedLiveData")));
@property SOCMvvm_livedataMutableLiveData<SOCStatusRequest<SOCBoolean *> *> *isTransferDialogSuccessLiveData __attribute__((swift_name("isTransferDialogSuccessLiveData")));
@property int32_t lastPage __attribute__((swift_name("lastPage")));
@property SOCMvvm_livedataMutableLiveData<SOCStatusRequest<SOCDialog *> *> *openDialogLiveData __attribute__((swift_name("openDialogLiveData")));
@property SOCMvvm_livedataMutableLiveData<SOCStatusRequest<NSArray<SOCUser *> *> *> *usersForSelectLiveData __attribute__((swift_name("usersForSelectLiveData")));
@end;

__attribute__((swift_name("BaseNetworkSource")))
@interface SOCBaseNetworkSource : SOCBase
- (instancetype)initWithJson:(SOCKotlinx_serialization_jsonJson *)json storage:(SOCStorage *)storage __attribute__((swift_name("init(json:storage:)"))) __attribute__((objc_designated_initializer));
- (void)cookiesHeadersBuilder:(SOCKtor_httpHeadersBuilder *)builder __attribute__((swift_name("cookiesHeaders(builder:)")));
- (SOCStatusRequestError<id> *)errorHandlerE:(SOCKotlinThrowable *)e __attribute__((swift_name("errorHandler(e:)")));
- (NSArray<SOCKtor_httpCookie *> *)getCookies __attribute__((swift_name("getCookies()")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)makeRequestStatement:(SOCKtor_client_coreHttpStatement *)statement completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("makeRequest(statement:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)makeRequestStatement:(SOCKtor_client_coreHttpStatement *)statement rawJson:(BOOL)rawJson serialize:(id _Nullable (^ _Nullable)(SOCKotlinx_serialization_jsonJsonElement *))serialize completionHandler:(void (^)(SOCStatusRequest<id> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("makeRequest(statement:rawJson:serialize:completionHandler:)")));
@end;

__attribute__((swift_name("IAuthNetworkSource")))
@protocol SOCIAuthNetworkSource
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getTokenWithCompletionHandler:(void (^)(SOCStatusRequest<NSString *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getToken(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)loginEmail:(NSString *)email domain:(NSString *)domain password:(NSString *)password completionHandler:(void (^)(SOCStatusRequest<SOCUser *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("login(email:domain:password:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)logoutWithCompletionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("logout(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)sendFCMTokenToken:(NSString *)token completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("sendFCMToken(token:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuthNetworkSource")))
@interface SOCAuthNetworkSource : SOCBaseNetworkSource <SOCIAuthNetworkSource>
- (instancetype)initWithJson:(SOCKotlinx_serialization_jsonJson *)json httpClient:(SOCKtor_client_coreHttpClient *)httpClient storage:(SOCStorage *)storage __attribute__((swift_name("init(json:httpClient:storage:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithJson:(SOCKotlinx_serialization_jsonJson *)json storage:(SOCStorage *)storage __attribute__((swift_name("init(json:storage:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getTokenWithCompletionHandler:(void (^)(SOCStatusRequest<NSString *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getToken(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)loginEmail:(NSString *)email domain:(NSString *)domain password:(NSString *)password completionHandler:(void (^)(SOCStatusRequest<SOCUser *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("login(email:domain:password:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)logoutWithCompletionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("logout(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)sendFCMTokenToken:(NSString *)token completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("sendFCMToken(token:completionHandler:)")));
@property (readonly) SOCKtor_client_coreHttpClient *httpClient __attribute__((swift_name("httpClient")));
@property (readonly) SOCKotlinx_serialization_jsonJson *json __attribute__((swift_name("json")));
@property (readonly) SOCStorage *storage __attribute__((swift_name("storage")));
@end;

__attribute__((swift_name("INoteNetworkSource")))
@protocol SOCINoteNetworkSource
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)fetchEmailEmailId:(int32_t)emailId completionHandler:(void (^)(SOCStatusRequest<SOCNote *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("fetchEmail(emailId:completionHandler:)")));
@end;

__attribute__((swift_name("IUnsortedNetworkSource")))
@protocol SOCIUnsortedNetworkSource
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)changeStatusValue:(int32_t)value completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("changeStatus(value:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)createLeadParameters:(NSArray<SOCKotlinPair<NSString *, NSString *> *> *)parameters completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("createLead(parameters:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)downloadFileFileName:(NSString *)fileName fileUrl:(NSString *)fileUrl downloadListener:(id<SOCDownloadListener> _Nullable)downloadListener completionHandler:(void (^)(SOCStatusRequest<SOCMultiplatformFile *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("downloadFile(fileName:fileUrl:downloadListener:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)fetchAllUsersWithCompletionHandler:(void (^)(SOCStatusRequest<NSArray<SOCUser *> *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("fetchAllUsers(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)fetchDialogDialogId:(int32_t)dialogId completionHandler:(void (^)(SOCStatusRequest<SOCDialog *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("fetchDialog(dialogId:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)fetchDialogsPage:(int32_t)page completionHandler:(void (^)(SOCStatusRequest<SOCData<SOCDialog *> *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("fetchDialogs(page:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getContactDialogId:(int32_t)dialogId completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getContact(dialogId:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getFieldsWithCompletionHandler:(void (^)(SOCStatusRequest<NSArray<SOCField *> *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getFields(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)removeDialogDialogId:(int32_t)dialogId completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("removeDialog(dialogId:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)sendMessageDialogId:(int32_t)dialogId message:(NSString *)message files:(NSArray<SOCMultiplatformFile *> * _Nullable)files integrationId:(int32_t)integrationId completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("sendMessage(dialogId:message:files:integrationId:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)takeDialogDialogId:(int32_t)dialogId completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("takeDialog(dialogId:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)transferDialogDialogId:(int32_t)dialogId userId:(int32_t)userId completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("transferDialog(dialogId:userId:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("NoteNetworkSource")))
@interface SOCNoteNetworkSource : SOCBaseNetworkSource <SOCINoteNetworkSource>
- (instancetype)initWithJson:(SOCKotlinx_serialization_jsonJson *)json httpClient:(SOCKtor_client_coreHttpClient *)httpClient storage:(SOCStorage *)storage __attribute__((swift_name("init(json:httpClient:storage:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithJson:(SOCKotlinx_serialization_jsonJson *)json storage:(SOCStorage *)storage __attribute__((swift_name("init(json:storage:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)fetchEmailEmailId:(int32_t)emailId completionHandler:(void (^)(SOCStatusRequest<SOCNote *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("fetchEmail(emailId:completionHandler:)")));
@property (readonly) SOCKtor_client_coreHttpClient *httpClient __attribute__((swift_name("httpClient")));
@property (readonly) SOCKotlinx_serialization_jsonJson *json __attribute__((swift_name("json")));
@property (readonly) SOCStorage *storage __attribute__((swift_name("storage")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UnsortedNetworkSource")))
@interface SOCUnsortedNetworkSource : SOCBaseNetworkSource <SOCIUnsortedNetworkSource>
- (instancetype)initWithJson:(SOCKotlinx_serialization_jsonJson *)json httpClient:(SOCKtor_client_coreHttpClient *)httpClient storage:(SOCStorage *)storage __attribute__((swift_name("init(json:httpClient:storage:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithJson:(SOCKotlinx_serialization_jsonJson *)json storage:(SOCStorage *)storage __attribute__((swift_name("init(json:storage:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)changeStatusValue:(int32_t)value completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("changeStatus(value:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)createLeadParameters:(NSArray<SOCKotlinPair<NSString *, NSString *> *> *)parameters completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("createLead(parameters:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)downloadFileFileName:(NSString *)fileName fileUrl:(NSString *)fileUrl downloadListener:(id<SOCDownloadListener> _Nullable)downloadListener completionHandler:(void (^)(SOCStatusRequest<SOCMultiplatformFile *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("downloadFile(fileName:fileUrl:downloadListener:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)fetchAllUsersWithCompletionHandler:(void (^)(SOCStatusRequest<NSArray<SOCUser *> *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("fetchAllUsers(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)fetchDialogDialogId:(int32_t)dialogId completionHandler:(void (^)(SOCStatusRequest<SOCDialog *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("fetchDialog(dialogId:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)fetchDialogsPage:(int32_t)page completionHandler:(void (^)(SOCStatusRequest<SOCData<SOCDialog *> *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("fetchDialogs(page:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getContactDialogId:(int32_t)dialogId completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getContact(dialogId:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getFieldsWithCompletionHandler:(void (^)(SOCStatusRequest<NSArray<SOCField *> *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getFields(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)removeDialogDialogId:(int32_t)dialogId completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("removeDialog(dialogId:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)sendMessageDialogId:(int32_t)dialogId message:(NSString *)message files:(NSArray<SOCMultiplatformFile *> * _Nullable)files integrationId:(int32_t)integrationId completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("sendMessage(dialogId:message:files:integrationId:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)takeDialogDialogId:(int32_t)dialogId completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("takeDialog(dialogId:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)transferDialogDialogId:(int32_t)dialogId userId:(int32_t)userId completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("transferDialog(dialogId:userId:completionHandler:)")));
@property (readonly) SOCKtor_client_coreHttpClient *httpClient __attribute__((swift_name("httpClient")));
@property (readonly) SOCKotlinx_serialization_jsonJson *json __attribute__((swift_name("json")));
@property (readonly) SOCStorage *storage __attribute__((swift_name("storage")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("City")))
@interface SOCCity : SOCBase
- (instancetype)initWithCityId:(SOCInt * _Nullable)cityId countryId:(SOCInt * _Nullable)countryId regionId:(SOCInt * _Nullable)regionId name:(NSString * _Nullable)name __attribute__((swift_name("init(cityId:countryId:regionId:name:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCCityCompanion *companion __attribute__((swift_name("companion")));
- (SOCInt * _Nullable)component1 __attribute__((swift_name("component1()")));
- (SOCInt * _Nullable)component2 __attribute__((swift_name("component2()")));
- (SOCInt * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (SOCCity *)doCopyCityId:(SOCInt * _Nullable)cityId countryId:(SOCInt * _Nullable)countryId regionId:(SOCInt * _Nullable)regionId name:(NSString * _Nullable)name __attribute__((swift_name("doCopy(cityId:countryId:regionId:name:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SOCInt * _Nullable cityId __attribute__((swift_name("cityId")));
@property (readonly) SOCInt * _Nullable countryId __attribute__((swift_name("countryId")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@property (readonly) SOCInt * _Nullable regionId __attribute__((swift_name("regionId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("City.Companion")))
@interface SOCCityCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCCityCompanion *shared __attribute__((swift_name("shared")));
- (id<SOCKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Dialog")))
@interface SOCDialog : SOCBase
- (instancetype)initWithId:(int32_t)id name:(NSString * _Nullable)name createdAt:(SOCLong * _Nullable)createdAt updatedAt:(SOCLong * _Nullable)updatedAt isOnline:(SOCInt * _Nullable)isOnline preview:(NSString * _Nullable)preview settingId:(SOCInt * _Nullable)settingId integrationId:(SOCInt * _Nullable)integrationId counterUnread:(int32_t)counterUnread initials:(NSString * _Nullable)initials userId:(SOCInt * _Nullable)userId phone:(NSString * _Nullable)phone email:(NSString * _Nullable)email city:(SOCCity * _Nullable)city fields:(SOCSite * _Nullable)fields messages:(NSMutableArray<SOCMessage *> *)messages __attribute__((swift_name("init(id:name:createdAt:updatedAt:isOnline:preview:settingId:integrationId:counterUnread:initials:userId:phone:email:city:fields:messages:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCDialogCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component10 __attribute__((swift_name("component10()")));
- (SOCInt * _Nullable)component11 __attribute__((swift_name("component11()")));
- (NSString * _Nullable)component12 __attribute__((swift_name("component12()")));
- (NSString * _Nullable)component13 __attribute__((swift_name("component13()")));
- (SOCCity * _Nullable)component14 __attribute__((swift_name("component14()")));
- (SOCSite * _Nullable)component15 __attribute__((swift_name("component15()")));
- (NSMutableArray<SOCMessage *> *)component16 __attribute__((swift_name("component16()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (SOCLong * _Nullable)component3 __attribute__((swift_name("component3()")));
- (SOCLong * _Nullable)component4 __attribute__((swift_name("component4()")));
- (SOCInt * _Nullable)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (SOCInt * _Nullable)component7 __attribute__((swift_name("component7()")));
- (SOCInt * _Nullable)component8 __attribute__((swift_name("component8()")));
- (int32_t)component9 __attribute__((swift_name("component9()")));
- (SOCDialog *)doCopyId:(int32_t)id name:(NSString * _Nullable)name createdAt:(SOCLong * _Nullable)createdAt updatedAt:(SOCLong * _Nullable)updatedAt isOnline:(SOCInt * _Nullable)isOnline preview:(NSString * _Nullable)preview settingId:(SOCInt * _Nullable)settingId integrationId:(SOCInt * _Nullable)integrationId counterUnread:(int32_t)counterUnread initials:(NSString * _Nullable)initials userId:(SOCInt * _Nullable)userId phone:(NSString * _Nullable)phone email:(NSString * _Nullable)email city:(SOCCity * _Nullable)city fields:(SOCSite * _Nullable)fields messages:(NSMutableArray<SOCMessage *> *)messages __attribute__((swift_name("doCopy(id:name:createdAt:updatedAt:isOnline:preview:settingId:integrationId:counterUnread:initials:userId:phone:email:city:fields:messages:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SOCCity * _Nullable city __attribute__((swift_name("city")));
@property (readonly) int32_t counterUnread __attribute__((swift_name("counterUnread")));
@property (readonly) SOCLong * _Nullable createdAt __attribute__((swift_name("createdAt")));
@property (readonly) NSString * _Nullable email __attribute__((swift_name("email")));
@property (readonly) SOCSite * _Nullable fields __attribute__((swift_name("fields")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable initials __attribute__((swift_name("initials")));
@property (readonly) SOCInt * _Nullable integrationId __attribute__((swift_name("integrationId")));
@property (readonly) SOCInt * _Nullable isOnline __attribute__((swift_name("isOnline")));
@property (readonly) NSMutableArray<SOCMessage *> *messages __attribute__((swift_name("messages")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@property (readonly) NSString * _Nullable phone __attribute__((swift_name("phone")));
@property (readonly) NSString * _Nullable preview __attribute__((swift_name("preview")));
@property (readonly) SOCInt * _Nullable settingId __attribute__((swift_name("settingId")));
@property (readonly) SOCLong * _Nullable updatedAt __attribute__((swift_name("updatedAt")));
@property (readonly) SOCInt * _Nullable userId __attribute__((swift_name("userId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Dialog.Companion")))
@interface SOCDialogCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCDialogCompanion *shared __attribute__((swift_name("shared")));
- (id<SOCKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Email")))
@interface SOCEmail : SOCBase
- (instancetype)initWithEmailId:(int32_t)emailId boxId:(SOCInt * _Nullable)boxId clientId:(SOCInt * _Nullable)clientId income:(SOCInt * _Nullable)income fromEmail:(NSString * _Nullable)fromEmail fromName:(NSString * _Nullable)fromName toEmail:(NSString * _Nullable)toEmail toName:(NSString * _Nullable)toName subject:(NSString * _Nullable)subject attachCount:(SOCInt * _Nullable)attachCount ownerId:(SOCInt * _Nullable)ownerId mailRead:(SOCInt * _Nullable)mailRead integrationId:(NSString * _Nullable)integrationId preview:(NSString * _Nullable)preview contentSummary:(NSString * _Nullable)contentSummary __attribute__((swift_name("init(emailId:boxId:clientId:income:fromEmail:fromName:toEmail:toName:subject:attachCount:ownerId:mailRead:integrationId:preview:contentSummary:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCEmailCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (SOCInt * _Nullable)component10 __attribute__((swift_name("component10()")));
- (SOCInt * _Nullable)component11 __attribute__((swift_name("component11()")));
- (SOCInt * _Nullable)component12 __attribute__((swift_name("component12()")));
- (NSString * _Nullable)component13 __attribute__((swift_name("component13()")));
- (NSString * _Nullable)component14 __attribute__((swift_name("component14()")));
- (NSString * _Nullable)component15 __attribute__((swift_name("component15()")));
- (SOCInt * _Nullable)component2 __attribute__((swift_name("component2()")));
- (SOCInt * _Nullable)component3 __attribute__((swift_name("component3()")));
- (SOCInt * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSString * _Nullable)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (NSString * _Nullable)component7 __attribute__((swift_name("component7()")));
- (NSString * _Nullable)component8 __attribute__((swift_name("component8()")));
- (NSString * _Nullable)component9 __attribute__((swift_name("component9()")));
- (SOCEmail *)doCopyEmailId:(int32_t)emailId boxId:(SOCInt * _Nullable)boxId clientId:(SOCInt * _Nullable)clientId income:(SOCInt * _Nullable)income fromEmail:(NSString * _Nullable)fromEmail fromName:(NSString * _Nullable)fromName toEmail:(NSString * _Nullable)toEmail toName:(NSString * _Nullable)toName subject:(NSString * _Nullable)subject attachCount:(SOCInt * _Nullable)attachCount ownerId:(SOCInt * _Nullable)ownerId mailRead:(SOCInt * _Nullable)mailRead integrationId:(NSString * _Nullable)integrationId preview:(NSString * _Nullable)preview contentSummary:(NSString * _Nullable)contentSummary __attribute__((swift_name("doCopy(emailId:boxId:clientId:income:fromEmail:fromName:toEmail:toName:subject:attachCount:ownerId:mailRead:integrationId:preview:contentSummary:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SOCInt * _Nullable attachCount __attribute__((swift_name("attachCount")));
@property (readonly) SOCInt * _Nullable boxId __attribute__((swift_name("boxId")));
@property (readonly) SOCInt * _Nullable clientId __attribute__((swift_name("clientId")));
@property (readonly) NSString * _Nullable contentSummary __attribute__((swift_name("contentSummary")));
@property (readonly) int32_t emailId __attribute__((swift_name("emailId")));
@property (readonly) NSString * _Nullable fromEmail __attribute__((swift_name("fromEmail")));
@property (readonly) NSString * _Nullable fromName __attribute__((swift_name("fromName")));
@property (readonly) SOCInt * _Nullable income __attribute__((swift_name("income")));
@property (readonly) NSString * _Nullable integrationId __attribute__((swift_name("integrationId")));
@property (readonly) SOCInt * _Nullable mailRead __attribute__((swift_name("mailRead")));
@property (readonly) SOCInt * _Nullable ownerId __attribute__((swift_name("ownerId")));
@property (readonly) NSString * _Nullable preview __attribute__((swift_name("preview")));
@property (readonly) NSString * _Nullable subject __attribute__((swift_name("subject")));
@property (readonly) NSString * _Nullable toEmail __attribute__((swift_name("toEmail")));
@property (readonly) NSString * _Nullable toName __attribute__((swift_name("toName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Email.Companion")))
@interface SOCEmailCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCEmailCompanion *shared __attribute__((swift_name("shared")));
- (id<SOCKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ErrorData")))
@interface SOCErrorData : SOCBase
- (instancetype)initWithName:(NSString * _Nullable)name value:(NSString * _Nullable)value __attribute__((swift_name("init(name:value:)"))) __attribute__((objc_designated_initializer));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (SOCErrorData *)doCopyName:(NSString * _Nullable)name value:(NSString * _Nullable)value __attribute__((swift_name("doCopy(name:value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@property (readonly) NSString * _Nullable value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Integration")))
@interface SOCIntegration : SOCBase
- (instancetype)initWithId:(SOCInt * _Nullable)id groupId:(SOCInt * _Nullable)groupId name:(NSString * _Nullable)name description:(NSString * _Nullable)description image:(NSString * _Nullable)image connected:(SOCBoolean * _Nullable)connected __attribute__((swift_name("init(id:groupId:name:description:image:connected:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCIntegrationCompanion *companion __attribute__((swift_name("companion")));
- (SOCInt * _Nullable)component1 __attribute__((swift_name("component1()")));
- (SOCInt * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSString * _Nullable)component5 __attribute__((swift_name("component5()")));
- (SOCBoolean * _Nullable)component6 __attribute__((swift_name("component6()")));
- (SOCIntegration *)doCopyId:(SOCInt * _Nullable)id groupId:(SOCInt * _Nullable)groupId name:(NSString * _Nullable)name description:(NSString * _Nullable)description image:(NSString * _Nullable)image connected:(SOCBoolean * _Nullable)connected __attribute__((swift_name("doCopy(id:groupId:name:description:image:connected:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SOCBoolean * _Nullable connected __attribute__((swift_name("connected")));
@property (readonly) NSString * _Nullable description_ __attribute__((swift_name("description_")));
@property (readonly) SOCInt * _Nullable groupId __attribute__((swift_name("groupId")));
@property (readonly) SOCInt * _Nullable id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable image __attribute__((swift_name("image")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Integration.Companion")))
@interface SOCIntegrationCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCIntegrationCompanion *shared __attribute__((swift_name("shared")));
- (id<SOCKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Message")))
@interface SOCMessage : SOCBase
- (instancetype)initWithId:(int32_t)id type:(SOCInt * _Nullable)type createdAt:(SOCLong * _Nullable)createdAt updatedAt:(SOCLong * _Nullable)updatedAt messenger:(SOCMessenger * _Nullable)messenger email:(SOCEmail * _Nullable)email __attribute__((swift_name("init(id:type:createdAt:updatedAt:messenger:email:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCMessageCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (SOCInt * _Nullable)component2 __attribute__((swift_name("component2()")));
- (SOCLong * _Nullable)component3 __attribute__((swift_name("component3()")));
- (SOCLong * _Nullable)component4 __attribute__((swift_name("component4()")));
- (SOCMessenger * _Nullable)component5 __attribute__((swift_name("component5()")));
- (SOCEmail * _Nullable)component6 __attribute__((swift_name("component6()")));
- (SOCMessage *)doCopyId:(int32_t)id type:(SOCInt * _Nullable)type createdAt:(SOCLong * _Nullable)createdAt updatedAt:(SOCLong * _Nullable)updatedAt messenger:(SOCMessenger * _Nullable)messenger email:(SOCEmail * _Nullable)email __attribute__((swift_name("doCopy(id:type:createdAt:updatedAt:messenger:email:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SOCLong * _Nullable createdAt __attribute__((swift_name("createdAt")));
@property (readonly) SOCEmail * _Nullable email __attribute__((swift_name("email")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@property (readonly) SOCMessenger * _Nullable messenger __attribute__((swift_name("messenger")));
@property (readonly) SOCInt * _Nullable type __attribute__((swift_name("type")));
@property (readonly) SOCLong * _Nullable updatedAt __attribute__((swift_name("updatedAt")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Message.Companion")))
@interface SOCMessageCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCMessageCompanion *shared __attribute__((swift_name("shared")));
- (id<SOCKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Messenger")))
@interface SOCMessenger : SOCBase
- (instancetype)initWithId:(int32_t)id noteId:(SOCInt * _Nullable)noteId clientId:(SOCInt * _Nullable)clientId contactMessengerId:(SOCInt * _Nullable)contactMessengerId settingId:(SOCInt * _Nullable)settingId integrationId:(SOCInt * _Nullable)integrationId direction:(SOCInt * _Nullable)direction userId:(SOCInt * _Nullable)userId text:(NSString * _Nullable)text sentAt:(SOCLong * _Nullable)sentAt readAt:(SOCLong * _Nullable)readAt filename:(NSString * _Nullable)filename filenameOri:(NSString * _Nullable)filenameOri filesize:(SOCLong * _Nullable)filesize urlFull:(NSString * _Nullable)urlFull responsible:(SOCUser * _Nullable)responsible integration:(SOCShortIntegration * _Nullable)integration __attribute__((swift_name("init(id:noteId:clientId:contactMessengerId:settingId:integrationId:direction:userId:text:sentAt:readAt:filename:filenameOri:filesize:urlFull:responsible:integration:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCMessengerCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (SOCLong * _Nullable)component10 __attribute__((swift_name("component10()")));
- (SOCLong * _Nullable)component11 __attribute__((swift_name("component11()")));
- (NSString * _Nullable)component12 __attribute__((swift_name("component12()")));
- (NSString * _Nullable)component13 __attribute__((swift_name("component13()")));
- (SOCLong * _Nullable)component14 __attribute__((swift_name("component14()")));
- (NSString * _Nullable)component15 __attribute__((swift_name("component15()")));
- (SOCUser * _Nullable)component16 __attribute__((swift_name("component16()")));
- (SOCShortIntegration * _Nullable)component17 __attribute__((swift_name("component17()")));
- (SOCInt * _Nullable)component2 __attribute__((swift_name("component2()")));
- (SOCInt * _Nullable)component3 __attribute__((swift_name("component3()")));
- (SOCInt * _Nullable)component4 __attribute__((swift_name("component4()")));
- (SOCInt * _Nullable)component5 __attribute__((swift_name("component5()")));
- (SOCInt * _Nullable)component6 __attribute__((swift_name("component6()")));
- (SOCInt * _Nullable)component7 __attribute__((swift_name("component7()")));
- (SOCInt * _Nullable)component8 __attribute__((swift_name("component8()")));
- (NSString * _Nullable)component9 __attribute__((swift_name("component9()")));
- (SOCMessenger *)doCopyId:(int32_t)id noteId:(SOCInt * _Nullable)noteId clientId:(SOCInt * _Nullable)clientId contactMessengerId:(SOCInt * _Nullable)contactMessengerId settingId:(SOCInt * _Nullable)settingId integrationId:(SOCInt * _Nullable)integrationId direction:(SOCInt * _Nullable)direction userId:(SOCInt * _Nullable)userId text:(NSString * _Nullable)text sentAt:(SOCLong * _Nullable)sentAt readAt:(SOCLong * _Nullable)readAt filename:(NSString * _Nullable)filename filenameOri:(NSString * _Nullable)filenameOri filesize:(SOCLong * _Nullable)filesize urlFull:(NSString * _Nullable)urlFull responsible:(SOCUser * _Nullable)responsible integration:(SOCShortIntegration * _Nullable)integration __attribute__((swift_name("doCopy(id:noteId:clientId:contactMessengerId:settingId:integrationId:direction:userId:text:sentAt:readAt:filename:filenameOri:filesize:urlFull:responsible:integration:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SOCInt * _Nullable clientId __attribute__((swift_name("clientId")));
@property (readonly) SOCInt * _Nullable contactMessengerId __attribute__((swift_name("contactMessengerId")));
@property (readonly) SOCInt * _Nullable direction __attribute__((swift_name("direction")));
@property (readonly) NSString * _Nullable filename __attribute__((swift_name("filename")));
@property (readonly) NSString * _Nullable filenameOri __attribute__((swift_name("filenameOri")));
@property (readonly) SOCLong * _Nullable filesize __attribute__((swift_name("filesize")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@property (readonly) SOCShortIntegration * _Nullable integration __attribute__((swift_name("integration")));
@property (readonly) SOCInt * _Nullable integrationId __attribute__((swift_name("integrationId")));
@property (readonly) SOCInt * _Nullable noteId __attribute__((swift_name("noteId")));
@property (readonly) SOCLong * _Nullable readAt __attribute__((swift_name("readAt")));
@property (readonly) SOCUser * _Nullable responsible __attribute__((swift_name("responsible")));
@property (readonly) SOCLong * _Nullable sentAt __attribute__((swift_name("sentAt")));
@property (readonly) SOCInt * _Nullable settingId __attribute__((swift_name("settingId")));
@property (readonly) NSString * _Nullable text __attribute__((swift_name("text")));
@property (readonly) NSString * _Nullable urlFull __attribute__((swift_name("urlFull")));
@property (readonly) SOCInt * _Nullable userId __attribute__((swift_name("userId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Messenger.Companion")))
@interface SOCMessengerCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCMessengerCompanion *shared __attribute__((swift_name("shared")));
- (id<SOCKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((swift_name("Enum")))
@interface SOCEnum : SOCBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) SOCEnumCompanion *companion __attribute__((swift_name("companion")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Pipeline")))
@interface SOCPipeline : SOCEnum
- (instancetype)initWithId:(int32_t)id name:(NSString * _Nullable)name sort:(SOCInt * _Nullable)sort stages:(NSArray<SOCStage *> * _Nullable)stages __attribute__((swift_name("init(id:name:sort:stages:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
@property (class, readonly, getter=companion) SOCPipelineCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (SOCInt * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSArray<SOCStage *> * _Nullable)component4 __attribute__((swift_name("component4()")));
- (SOCPipeline *)doCopyId:(int32_t)id name:(NSString * _Nullable)name sort:(SOCInt * _Nullable)sort stages:(NSArray<SOCStage *> * _Nullable)stages __attribute__((swift_name("doCopy(id:name:sort:stages:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@property (readonly) SOCInt * _Nullable sort __attribute__((swift_name("sort")));
@property (readonly) NSArray<SOCStage *> * _Nullable stages __attribute__((swift_name("stages")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Pipeline.Companion")))
@interface SOCPipelineCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCPipelineCompanion *shared __attribute__((swift_name("shared")));
- (id<SOCKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ShortIntegration")))
@interface SOCShortIntegration : SOCBase
- (instancetype)initWithImage:(NSString * _Nullable)image __attribute__((swift_name("init(image:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCShortIntegrationCompanion *companion __attribute__((swift_name("companion")));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (SOCShortIntegration *)doCopyImage:(NSString * _Nullable)image __attribute__((swift_name("doCopy(image:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable image __attribute__((swift_name("image")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ShortIntegration.Companion")))
@interface SOCShortIntegrationCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCShortIntegrationCompanion *shared __attribute__((swift_name("shared")));
- (id<SOCKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Site")))
@interface SOCSite : SOCBase
- (instancetype)initWithPageUrl:(NSString * _Nullable)pageUrl pageTitle:(NSString * _Nullable)pageTitle __attribute__((swift_name("init(pageUrl:pageTitle:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCSiteCompanion *companion __attribute__((swift_name("companion")));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (SOCSite *)doCopyPageUrl:(NSString * _Nullable)pageUrl pageTitle:(NSString * _Nullable)pageTitle __attribute__((swift_name("doCopy(pageUrl:pageTitle:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable pageTitle __attribute__((swift_name("pageTitle")));
@property (readonly) NSString * _Nullable pageUrl __attribute__((swift_name("pageUrl")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Site.Companion")))
@interface SOCSiteCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCSiteCompanion *shared __attribute__((swift_name("shared")));
- (id<SOCKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Stage")))
@interface SOCStage : SOCBase
- (instancetype)initWithId:(int32_t)id pipelineId:(SOCInt * _Nullable)pipelineId name:(NSString * _Nullable)name color:(NSString * _Nullable)color sort:(SOCInt * _Nullable)sort __attribute__((swift_name("init(id:pipelineId:name:color:sort:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCStageCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (SOCInt * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (SOCInt * _Nullable)component5 __attribute__((swift_name("component5()")));
- (SOCStage *)doCopyId:(int32_t)id pipelineId:(SOCInt * _Nullable)pipelineId name:(NSString * _Nullable)name color:(NSString * _Nullable)color sort:(SOCInt * _Nullable)sort __attribute__((swift_name("doCopy(id:pipelineId:name:color:sort:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable color __attribute__((swift_name("color")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@property (readonly) SOCInt * _Nullable pipelineId __attribute__((swift_name("pipelineId")));
@property (readonly) SOCInt * _Nullable sort __attribute__((swift_name("sort")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Stage.Companion")))
@interface SOCStageCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCStageCompanion *shared __attribute__((swift_name("shared")));
- (id<SOCKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((swift_name("StatusRequest")))
@interface SOCStatusRequest<__covariant T> : SOCBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StatusRequestEmpty")))
@interface SOCStatusRequestEmpty<T> : SOCStatusRequest<T>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StatusRequestError")))
@interface SOCStatusRequestError<T> : SOCStatusRequest<T>
- (instancetype)initWithErrorCode:(int32_t)errorCode errorMessage:(NSString *)errorMessage errorData:(NSArray<SOCErrorData *> * _Nullable)errorData __attribute__((swift_name("init(errorCode:errorMessage:errorData:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSArray<SOCErrorData *> * _Nullable)component3 __attribute__((swift_name("component3()")));
- (SOCStatusRequestError<T> *)doCopyErrorCode:(int32_t)errorCode errorMessage:(NSString *)errorMessage errorData:(NSArray<SOCErrorData *> * _Nullable)errorData __attribute__((swift_name("doCopy(errorCode:errorMessage:errorData:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t errorCode __attribute__((swift_name("errorCode")));
@property (readonly) NSArray<SOCErrorData *> * _Nullable errorData __attribute__((swift_name("errorData")));
@property (readonly) NSString *errorMessage __attribute__((swift_name("errorMessage")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StatusRequestLoading")))
@interface SOCStatusRequestLoading<T> : SOCStatusRequest<T>
- (instancetype)initWithData:(T _Nullable)data loadingMessage:(NSString * _Nullable)loadingMessage __attribute__((swift_name("init(data:loadingMessage:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (T _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (SOCStatusRequestLoading<T> *)doCopyData:(T _Nullable)data loadingMessage:(NSString * _Nullable)loadingMessage __attribute__((swift_name("doCopy(data:loadingMessage:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) T _Nullable data __attribute__((swift_name("data")));
@property (readonly) NSString * _Nullable loadingMessage __attribute__((swift_name("loadingMessage")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StatusRequestSuccess")))
@interface SOCStatusRequestSuccess<T> : SOCStatusRequest<T>
- (instancetype)initWithData:(T _Nullable)data __attribute__((swift_name("init(data:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (T _Nullable)component1 __attribute__((swift_name("component1()")));
- (SOCStatusRequestSuccess<T> *)doCopyData:(T _Nullable)data __attribute__((swift_name("doCopy(data:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) T _Nullable data __attribute__((swift_name("data")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("User")))
@interface SOCUser : SOCEnum
- (instancetype)initWithId:(int32_t)id roleId:(SOCInt * _Nullable)roleId email:(NSString * _Nullable)email name:(NSString * _Nullable)name dateBorn:(NSString * _Nullable)dateBorn phone:(NSString * _Nullable)phone avatarUrl:(NSString * _Nullable)avatarUrl initials:(NSString * _Nullable)initials chatOnline:(SOCInt * _Nullable)chatOnline __attribute__((swift_name("init(id:roleId:email:name:dateBorn:phone:avatarUrl:initials:chatOnline:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
@property (class, readonly, getter=companion) SOCUserCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (SOCInt * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSString * _Nullable)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (NSString * _Nullable)component7 __attribute__((swift_name("component7()")));
- (NSString * _Nullable)component8 __attribute__((swift_name("component8()")));
- (SOCInt * _Nullable)component9 __attribute__((swift_name("component9()")));
- (SOCUser *)doCopyId:(int32_t)id roleId:(SOCInt * _Nullable)roleId email:(NSString * _Nullable)email name:(NSString * _Nullable)name dateBorn:(NSString * _Nullable)dateBorn phone:(NSString * _Nullable)phone avatarUrl:(NSString * _Nullable)avatarUrl initials:(NSString * _Nullable)initials chatOnline:(SOCInt * _Nullable)chatOnline __attribute__((swift_name("doCopy(id:roleId:email:name:dateBorn:phone:avatarUrl:initials:chatOnline:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable avatarUrl __attribute__((swift_name("avatarUrl")));
@property SOCInt * _Nullable chatOnline __attribute__((swift_name("chatOnline")));
@property (readonly) NSString * _Nullable dateBorn __attribute__((swift_name("dateBorn")));
@property (readonly) NSString * _Nullable email __attribute__((swift_name("email")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable initials __attribute__((swift_name("initials")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@property (readonly) NSString * _Nullable phone __attribute__((swift_name("phone")));
@property (readonly) SOCInt * _Nullable roleId __attribute__((swift_name("roleId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("User.Companion")))
@interface SOCUserCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCUserCompanion *shared __attribute__((swift_name("shared")));
- (id<SOCKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Note")))
@interface SOCNote : SOCBase
- (instancetype)initWithId:(SOCInt * _Nullable)id type:(SOCInt * _Nullable)type leadId:(SOCInt * _Nullable)leadId contactId:(SOCInt * _Nullable)contactId companyId:(SOCInt * _Nullable)companyId userId:(SOCInt * _Nullable)userId createdAt:(SOCLong * _Nullable)createdAt updatedAt:(SOCLong * _Nullable)updatedAt email:(SOCEmail * _Nullable)email __attribute__((swift_name("init(id:type:leadId:contactId:companyId:userId:createdAt:updatedAt:email:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCNoteCompanion *companion __attribute__((swift_name("companion")));
- (SOCInt * _Nullable)component1 __attribute__((swift_name("component1()")));
- (SOCInt * _Nullable)component2 __attribute__((swift_name("component2()")));
- (SOCInt * _Nullable)component3 __attribute__((swift_name("component3()")));
- (SOCInt * _Nullable)component4 __attribute__((swift_name("component4()")));
- (SOCInt * _Nullable)component5 __attribute__((swift_name("component5()")));
- (SOCInt * _Nullable)component6 __attribute__((swift_name("component6()")));
- (SOCLong * _Nullable)component7 __attribute__((swift_name("component7()")));
- (SOCLong * _Nullable)component8 __attribute__((swift_name("component8()")));
- (SOCEmail * _Nullable)component9 __attribute__((swift_name("component9()")));
- (SOCNote *)doCopyId:(SOCInt * _Nullable)id type:(SOCInt * _Nullable)type leadId:(SOCInt * _Nullable)leadId contactId:(SOCInt * _Nullable)contactId companyId:(SOCInt * _Nullable)companyId userId:(SOCInt * _Nullable)userId createdAt:(SOCLong * _Nullable)createdAt updatedAt:(SOCLong * _Nullable)updatedAt email:(SOCEmail * _Nullable)email __attribute__((swift_name("doCopy(id:type:leadId:contactId:companyId:userId:createdAt:updatedAt:email:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SOCInt * _Nullable companyId __attribute__((swift_name("companyId")));
@property (readonly) SOCInt * _Nullable contactId __attribute__((swift_name("contactId")));
@property (readonly) SOCLong * _Nullable createdAt __attribute__((swift_name("createdAt")));
@property (readonly) SOCEmail * _Nullable email __attribute__((swift_name("email")));
@property (readonly) SOCInt * _Nullable id __attribute__((swift_name("id")));
@property (readonly) SOCInt * _Nullable leadId __attribute__((swift_name("leadId")));
@property (readonly) SOCInt * _Nullable type __attribute__((swift_name("type")));
@property (readonly) SOCLong * _Nullable updatedAt __attribute__((swift_name("updatedAt")));
@property (readonly) SOCInt * _Nullable userId __attribute__((swift_name("userId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Note.Companion")))
@interface SOCNoteCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCNoteCompanion *shared __attribute__((swift_name("shared")));
- (id<SOCKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Data")))
@interface SOCData<T> : SOCBase
- (instancetype)initWithCurrentPage:(int32_t)currentPage lastPage:(SOCInt * _Nullable)lastPage data:(NSArray<id> * _Nullable)data perPage:(SOCInt * _Nullable)perPage __attribute__((swift_name("init(currentPage:lastPage:data:perPage:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCDataCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (SOCInt * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSArray<id> * _Nullable)component3 __attribute__((swift_name("component3()")));
- (SOCInt * _Nullable)component4 __attribute__((swift_name("component4()")));
- (SOCData<T> *)doCopyCurrentPage:(int32_t)currentPage lastPage:(SOCInt * _Nullable)lastPage data:(NSArray<id> * _Nullable)data perPage:(SOCInt * _Nullable)perPage __attribute__((swift_name("doCopy(currentPage:lastPage:data:perPage:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t currentPage __attribute__((swift_name("currentPage")));
@property (readonly) NSArray<id> * _Nullable data __attribute__((swift_name("data")));
@property (readonly) SOCInt * _Nullable lastPage __attribute__((swift_name("lastPage")));
@property (readonly) SOCInt * _Nullable perPage __attribute__((swift_name("perPage")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataCompanion")))
@interface SOCDataCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCDataCompanion *shared __attribute__((swift_name("shared")));
- (id<SOCKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(SOCKotlinArray<id<SOCKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
- (id<SOCKotlinx_serialization_coreKSerializer>)serializerTypeSerial0:(id<SOCKotlinx_serialization_coreKSerializer>)typeSerial0 __attribute__((swift_name("serializer(typeSerial0:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MultiplatformFile")))
@interface SOCMultiplatformFile : SOCBase
- (instancetype)initWithName:(NSString *)name bytes:(SOCKotlinByteArray *)bytes bytesStr:(NSString *)bytesStr path:(NSString * _Nullable)path __attribute__((swift_name("init(name:bytes:bytesStr:path:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (SOCKotlinByteArray *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (SOCMultiplatformFile *)doCopyName:(NSString *)name bytes:(SOCKotlinByteArray *)bytes bytesStr:(NSString *)bytesStr path:(NSString * _Nullable)path __attribute__((swift_name("doCopy(name:bytes:bytesStr:path:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SOCKotlinByteArray *bytes __attribute__((swift_name("bytes")));
@property (readonly) NSString *bytesStr __attribute__((swift_name("bytesStr")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSString * _Nullable path __attribute__((swift_name("path")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Enum.Companion")))
@interface SOCEnumCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCEnumCompanion *shared __attribute__((swift_name("shared")));
- (id<SOCKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerializationStrategy")))
@protocol SOCKotlinx_serialization_coreSerializationStrategy
@required
- (void)serializeEncoder:(id<SOCKotlinx_serialization_coreEncoder>)encoder value:(id _Nullable)value __attribute__((swift_name("serialize(encoder:value:)")));
@property (readonly) id<SOCKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreDeserializationStrategy")))
@protocol SOCKotlinx_serialization_coreDeserializationStrategy
@required
- (id _Nullable)deserializeDecoder:(id<SOCKotlinx_serialization_coreDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
@property (readonly) id<SOCKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreKSerializer")))
@protocol SOCKotlinx_serialization_coreKSerializer <SOCKotlinx_serialization_coreSerializationStrategy, SOCKotlinx_serialization_coreDeserializationStrategy>
@required
@end;

__attribute__((swift_name("Kotlinx_serialization_jsonJsonContentPolymorphicSerializer")))
@interface SOCKotlinx_serialization_jsonJsonContentPolymorphicSerializer<T> : SOCBase <SOCKotlinx_serialization_coreKSerializer>
- (instancetype)initWithBaseClass:(id<SOCKotlinKClass>)baseClass __attribute__((swift_name("init(baseClass:)"))) __attribute__((objc_designated_initializer));
- (T)deserializeDecoder:(id<SOCKotlinx_serialization_coreDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
- (id<SOCKotlinx_serialization_coreDeserializationStrategy>)selectDeserializerElement:(SOCKotlinx_serialization_jsonJsonElement *)element __attribute__((swift_name("selectDeserializer(element:)")));
- (void)serializeEncoder:(id<SOCKotlinx_serialization_coreEncoder>)encoder value:(T)value __attribute__((swift_name("serialize(encoder:value:)")));
@property (readonly) id<SOCKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EnumSerializer")))
@interface SOCEnumSerializer : SOCKotlinx_serialization_jsonJsonContentPolymorphicSerializer<SOCEnum *> <SOCKotlinx_serialization_coreKSerializer>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithBaseClass:(id<SOCKotlinKClass>)baseClass __attribute__((swift_name("init(baseClass:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)enumSerializer __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCEnumSerializer *shared __attribute__((swift_name("shared")));
- (id<SOCKotlinx_serialization_coreDeserializationStrategy>)selectDeserializerElement:(SOCKotlinx_serialization_jsonJsonElement *)element __attribute__((swift_name("selectDeserializer(element:)")));
@property (readonly) id<SOCKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Field")))
@interface SOCField : SOCBase
- (instancetype)initWithId:(SOCInt * _Nullable)id name:(NSString * _Nullable)name system:(NSString * _Nullable)system varName:(NSString * _Nullable)varName varType:(SOCVarType * _Nullable)varType enums:(NSArray<SOCEnum *> * _Nullable)enums __attribute__((swift_name("init(id:name:system:varName:varType:enums:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCFieldCompanion *companion __attribute__((swift_name("companion")));
- (SOCInt * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (SOCVarType * _Nullable)component5 __attribute__((swift_name("component5()")));
- (NSArray<SOCEnum *> * _Nullable)component6 __attribute__((swift_name("component6()")));
- (SOCField *)doCopyId:(SOCInt * _Nullable)id name:(NSString * _Nullable)name system:(NSString * _Nullable)system varName:(NSString * _Nullable)varName varType:(SOCVarType * _Nullable)varType enums:(NSArray<SOCEnum *> * _Nullable)enums __attribute__((swift_name("doCopy(id:name:system:varName:varType:enums:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<SOCEnum *> * _Nullable enums __attribute__((swift_name("enums")));
@property (readonly) SOCInt * _Nullable id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@property (readonly) NSString * _Nullable system __attribute__((swift_name("system")));
@property (readonly) NSString * _Nullable varName __attribute__((swift_name("varName")));
@property (readonly) SOCVarType * _Nullable varType __attribute__((swift_name("varType")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Field.Companion")))
@interface SOCFieldCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCFieldCompanion *shared __attribute__((swift_name("shared")));
- (id<SOCKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InfoField")))
@interface SOCInfoField : SOCEnum
- (instancetype)initWithId:(SOCInt * _Nullable)id name:(NSString * _Nullable)name __attribute__((swift_name("init(id:name:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
@property (class, readonly, getter=companion) SOCInfoFieldCompanion *companion __attribute__((swift_name("companion")));
- (SOCInt * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (SOCInfoField *)doCopyId:(SOCInt * _Nullable)id name:(NSString * _Nullable)name __attribute__((swift_name("doCopy(id:name:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SOCInt * _Nullable id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InfoField.Companion")))
@interface SOCInfoFieldCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCInfoFieldCompanion *shared __attribute__((swift_name("shared")));
- (id<SOCKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("VarType")))
@interface SOCVarType : SOCBase
- (instancetype)initWithId:(SOCInt * _Nullable)id name:(NSString * _Nullable)name type:(NSString * _Nullable)type enums:(SOCInt * _Nullable)enums __attribute__((swift_name("init(id:name:type:enums:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCVarTypeCompanion *companion __attribute__((swift_name("companion")));
- (SOCInt * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (SOCInt * _Nullable)component4 __attribute__((swift_name("component4()")));
- (SOCVarType *)doCopyId:(SOCInt * _Nullable)id name:(NSString * _Nullable)name type:(NSString * _Nullable)type enums:(SOCInt * _Nullable)enums __attribute__((swift_name("doCopy(id:name:type:enums:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SOCInt * _Nullable enums __attribute__((swift_name("enums")));
@property (readonly) SOCInt * _Nullable id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@property (readonly) NSString * _Nullable type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("VarType.Companion")))
@interface SOCVarTypeCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCVarTypeCompanion *shared __attribute__((swift_name("shared")));
- (id<SOCKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("HttpEngineFactory")))
@interface SOCHttpEngineFactory : SOCBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (id<SOCKtor_client_coreHttpClientEngineFactory>)createEngine __attribute__((swift_name("createEngine()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("NetworkSettings")))
@interface SOCNetworkSettings : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)networkSettings __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCNetworkSettings *shared __attribute__((swift_name("shared")));
@property NSString *domain __attribute__((swift_name("domain")));
@property (readonly) NSString *socket_path __attribute__((swift_name("socket_path")));
@property (readonly) NSString *socket_url __attribute__((swift_name("socket_url")));
@property (readonly) NSString *socket_url_2 __attribute__((swift_name("socket_url_2")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuthRepository")))
@interface SOCAuthRepository : SOCBase
- (instancetype)initWithAuthNetworkSource:(id<SOCIAuthNetworkSource>)authNetworkSource __attribute__((swift_name("init(authNetworkSource:)"))) __attribute__((objc_designated_initializer));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getTokenWithCompletionHandler:(void (^)(SOCStatusRequest<NSString *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getToken(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)loginEmail:(NSString *)email domain:(NSString *)domain password:(NSString *)password completionHandler:(void (^)(SOCStatusRequest<SOCUser *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("login(email:domain:password:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)logoutWithCompletionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("logout(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)sendFcmTokenToken:(NSString *)token completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("sendFcmToken(token:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("NoteRepository")))
@interface SOCNoteRepository : SOCBase
- (instancetype)initWithNoteNetworkSource:(id<SOCINoteNetworkSource>)noteNetworkSource __attribute__((swift_name("init(noteNetworkSource:)"))) __attribute__((objc_designated_initializer));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getEmailEmailId:(int32_t)emailId completionHandler:(void (^)(SOCStatusRequest<SOCNote *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getEmail(emailId:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UnsortedRepository")))
@interface SOCUnsortedRepository : SOCBase
- (instancetype)initWithUnsortedNetworkSource:(id<SOCIUnsortedNetworkSource>)unsortedNetworkSource __attribute__((swift_name("init(unsortedNetworkSource:)"))) __attribute__((objc_designated_initializer));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)changeStatusValue:(int32_t)value completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("changeStatus(value:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)createLeadParameters:(NSArray<SOCKotlinPair<NSString *, NSString *> *> *)parameters completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("createLead(parameters:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)downloadFileFileName:(NSString *)fileName fileUrl:(NSString *)fileUrl downloadListener:(id<SOCDownloadListener> _Nullable)downloadListener completionHandler:(void (^)(SOCStatusRequest<SOCMultiplatformFile *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("downloadFile(fileName:fileUrl:downloadListener:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getContactDialogId:(int32_t)dialogId completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getContact(dialogId:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getDialogDialogId:(int32_t)dialogId completionHandler:(void (^)(SOCStatusRequest<SOCDialog *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getDialog(dialogId:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getDialogsPage:(int32_t)page completionHandler:(void (^)(SOCStatusRequest<SOCData<SOCDialog *> *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getDialogs(page:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getFieldsWithCompletionHandler:(void (^)(SOCStatusRequest<NSArray<SOCField *> *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getFields(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getUsersWithCompletionHandler:(void (^)(SOCStatusRequest<NSArray<SOCUser *> *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getUsers(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)removeDialogDialogId:(int32_t)dialogId completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("removeDialog(dialogId:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)sendMessageDialogId:(int32_t)dialogId message:(NSString *)message files:(NSArray<SOCMultiplatformFile *> * _Nullable)files integrationId:(int32_t)integrationId completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("sendMessage(dialogId:message:files:integrationId:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)takeDialogDialogId:(int32_t)dialogId completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("takeDialog(dialogId:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)transferDialogDialogId:(int32_t)dialogId userId:(int32_t)userId completionHandler:(void (^)(SOCStatusRequest<SOCBoolean *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("transferDialog(dialogId:userId:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Preferences")))
@interface SOCPreferences : SOCBase
- (instancetype)initWithName:(NSString *)name __attribute__((swift_name("init(name:)"))) __attribute__((objc_designated_initializer));
- (void)clearAll __attribute__((swift_name("clearAll()")));
- (NSString * _Nullable)getStringKey:(NSString *)key defaultValue:(NSString * _Nullable)defaultValue __attribute__((swift_name("getString(key:defaultValue:)")));
- (void)setStringKey:(NSString *)key value:(NSString *)value __attribute__((swift_name("setString(key:value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Storage")))
@interface SOCStorage : SOCBase
- (instancetype)initWithPref:(SOCPreferences *)pref __attribute__((swift_name("init(pref:)"))) __attribute__((objc_designated_initializer));
- (void)clearAll __attribute__((swift_name("clearAll()")));
- (NSArray<SOCKtor_httpCookie *> *)getCookies __attribute__((swift_name("getCookies()")));
- (SOCUser * _Nullable)getCurrentUser __attribute__((swift_name("getCurrentUser()")));
- (NSString *)getDomen __attribute__((swift_name("getDomen()")));
- (NSString *)getJWTurl __attribute__((swift_name("getJWTurl()")));
- (NSString *)getToken __attribute__((swift_name("getToken()")));
- (NSString *)getUrl __attribute__((swift_name("getUrl()")));
- (void)setCookiesCookies:(NSArray<SOCKtor_httpCookie *> *)cookies __attribute__((swift_name("setCookies(cookies:)")));
- (void)setCurrentUserUser:(SOCUser *)user __attribute__((swift_name("setCurrentUser(user:)")));
- (void)setDomenNewDomen:(NSString *)newDomen __attribute__((swift_name("setDomen(newDomen:)")));
- (void)setTokenToken:(NSString *)token __attribute__((swift_name("setToken(token:)")));
@end;

__attribute__((swift_name("DownloadListener")))
@protocol SOCDownloadListener
@required
- (void)onDownloadFinishedSize:(int64_t)size isStopDownload:(SOCBoolean * _Nullable)isStopDownload __attribute__((swift_name("onDownloadFinished(size:isStopDownload:)")));
- (void)onDownloadStart __attribute__((swift_name("onDownloadStart()")));
- (void)onDownloadUpdatePercent:(int32_t)percent __attribute__((swift_name("onDownloadUpdate(percent:)")));
- (void)onDownloadUpdateDownloaded:(int64_t)downloaded size:(int64_t)size __attribute__((swift_name("onDownloadUpdate(downloaded:size:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Utils")))
@interface SOCUtils : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)utils __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCUtils *shared __attribute__((swift_name("shared")));
- (NSString *)convertTimeTimeStamp:(SOCLong * _Nullable)timeStamp isSimple:(BOOL)isSimple __attribute__((swift_name("convertTime(timeStamp:isSimple:)")));
- (NSString *)cutFileNameName:(NSString *)name maxSize:(int32_t)maxSize startIndex:(int32_t)startIndex __attribute__((swift_name("cutFileName(name:maxSize:startIndex:)")));
- (NSString *)dateFormatTimeStamp:(SOCLong * _Nullable)timeStamp format:(NSString *)format isSimple:(BOOL)isSimple __attribute__((swift_name("dateFormat(timeStamp:format:isSimple:)")));
- (NSString *)formatFileSizeBytes:(int64_t)bytes __attribute__((swift_name("formatFileSize(bytes:)")));
- (NSString *)getIconNameIntegrationId:(SOCInt * _Nullable)integrationId __attribute__((swift_name("getIconName(integrationId:)")));
- (double)roundIntegerNum:(double)num __attribute__((swift_name("roundInteger(num:)")));
- (NSString *)sliceStringStr:(NSString *)str limits:(int32_t)limits isDots:(BOOL)isDots __attribute__((swift_name("sliceString(str:limits:isDots:)")));
- (double)round:(double)receiver decimals:(int32_t)decimals __attribute__((swift_name("round(_:decimals:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MainSDK")))
@interface SOCMainSDK : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)mainSDK __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCMainSDK *shared __attribute__((swift_name("shared")));
- (void)doInitKoin __attribute__((swift_name("doInitKoin()")));
@property (readonly) SOCPreferences *preferences __attribute__((swift_name("preferences")));
@property (readonly) SOCStorage *storage __attribute__((swift_name("storage")));
@end;

__attribute__((swift_name("Koin_coreKoinComponent")))
@protocol SOCKoin_coreKoinComponent
@required
- (SOCKoin_coreKoin *)getKoin __attribute__((swift_name("getKoin()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("diMain")))
@interface SOCdiMain : SOCBase <SOCKoin_coreKoinComponent>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)diMain __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCdiMain *shared __attribute__((swift_name("shared")));
@property (readonly) SOCKotlinx_serialization_jsonJson *json __attribute__((swift_name("json")));
@property (readonly) SOCPreferences *preferences __attribute__((swift_name("preferences")));
@property (readonly) SOCStorage *storage __attribute__((swift_name("storage")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("diRepositories")))
@interface SOCdiRepositories : SOCBase <SOCKoin_coreKoinComponent>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)diRepositories __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCdiRepositories *shared __attribute__((swift_name("shared")));
@property (readonly) SOCAuthRepository *authRepository __attribute__((swift_name("authRepository")));
@property (readonly) SOCNoteRepository *noteRepository __attribute__((swift_name("noteRepository")));
@property (readonly) SOCUnsortedRepository *unsortedRepository __attribute__((swift_name("unsortedRepository")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConstantsKt")))
@interface SOCConstantsKt : SOCBase
@property (class, readonly) NSString *CURRENT_USER __attribute__((swift_name("CURRENT_USER")));
@property (class, readonly) NSString *DOMEN __attribute__((swift_name("DOMEN")));
@property (class, readonly) int32_t EMAIL_INTEGRATION __attribute__((swift_name("EMAIL_INTEGRATION")));
@property (class, readonly) NSString *JWT_TOKEN __attribute__((swift_name("JWT_TOKEN")));
@property (class, readonly) NSString *OKOCRM_TOKEN __attribute__((swift_name("OKOCRM_TOKEN")));
@property (class, readonly) NSString *XSRF_TOKEN __attribute__((swift_name("XSRF_TOKEN")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ErrorsCodeKt")))
@interface SOCErrorsCodeKt : SOCBase
@property (class, readonly) int32_t CONNECTION_ERROR __attribute__((swift_name("CONNECTION_ERROR")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MainModuleKt")))
@interface SOCMainModuleKt : SOCBase
@property (class, readonly) SOCKoin_coreModule *mainModule __attribute__((swift_name("mainModule")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("NetworkModuleKt")))
@interface SOCNetworkModuleKt : SOCBase
@property (class, readonly) SOCKoin_coreModule *networkModule __attribute__((swift_name("networkModule")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreCoroutineScope")))
@protocol SOCKotlinx_coroutines_coreCoroutineScope
@required
@property (readonly) id<SOCKotlinCoroutineContext> coroutineContext __attribute__((swift_name("coroutineContext")));
@end;

__attribute__((swift_name("KotlinCoroutineContext")))
@protocol SOCKotlinCoroutineContext
@required
- (id _Nullable)foldInitial:(id _Nullable)initial operation:(id _Nullable (^)(id _Nullable, id<SOCKotlinCoroutineContextElement>))operation __attribute__((swift_name("fold(initial:operation:)")));
- (id<SOCKotlinCoroutineContextElement> _Nullable)getKey:(id<SOCKotlinCoroutineContextKey>)key __attribute__((swift_name("get(key:)")));
- (id<SOCKotlinCoroutineContext>)minusKeyKey:(id<SOCKotlinCoroutineContextKey>)key __attribute__((swift_name("minusKey(key:)")));
- (id<SOCKotlinCoroutineContext>)plusContext:(id<SOCKotlinCoroutineContext>)context __attribute__((swift_name("plus(context:)")));
@end;

__attribute__((swift_name("KotlinCoroutineContextElement")))
@protocol SOCKotlinCoroutineContextElement <SOCKotlinCoroutineContext>
@required
@property (readonly) id<SOCKotlinCoroutineContextKey> key __attribute__((swift_name("key")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreJob")))
@protocol SOCKotlinx_coroutines_coreJob <SOCKotlinCoroutineContextElement>
@required
- (id<SOCKotlinx_coroutines_coreChildHandle>)attachChildChild:(id<SOCKotlinx_coroutines_coreChildJob>)child __attribute__((swift_name("attachChild(child:)")));
- (void)cancelCause:(SOCKotlinCancellationException * _Nullable)cause __attribute__((swift_name("cancel(cause:)")));
- (SOCKotlinCancellationException *)getCancellationException __attribute__((swift_name("getCancellationException()")));
- (id<SOCKotlinx_coroutines_coreDisposableHandle>)invokeOnCompletionOnCancelling:(BOOL)onCancelling invokeImmediately:(BOOL)invokeImmediately handler:(void (^)(SOCKotlinThrowable * _Nullable))handler __attribute__((swift_name("invokeOnCompletion(onCancelling:invokeImmediately:handler:)")));
- (id<SOCKotlinx_coroutines_coreDisposableHandle>)invokeOnCompletionHandler:(void (^)(SOCKotlinThrowable * _Nullable))handler __attribute__((swift_name("invokeOnCompletion(handler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)joinWithCompletionHandler:(void (^)(SOCKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("join(completionHandler:)")));
- (id<SOCKotlinx_coroutines_coreJob>)plusOther:(id<SOCKotlinx_coroutines_coreJob>)other __attribute__((swift_name("plus(other:)"))) __attribute__((unavailable("Operator '+' on two Job objects is meaningless. Job is a coroutine context element and `+` is a set-sum operator for coroutine contexts. The job to the right of `+` just replaces the job the left of `+`.")));
- (BOOL)start __attribute__((swift_name("start()")));
@property (readonly) id<SOCKotlinSequence> children __attribute__((swift_name("children")));
@property (readonly) BOOL isActive __attribute__((swift_name("isActive")));
@property (readonly) BOOL isCancelled __attribute__((swift_name("isCancelled")));
@property (readonly) BOOL isCompleted __attribute__((swift_name("isCompleted")));
@property (readonly) id<SOCKotlinx_coroutines_coreSelectClause0> onJoin __attribute__((swift_name("onJoin")));
@end;

__attribute__((swift_name("Mvvm_livedataLiveData")))
@interface SOCMvvm_livedataLiveData<T> : SOCBase
- (instancetype)initWithInitialValue:(T _Nullable)initialValue __attribute__((swift_name("init(initialValue:)"))) __attribute__((objc_designated_initializer));
- (void)addObserverObserver:(void (^)(T _Nullable))observer __attribute__((swift_name("addObserver(observer:)")));
- (void)changeValueValue:(T _Nullable)value __attribute__((swift_name("changeValue(value:)")));
- (void)removeObserverObserver:(void (^)(T _Nullable))observer __attribute__((swift_name("removeObserver(observer:)")));
@property (readonly) T _Nullable value __attribute__((swift_name("value")));
@end;

__attribute__((swift_name("Mvvm_livedataMutableLiveData")))
@interface SOCMvvm_livedataMutableLiveData<T> : SOCMvvm_livedataLiveData<T>
- (instancetype)initWithInitialValue:(T _Nullable)initialValue __attribute__((swift_name("init(initialValue:)"))) __attribute__((objc_designated_initializer));
- (void)postValueValue:(T _Nullable)value __attribute__((swift_name("postValue(value:)")));
@property T _Nullable value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinPair")))
@interface SOCKotlinPair<__covariant A, __covariant B> : SOCBase
- (instancetype)initWithFirst:(A _Nullable)first second:(B _Nullable)second __attribute__((swift_name("init(first:second:)"))) __attribute__((objc_designated_initializer));
- (A _Nullable)component1 __attribute__((swift_name("component1()")));
- (B _Nullable)component2 __attribute__((swift_name("component2()")));
- (SOCKotlinPair<A, B> *)doCopyFirst:(A _Nullable)first second:(B _Nullable)second __attribute__((swift_name("doCopy(first:second:)")));
- (BOOL)equalsOther:(id _Nullable)other __attribute__((swift_name("equals(other:)")));
- (int32_t)hashCode __attribute__((swift_name("hashCode()")));
- (NSString *)toString __attribute__((swift_name("toString()")));
@property (readonly) A _Nullable first __attribute__((swift_name("first")));
@property (readonly) B _Nullable second __attribute__((swift_name("second")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerialFormat")))
@protocol SOCKotlinx_serialization_coreSerialFormat
@required
@property (readonly) SOCKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreStringFormat")))
@protocol SOCKotlinx_serialization_coreStringFormat <SOCKotlinx_serialization_coreSerialFormat>
@required
- (id _Nullable)decodeFromStringDeserializer:(id<SOCKotlinx_serialization_coreDeserializationStrategy>)deserializer string:(NSString *)string __attribute__((swift_name("decodeFromString(deserializer:string:)")));
- (NSString *)encodeToStringSerializer:(id<SOCKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeToString(serializer:value:)")));
@end;

__attribute__((swift_name("Kotlinx_serialization_jsonJson")))
@interface SOCKotlinx_serialization_jsonJson : SOCBase <SOCKotlinx_serialization_coreStringFormat>
- (instancetype)initWithConfiguration:(SOCKotlinx_serialization_jsonJsonConfiguration *)configuration serializersModule:(SOCKotlinx_serialization_coreSerializersModule *)serializersModule __attribute__((swift_name("init(configuration:serializersModule:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCKotlinx_serialization_jsonJsonDefault *companion __attribute__((swift_name("companion")));
- (id _Nullable)decodeFromJsonElementDeserializer:(id<SOCKotlinx_serialization_coreDeserializationStrategy>)deserializer element:(SOCKotlinx_serialization_jsonJsonElement *)element __attribute__((swift_name("decodeFromJsonElement(deserializer:element:)")));
- (id _Nullable)decodeFromStringDeserializer:(id<SOCKotlinx_serialization_coreDeserializationStrategy>)deserializer string:(NSString *)string __attribute__((swift_name("decodeFromString(deserializer:string:)")));
- (SOCKotlinx_serialization_jsonJsonElement *)encodeToJsonElementSerializer:(id<SOCKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeToJsonElement(serializer:value:)")));
- (NSString *)encodeToStringSerializer:(id<SOCKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeToString(serializer:value:)")));
- (SOCKotlinx_serialization_jsonJsonElement *)parseToJsonElementString:(NSString *)string __attribute__((swift_name("parseToJsonElement(string:)")));
@property (readonly) SOCKotlinx_serialization_jsonJsonConfiguration *configuration __attribute__((swift_name("configuration")));
@property (readonly) SOCKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("KotlinThrowable")))
@interface SOCKotlinThrowable : SOCBase
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(SOCKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(SOCKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (SOCKotlinArray<NSString *> *)getStackTrace __attribute__((swift_name("getStackTrace()")));
- (void)printStackTrace __attribute__((swift_name("printStackTrace()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SOCKotlinThrowable * _Nullable cause __attribute__((swift_name("cause")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
- (NSError *)asError __attribute__((swift_name("asError()")));
@end;

__attribute__((swift_name("KotlinException")))
@interface SOCKotlinException : SOCKotlinThrowable
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(SOCKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(SOCKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("KotlinRuntimeException")))
@interface SOCKotlinRuntimeException : SOCKotlinException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(SOCKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(SOCKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("KotlinIllegalStateException")))
@interface SOCKotlinIllegalStateException : SOCKotlinRuntimeException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(SOCKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(SOCKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("KotlinCancellationException")))
@interface SOCKotlinCancellationException : SOCKotlinIllegalStateException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(SOCKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(SOCKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("Ktor_utilsStringValuesBuilder")))
@interface SOCKtor_utilsStringValuesBuilder : SOCBase
- (instancetype)initWithCaseInsensitiveName:(BOOL)caseInsensitiveName size:(int32_t)size __attribute__((swift_name("init(caseInsensitiveName:size:)"))) __attribute__((objc_designated_initializer));
- (void)appendName:(NSString *)name value:(NSString *)value __attribute__((swift_name("append(name:value:)")));
- (void)appendAllStringValues:(id<SOCKtor_utilsStringValues>)stringValues __attribute__((swift_name("appendAll(stringValues:)")));
- (void)appendAllName:(NSString *)name values:(id)values __attribute__((swift_name("appendAll(name:values:)")));
- (void)appendMissingStringValues:(id<SOCKtor_utilsStringValues>)stringValues __attribute__((swift_name("appendMissing(stringValues:)")));
- (void)appendMissingName:(NSString *)name values:(id)values __attribute__((swift_name("appendMissing(name:values:)")));
- (id<SOCKtor_utilsStringValues>)build __attribute__((swift_name("build()")));
- (void)clear __attribute__((swift_name("clear()")));
- (BOOL)containsName:(NSString *)name __attribute__((swift_name("contains(name:)")));
- (BOOL)containsName:(NSString *)name value:(NSString *)value __attribute__((swift_name("contains(name:value:)")));
- (NSSet<id<SOCKotlinMapEntry>> *)entries __attribute__((swift_name("entries()")));
- (NSString * _Nullable)getName:(NSString *)name __attribute__((swift_name("get(name:)")));
- (NSArray<NSString *> * _Nullable)getAllName:(NSString *)name __attribute__((swift_name("getAll(name:)")));
- (BOOL)isEmpty __attribute__((swift_name("isEmpty()")));
- (NSSet<NSString *> *)names __attribute__((swift_name("names()")));
- (void)removeName:(NSString *)name __attribute__((swift_name("remove(name:)")));
- (BOOL)removeName:(NSString *)name value:(NSString *)value __attribute__((swift_name("remove(name:value:)")));
- (void)removeKeysWithNoEntries __attribute__((swift_name("removeKeysWithNoEntries()")));
- (void)setName:(NSString *)name value:(NSString *)value __attribute__((swift_name("set(name:value:)")));
- (void)validateNameName:(NSString *)name __attribute__((swift_name("validateName(name:)")));
- (void)validateValueValue:(NSString *)value __attribute__((swift_name("validateValue(value:)")));
@property BOOL built __attribute__((swift_name("built")));
@property (readonly) BOOL caseInsensitiveName __attribute__((swift_name("caseInsensitiveName")));
@property (readonly) SOCMutableDictionary<NSString *, NSMutableArray<NSString *> *> *values __attribute__((swift_name("values")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHeadersBuilder")))
@interface SOCKtor_httpHeadersBuilder : SOCKtor_utilsStringValuesBuilder
- (instancetype)initWithSize:(int32_t)size __attribute__((swift_name("init(size:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCaseInsensitiveName:(BOOL)caseInsensitiveName size:(int32_t)size __attribute__((swift_name("init(caseInsensitiveName:size:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (id<SOCKtor_httpHeaders>)build __attribute__((swift_name("build()")));
- (void)validateNameName:(NSString *)name __attribute__((swift_name("validateName(name:)")));
- (void)validateValueValue:(NSString *)value __attribute__((swift_name("validateValue(value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpCookie")))
@interface SOCKtor_httpCookie : SOCBase
- (instancetype)initWithName:(NSString *)name value:(NSString *)value encoding:(SOCKtor_httpCookieEncoding *)encoding maxAge:(int32_t)maxAge expires:(SOCKtor_utilsGMTDate * _Nullable)expires domain:(NSString * _Nullable)domain path:(NSString * _Nullable)path secure:(BOOL)secure httpOnly:(BOOL)httpOnly extensions:(NSDictionary<NSString *, id> *)extensions __attribute__((swift_name("init(name:value:encoding:maxAge:expires:domain:path:secure:httpOnly:extensions:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSDictionary<NSString *, id> *)component10 __attribute__((swift_name("component10()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (SOCKtor_httpCookieEncoding *)component3 __attribute__((swift_name("component3()")));
- (int32_t)component4 __attribute__((swift_name("component4()")));
- (SOCKtor_utilsGMTDate * _Nullable)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (NSString * _Nullable)component7 __attribute__((swift_name("component7()")));
- (BOOL)component8 __attribute__((swift_name("component8()")));
- (BOOL)component9 __attribute__((swift_name("component9()")));
- (SOCKtor_httpCookie *)doCopyName:(NSString *)name value:(NSString *)value encoding:(SOCKtor_httpCookieEncoding *)encoding maxAge:(int32_t)maxAge expires:(SOCKtor_utilsGMTDate * _Nullable)expires domain:(NSString * _Nullable)domain path:(NSString * _Nullable)path secure:(BOOL)secure httpOnly:(BOOL)httpOnly extensions:(NSDictionary<NSString *, id> *)extensions __attribute__((swift_name("doCopy(name:value:encoding:maxAge:expires:domain:path:secure:httpOnly:extensions:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable domain __attribute__((swift_name("domain")));
@property (readonly) SOCKtor_httpCookieEncoding *encoding __attribute__((swift_name("encoding")));
@property (readonly) SOCKtor_utilsGMTDate * _Nullable expires __attribute__((swift_name("expires")));
@property (readonly) NSDictionary<NSString *, id> *extensions __attribute__((swift_name("extensions")));
@property (readonly) BOOL httpOnly __attribute__((swift_name("httpOnly")));
@property (readonly) int32_t maxAge __attribute__((swift_name("maxAge")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSString * _Nullable path __attribute__((swift_name("path")));
@property (readonly) BOOL secure __attribute__((swift_name("secure")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpStatement")))
@interface SOCKtor_client_coreHttpStatement : SOCBase
- (instancetype)initWithBuilder:(SOCKtor_client_coreHttpRequestBuilder *)builder client:(SOCKtor_client_coreHttpClient *)client __attribute__((swift_name("init(builder:client:)"))) __attribute__((objc_designated_initializer));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)executeWithCompletionHandler:(void (^)(SOCKtor_client_coreHttpResponse * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("execute(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)executeBlock:(id<SOCKotlinSuspendFunction1>)block completionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("execute(block:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)receiveWithCompletionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("receive(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)receiveBlock:(id<SOCKotlinSuspendFunction1>)block completionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("receive(block:completionHandler:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_jsonJsonElement")))
@interface SOCKotlinx_serialization_jsonJsonElement : SOCBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) SOCKotlinx_serialization_jsonJsonElementCompanion *companion __attribute__((swift_name("companion")));
@end;

__attribute__((swift_name("Ktor_ioCloseable")))
@protocol SOCKtor_ioCloseable
@required
- (void)close __attribute__((swift_name("close()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpClient")))
@interface SOCKtor_client_coreHttpClient : SOCBase <SOCKotlinx_coroutines_coreCoroutineScope, SOCKtor_ioCloseable>
- (instancetype)initWithEngine:(id<SOCKtor_client_coreHttpClientEngine>)engine userConfig:(SOCKtor_client_coreHttpClientConfig<SOCKtor_client_coreHttpClientEngineConfig *> *)userConfig __attribute__((swift_name("init(engine:userConfig:)"))) __attribute__((objc_designated_initializer));
- (void)close __attribute__((swift_name("close()")));
- (SOCKtor_client_coreHttpClient *)configBlock:(void (^)(SOCKtor_client_coreHttpClientConfig<id> *))block __attribute__((swift_name("config(block:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)executeBuilder:(SOCKtor_client_coreHttpRequestBuilder *)builder completionHandler:(void (^)(SOCKtor_client_coreHttpClientCall * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("execute(builder:completionHandler:)"))) __attribute__((unavailable("Unbound [HttpClientCall] is deprecated. Consider using [request<HttpResponse>(builder)] instead.")));
- (BOOL)isSupportedCapability:(id<SOCKtor_client_coreHttpClientEngineCapability>)capability __attribute__((swift_name("isSupported(capability:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<SOCKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) id<SOCKotlinCoroutineContext> coroutineContext __attribute__((swift_name("coroutineContext")));
@property (readonly) SOCKotlinx_coroutines_coreCoroutineDispatcher *dispatcher __attribute__((swift_name("dispatcher"))) __attribute__((unavailable("[dispatcher] is deprecated. Use coroutineContext instead.")));
@property (readonly) id<SOCKtor_client_coreHttpClientEngine> engine __attribute__((swift_name("engine")));
@property (readonly) SOCKtor_client_coreHttpClientEngineConfig *engineConfig __attribute__((swift_name("engineConfig")));
@property (readonly) SOCKtor_client_coreHttpReceivePipeline *receivePipeline __attribute__((swift_name("receivePipeline")));
@property (readonly) SOCKtor_client_coreHttpRequestPipeline *requestPipeline __attribute__((swift_name("requestPipeline")));
@property (readonly) SOCKtor_client_coreHttpResponsePipeline *responsePipeline __attribute__((swift_name("responsePipeline")));
@property (readonly) SOCKtor_client_coreHttpSendPipeline *sendPipeline __attribute__((swift_name("sendPipeline")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinArray")))
@interface SOCKotlinArray<T> : SOCBase
+ (instancetype)arrayWithSize:(int32_t)size init:(T _Nullable (^)(SOCInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (T _Nullable)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (id<SOCKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(T _Nullable)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinByteArray")))
@interface SOCKotlinByteArray : SOCBase
+ (instancetype)arrayWithSize:(int32_t)size __attribute__((swift_name("init(size:)")));
+ (instancetype)arrayWithSize:(int32_t)size init:(SOCByte *(^)(SOCInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (int8_t)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (SOCKotlinByteIterator *)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(int8_t)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreEncoder")))
@protocol SOCKotlinx_serialization_coreEncoder
@required
- (id<SOCKotlinx_serialization_coreCompositeEncoder>)beginCollectionDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor collectionSize:(int32_t)collectionSize __attribute__((swift_name("beginCollection(descriptor:collectionSize:)")));
- (id<SOCKotlinx_serialization_coreCompositeEncoder>)beginStructureDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("beginStructure(descriptor:)")));
- (void)encodeBooleanValue:(BOOL)value __attribute__((swift_name("encodeBoolean(value:)")));
- (void)encodeByteValue:(int8_t)value __attribute__((swift_name("encodeByte(value:)")));
- (void)encodeCharValue:(unichar)value __attribute__((swift_name("encodeChar(value:)")));
- (void)encodeDoubleValue:(double)value __attribute__((swift_name("encodeDouble(value:)")));
- (void)encodeEnumEnumDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)enumDescriptor index:(int32_t)index __attribute__((swift_name("encodeEnum(enumDescriptor:index:)")));
- (void)encodeFloatValue:(float)value __attribute__((swift_name("encodeFloat(value:)")));
- (id<SOCKotlinx_serialization_coreEncoder>)encodeInlineInlineDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)inlineDescriptor __attribute__((swift_name("encodeInline(inlineDescriptor:)")));
- (void)encodeIntValue:(int32_t)value __attribute__((swift_name("encodeInt(value:)")));
- (void)encodeLongValue:(int64_t)value __attribute__((swift_name("encodeLong(value:)")));
- (void)encodeNotNullMark __attribute__((swift_name("encodeNotNullMark()")));
- (void)encodeNull __attribute__((swift_name("encodeNull()")));
- (void)encodeNullableSerializableValueSerializer:(id<SOCKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableValue(serializer:value:)")));
- (void)encodeSerializableValueSerializer:(id<SOCKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableValue(serializer:value:)")));
- (void)encodeShortValue:(int16_t)value __attribute__((swift_name("encodeShort(value:)")));
- (void)encodeStringValue:(NSString *)value __attribute__((swift_name("encodeString(value:)")));
@property (readonly) SOCKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerialDescriptor")))
@protocol SOCKotlinx_serialization_coreSerialDescriptor
@required
- (NSArray<id<SOCKotlinAnnotation>> *)getElementAnnotationsIndex:(int32_t)index __attribute__((swift_name("getElementAnnotations(index:)")));
- (id<SOCKotlinx_serialization_coreSerialDescriptor>)getElementDescriptorIndex:(int32_t)index __attribute__((swift_name("getElementDescriptor(index:)")));
- (int32_t)getElementIndexName:(NSString *)name __attribute__((swift_name("getElementIndex(name:)")));
- (NSString *)getElementNameIndex:(int32_t)index __attribute__((swift_name("getElementName(index:)")));
- (BOOL)isElementOptionalIndex:(int32_t)index __attribute__((swift_name("isElementOptional(index:)")));
@property (readonly) NSArray<id<SOCKotlinAnnotation>> *annotations __attribute__((swift_name("annotations")));
@property (readonly) int32_t elementsCount __attribute__((swift_name("elementsCount")));
@property (readonly) BOOL isInline __attribute__((swift_name("isInline")));
@property (readonly) BOOL isNullable __attribute__((swift_name("isNullable")));
@property (readonly) SOCKotlinx_serialization_coreSerialKind *kind __attribute__((swift_name("kind")));
@property (readonly) NSString *serialName __attribute__((swift_name("serialName")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreDecoder")))
@protocol SOCKotlinx_serialization_coreDecoder
@required
- (id<SOCKotlinx_serialization_coreCompositeDecoder>)beginStructureDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("beginStructure(descriptor:)")));
- (BOOL)decodeBoolean __attribute__((swift_name("decodeBoolean()")));
- (int8_t)decodeByte __attribute__((swift_name("decodeByte()")));
- (unichar)decodeChar __attribute__((swift_name("decodeChar()")));
- (double)decodeDouble __attribute__((swift_name("decodeDouble()")));
- (int32_t)decodeEnumEnumDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)enumDescriptor __attribute__((swift_name("decodeEnum(enumDescriptor:)")));
- (float)decodeFloat __attribute__((swift_name("decodeFloat()")));
- (id<SOCKotlinx_serialization_coreDecoder>)decodeInlineInlineDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)inlineDescriptor __attribute__((swift_name("decodeInline(inlineDescriptor:)")));
- (int32_t)decodeInt __attribute__((swift_name("decodeInt()")));
- (int64_t)decodeLong __attribute__((swift_name("decodeLong()")));
- (BOOL)decodeNotNullMark __attribute__((swift_name("decodeNotNullMark()")));
- (SOCKotlinNothing * _Nullable)decodeNull __attribute__((swift_name("decodeNull()")));
- (id _Nullable)decodeNullableSerializableValueDeserializer:(id<SOCKotlinx_serialization_coreDeserializationStrategy>)deserializer __attribute__((swift_name("decodeNullableSerializableValue(deserializer:)")));
- (id _Nullable)decodeSerializableValueDeserializer:(id<SOCKotlinx_serialization_coreDeserializationStrategy>)deserializer __attribute__((swift_name("decodeSerializableValue(deserializer:)")));
- (int16_t)decodeShort __attribute__((swift_name("decodeShort()")));
- (NSString *)decodeString __attribute__((swift_name("decodeString()")));
@property (readonly) SOCKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("KotlinKDeclarationContainer")))
@protocol SOCKotlinKDeclarationContainer
@required
@end;

__attribute__((swift_name("KotlinKAnnotatedElement")))
@protocol SOCKotlinKAnnotatedElement
@required
@end;

__attribute__((swift_name("KotlinKClassifier")))
@protocol SOCKotlinKClassifier
@required
@end;

__attribute__((swift_name("KotlinKClass")))
@protocol SOCKotlinKClass <SOCKotlinKDeclarationContainer, SOCKotlinKAnnotatedElement, SOCKotlinKClassifier>
@required
- (BOOL)isInstanceValue:(id _Nullable)value __attribute__((swift_name("isInstance(value:)")));
@property (readonly) NSString * _Nullable qualifiedName __attribute__((swift_name("qualifiedName")));
@property (readonly) NSString * _Nullable simpleName __attribute__((swift_name("simpleName")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientEngineFactory")))
@protocol SOCKtor_client_coreHttpClientEngineFactory
@required
- (id<SOCKtor_client_coreHttpClientEngine>)createBlock:(void (^)(SOCKtor_client_coreHttpClientEngineConfig *))block __attribute__((swift_name("create(block:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreKoin")))
@interface SOCKoin_coreKoin : SOCBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)close __attribute__((swift_name("close()")));
- (void)createEagerInstances __attribute__((swift_name("createEagerInstances()")));
- (SOCKoin_coreScope *)createScopeT:(id<SOCKoin_coreKoinScopeComponent>)t __attribute__((swift_name("createScope(t:)")));
- (SOCKoin_coreScope *)createScopeScopeId:(NSString *)scopeId __attribute__((swift_name("createScope(scopeId:)")));
- (SOCKoin_coreScope *)createScopeScopeId:(NSString *)scopeId source:(id _Nullable)source __attribute__((swift_name("createScope(scopeId:source:)")));
- (SOCKoin_coreScope *)createScopeScopeId:(NSString *)scopeId qualifier:(id<SOCKoin_coreQualifier>)qualifier source:(id _Nullable)source __attribute__((swift_name("createScope(scopeId:qualifier:source:)")));
- (void)declareInstance:(id _Nullable)instance qualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier secondaryTypes:(NSArray<id<SOCKotlinKClass>> *)secondaryTypes allowOverride:(BOOL)allowOverride __attribute__((swift_name("declare(instance:qualifier:secondaryTypes:allowOverride:)")));
- (void)deletePropertyKey:(NSString *)key __attribute__((swift_name("deleteProperty(key:)")));
- (void)deleteScopeScopeId:(NSString *)scopeId __attribute__((swift_name("deleteScope(scopeId:)")));
- (id _Nullable)getClazz:(id<SOCKotlinKClass>)clazz qualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier parameters:(SOCKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("get(clazz:qualifier:parameters:)")));
- (id)getQualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier parameters:(SOCKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("get(qualifier:parameters:)")));
- (NSArray<id> *)getAll __attribute__((swift_name("getAll()")));
- (SOCKoin_coreScope *)getOrCreateScopeScopeId:(NSString *)scopeId __attribute__((swift_name("getOrCreateScope(scopeId:)")));
- (SOCKoin_coreScope *)getOrCreateScopeScopeId:(NSString *)scopeId qualifier:(id<SOCKoin_coreQualifier>)qualifier source:(id _Nullable)source __attribute__((swift_name("getOrCreateScope(scopeId:qualifier:source:)")));
- (id _Nullable)getOrNullClazz:(id<SOCKotlinKClass>)clazz qualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier parameters:(SOCKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("getOrNull(clazz:qualifier:parameters:)")));
- (id _Nullable)getOrNullQualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier parameters:(SOCKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("getOrNull(qualifier:parameters:)")));
- (id _Nullable)getPropertyKey:(NSString *)key __attribute__((swift_name("getProperty(key:)")));
- (id)getPropertyKey:(NSString *)key defaultValue:(id)defaultValue __attribute__((swift_name("getProperty(key:defaultValue:)")));
- (SOCKoin_coreScope *)getScopeScopeId:(NSString *)scopeId __attribute__((swift_name("getScope(scopeId:)")));
- (SOCKoin_coreScope * _Nullable)getScopeOrNullScopeId:(NSString *)scopeId __attribute__((swift_name("getScopeOrNull(scopeId:)")));
- (id<SOCKotlinLazy>)injectQualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier mode:(SOCKotlinLazyThreadSafetyMode *)mode parameters:(SOCKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("inject(qualifier:mode:parameters:)")));
- (id<SOCKotlinLazy>)injectOrNullQualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier mode:(SOCKotlinLazyThreadSafetyMode *)mode parameters:(SOCKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("injectOrNull(qualifier:mode:parameters:)")));
- (void)loadModulesModules:(NSArray<SOCKoin_coreModule *> *)modules allowOverride:(BOOL)allowOverride __attribute__((swift_name("loadModules(modules:allowOverride:)")));
- (void)setPropertyKey:(NSString *)key value:(id)value __attribute__((swift_name("setProperty(key:value:)")));
- (void)setupLoggerLogger:(SOCKoin_coreLogger *)logger __attribute__((swift_name("setupLogger(logger:)")));
- (void)unloadModulesModules:(NSArray<SOCKoin_coreModule *> *)modules __attribute__((swift_name("unloadModules(modules:)")));
@property (readonly) SOCKoin_coreInstanceRegistry *instanceRegistry __attribute__((swift_name("instanceRegistry")));
@property (readonly) SOCKoin_coreLogger *logger __attribute__((swift_name("logger")));
@property (readonly) SOCKoin_corePropertyRegistry *propertyRegistry __attribute__((swift_name("propertyRegistry")));
@property (readonly) SOCKoin_coreScopeRegistry *scopeRegistry __attribute__((swift_name("scopeRegistry")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreModule")))
@interface SOCKoin_coreModule : SOCBase
- (instancetype)initWithCreatedAtStart:(BOOL)createdAtStart __attribute__((swift_name("init(createdAtStart:)"))) __attribute__((objc_designated_initializer));
- (SOCKotlinPair<SOCKoin_coreModule *, SOCKoin_coreInstanceFactory<id> *> *)factoryQualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier definition:(id _Nullable (^)(SOCKoin_coreScope *, SOCKoin_coreParametersHolder *))definition __attribute__((swift_name("factory(qualifier:definition:)")));
- (NSArray<SOCKoin_coreModule *> *)plusModules:(NSArray<SOCKoin_coreModule *> *)modules __attribute__((swift_name("plus(modules:)")));
- (NSArray<SOCKoin_coreModule *> *)plusModule:(SOCKoin_coreModule *)module __attribute__((swift_name("plus(module:)")));
- (void)scopeQualifier:(id<SOCKoin_coreQualifier>)qualifier scopeSet:(void (^)(SOCKoin_coreScopeDSL *))scopeSet __attribute__((swift_name("scope(qualifier:scopeSet:)")));
- (void)scopeScopeSet:(void (^)(SOCKoin_coreScopeDSL *))scopeSet __attribute__((swift_name("scope(scopeSet:)")));
- (SOCKotlinPair<SOCKoin_coreModule *, SOCKoin_coreInstanceFactory<id> *> *)singleQualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier createdAtStart:(BOOL)createdAtStart definition:(id _Nullable (^)(SOCKoin_coreScope *, SOCKoin_coreParametersHolder *))definition __attribute__((swift_name("single(qualifier:createdAtStart:definition:)")));
@property (readonly) BOOL createdAtStart __attribute__((swift_name("createdAtStart")));
@property (readonly) SOCMutableSet<SOCKoin_coreSingleInstanceFactory<id> *> *eagerInstances __attribute__((swift_name("eagerInstances")));
@property (readonly) BOOL isLoaded __attribute__((swift_name("isLoaded")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreDisposableHandle")))
@protocol SOCKotlinx_coroutines_coreDisposableHandle
@required
- (void)dispose __attribute__((swift_name("dispose()")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreChildHandle")))
@protocol SOCKotlinx_coroutines_coreChildHandle <SOCKotlinx_coroutines_coreDisposableHandle>
@required
- (BOOL)childCancelledCause:(SOCKotlinThrowable *)cause __attribute__((swift_name("childCancelled(cause:)")));
@property (readonly) id<SOCKotlinx_coroutines_coreJob> _Nullable parent __attribute__((swift_name("parent")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreChildJob")))
@protocol SOCKotlinx_coroutines_coreChildJob <SOCKotlinx_coroutines_coreJob>
@required
- (void)parentCancelledParentJob:(id<SOCKotlinx_coroutines_coreParentJob>)parentJob __attribute__((swift_name("parentCancelled(parentJob:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinUnit")))
@interface SOCKotlinUnit : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)unit __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKotlinUnit *shared __attribute__((swift_name("shared")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("KotlinSequence")))
@protocol SOCKotlinSequence
@required
- (id<SOCKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreSelectClause0")))
@protocol SOCKotlinx_coroutines_coreSelectClause0
@required
- (void)registerSelectClause0Select:(id<SOCKotlinx_coroutines_coreSelectInstance>)select block:(id<SOCKotlinSuspendFunction0>)block __attribute__((swift_name("registerSelectClause0(select:block:)")));
@end;

__attribute__((swift_name("KotlinCoroutineContextKey")))
@protocol SOCKotlinCoroutineContextKey
@required
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerializersModule")))
@interface SOCKotlinx_serialization_coreSerializersModule : SOCBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)dumpToCollector:(id<SOCKotlinx_serialization_coreSerializersModuleCollector>)collector __attribute__((swift_name("dumpTo(collector:)")));
- (id<SOCKotlinx_serialization_coreKSerializer> _Nullable)getContextualKClass:(id<SOCKotlinKClass>)kClass typeArgumentsSerializers:(NSArray<id<SOCKotlinx_serialization_coreKSerializer>> *)typeArgumentsSerializers __attribute__((swift_name("getContextual(kClass:typeArgumentsSerializers:)")));
- (id<SOCKotlinx_serialization_coreSerializationStrategy> _Nullable)getPolymorphicBaseClass:(id<SOCKotlinKClass>)baseClass value:(id)value __attribute__((swift_name("getPolymorphic(baseClass:value:)")));
- (id<SOCKotlinx_serialization_coreDeserializationStrategy> _Nullable)getPolymorphicBaseClass:(id<SOCKotlinKClass>)baseClass serializedClassName:(NSString * _Nullable)serializedClassName __attribute__((swift_name("getPolymorphic(baseClass:serializedClassName:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_jsonJsonConfiguration")))
@interface SOCKotlinx_serialization_jsonJsonConfiguration : SOCBase
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL allowSpecialFloatingPointValues __attribute__((swift_name("allowSpecialFloatingPointValues")));
@property (readonly) BOOL allowStructuredMapKeys __attribute__((swift_name("allowStructuredMapKeys")));
@property (readonly) NSString *classDiscriminator __attribute__((swift_name("classDiscriminator")));
@property (readonly) BOOL coerceInputValues __attribute__((swift_name("coerceInputValues")));
@property (readonly) BOOL encodeDefaults __attribute__((swift_name("encodeDefaults")));
@property (readonly) BOOL ignoreUnknownKeys __attribute__((swift_name("ignoreUnknownKeys")));
@property (readonly) BOOL isLenient __attribute__((swift_name("isLenient")));
@property (readonly) BOOL prettyPrint __attribute__((swift_name("prettyPrint")));
@property (readonly) NSString *prettyPrintIndent __attribute__((swift_name("prettyPrintIndent")));
@property (readonly) BOOL useAlternativeNames __attribute__((swift_name("useAlternativeNames")));
@property (readonly) BOOL useArrayPolymorphism __attribute__((swift_name("useArrayPolymorphism")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_jsonJson.Default")))
@interface SOCKotlinx_serialization_jsonJsonDefault : SOCKotlinx_serialization_jsonJson
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithConfiguration:(SOCKotlinx_serialization_jsonJsonConfiguration *)configuration serializersModule:(SOCKotlinx_serialization_coreSerializersModule *)serializersModule __attribute__((swift_name("init(configuration:serializersModule:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)default_ __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKotlinx_serialization_jsonJsonDefault *shared __attribute__((swift_name("shared")));
@end;

__attribute__((swift_name("Ktor_utilsStringValues")))
@protocol SOCKtor_utilsStringValues
@required
- (BOOL)containsName:(NSString *)name __attribute__((swift_name("contains(name:)")));
- (BOOL)containsName:(NSString *)name value:(NSString *)value __attribute__((swift_name("contains(name:value:)")));
- (NSSet<id<SOCKotlinMapEntry>> *)entries __attribute__((swift_name("entries()")));
- (void)forEachBody:(void (^)(NSString *, NSArray<NSString *> *))body __attribute__((swift_name("forEach(body:)")));
- (NSString * _Nullable)getName:(NSString *)name __attribute__((swift_name("get(name:)")));
- (NSArray<NSString *> * _Nullable)getAllName:(NSString *)name __attribute__((swift_name("getAll(name:)")));
- (BOOL)isEmpty __attribute__((swift_name("isEmpty()")));
- (NSSet<NSString *> *)names __attribute__((swift_name("names()")));
@property (readonly) BOOL caseInsensitiveName __attribute__((swift_name("caseInsensitiveName")));
@end;

__attribute__((swift_name("KotlinMapEntry")))
@protocol SOCKotlinMapEntry
@required
@property (readonly) id _Nullable key __attribute__((swift_name("key")));
@property (readonly) id _Nullable value __attribute__((swift_name("value")));
@end;

__attribute__((swift_name("Ktor_httpHeaders")))
@protocol SOCKtor_httpHeaders <SOCKtor_utilsStringValues>
@required
@end;

__attribute__((swift_name("KotlinComparable")))
@protocol SOCKotlinComparable
@required
- (int32_t)compareToOther:(id _Nullable)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("KotlinEnum")))
@interface SOCKotlinEnum<E> : SOCBase <SOCKotlinComparable>
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCKotlinEnumCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)compareToOther:(E)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) int32_t ordinal __attribute__((swift_name("ordinal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpCookieEncoding")))
@interface SOCKtor_httpCookieEncoding : SOCKotlinEnum<SOCKtor_httpCookieEncoding *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) SOCKtor_httpCookieEncoding *raw __attribute__((swift_name("raw")));
@property (class, readonly) SOCKtor_httpCookieEncoding *dquotes __attribute__((swift_name("dquotes")));
@property (class, readonly) SOCKtor_httpCookieEncoding *uriEncoding __attribute__((swift_name("uriEncoding")));
@property (class, readonly) SOCKtor_httpCookieEncoding *base64Encoding __attribute__((swift_name("base64Encoding")));
+ (SOCKotlinArray<SOCKtor_httpCookieEncoding *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsGMTDate")))
@interface SOCKtor_utilsGMTDate : SOCBase <SOCKotlinComparable>
@property (class, readonly, getter=companion) SOCKtor_utilsGMTDateCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)compareToOther:(SOCKtor_utilsGMTDate *)other __attribute__((swift_name("compareTo(other:)")));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (SOCKtor_utilsWeekDay *)component4 __attribute__((swift_name("component4()")));
- (int32_t)component5 __attribute__((swift_name("component5()")));
- (int32_t)component6 __attribute__((swift_name("component6()")));
- (SOCKtor_utilsMonth *)component7 __attribute__((swift_name("component7()")));
- (int32_t)component8 __attribute__((swift_name("component8()")));
- (int64_t)component9 __attribute__((swift_name("component9()")));
- (SOCKtor_utilsGMTDate *)doCopySeconds:(int32_t)seconds minutes:(int32_t)minutes hours:(int32_t)hours dayOfWeek:(SOCKtor_utilsWeekDay *)dayOfWeek dayOfMonth:(int32_t)dayOfMonth dayOfYear:(int32_t)dayOfYear month:(SOCKtor_utilsMonth *)month year:(int32_t)year timestamp:(int64_t)timestamp __attribute__((swift_name("doCopy(seconds:minutes:hours:dayOfWeek:dayOfMonth:dayOfYear:month:year:timestamp:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t dayOfMonth __attribute__((swift_name("dayOfMonth")));
@property (readonly) SOCKtor_utilsWeekDay *dayOfWeek __attribute__((swift_name("dayOfWeek")));
@property (readonly) int32_t dayOfYear __attribute__((swift_name("dayOfYear")));
@property (readonly) int32_t hours __attribute__((swift_name("hours")));
@property (readonly) int32_t minutes __attribute__((swift_name("minutes")));
@property (readonly) SOCKtor_utilsMonth *month __attribute__((swift_name("month")));
@property (readonly) int32_t seconds __attribute__((swift_name("seconds")));
@property (readonly) int64_t timestamp __attribute__((swift_name("timestamp")));
@property (readonly) int32_t year __attribute__((swift_name("year")));
@end;

__attribute__((swift_name("Ktor_httpHttpMessageBuilder")))
@protocol SOCKtor_httpHttpMessageBuilder
@required
@property (readonly) SOCKtor_httpHeadersBuilder *headers __attribute__((swift_name("headers")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestBuilder")))
@interface SOCKtor_client_coreHttpRequestBuilder : SOCBase <SOCKtor_httpHttpMessageBuilder>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) SOCKtor_client_coreHttpRequestBuilderCompanion *companion __attribute__((swift_name("companion")));
- (SOCKtor_client_coreHttpRequestData *)build __attribute__((swift_name("build()")));
- (id _Nullable)getCapabilityOrNullKey:(id<SOCKtor_client_coreHttpClientEngineCapability>)key __attribute__((swift_name("getCapabilityOrNull(key:)")));
- (void)setAttributesBlock:(void (^)(id<SOCKtor_utilsAttributes>))block __attribute__((swift_name("setAttributes(block:)")));
- (void)setCapabilityKey:(id<SOCKtor_client_coreHttpClientEngineCapability>)key capability:(id)capability __attribute__((swift_name("setCapability(key:capability:)")));
- (SOCKtor_client_coreHttpRequestBuilder *)takeFromBuilder:(SOCKtor_client_coreHttpRequestBuilder *)builder __attribute__((swift_name("takeFrom(builder:)")));
- (SOCKtor_client_coreHttpRequestBuilder *)takeFromWithExecutionContextBuilder:(SOCKtor_client_coreHttpRequestBuilder *)builder __attribute__((swift_name("takeFromWithExecutionContext(builder:)")));
- (void)urlBlock:(void (^)(SOCKtor_httpURLBuilder *, SOCKtor_httpURLBuilder *))block __attribute__((swift_name("url(block:)")));
@property (readonly) id<SOCKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property id body __attribute__((swift_name("body")));
@property (readonly) id<SOCKotlinx_coroutines_coreJob> executionContext __attribute__((swift_name("executionContext")));
@property (readonly) SOCKtor_httpHeadersBuilder *headers __attribute__((swift_name("headers")));
@property SOCKtor_httpHttpMethod *method __attribute__((swift_name("method")));
@property (readonly) SOCKtor_httpURLBuilder *url __attribute__((swift_name("url")));
@end;

__attribute__((swift_name("Ktor_httpHttpMessage")))
@protocol SOCKtor_httpHttpMessage
@required
@property (readonly) id<SOCKtor_httpHeaders> headers __attribute__((swift_name("headers")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpResponse")))
@interface SOCKtor_client_coreHttpResponse : SOCBase <SOCKtor_httpHttpMessage, SOCKotlinx_coroutines_coreCoroutineScope>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SOCKtor_client_coreHttpClientCall *call __attribute__((swift_name("call")));
@property (readonly) id<SOCKtor_ioByteReadChannel> content __attribute__((swift_name("content")));
@property (readonly) SOCKtor_utilsGMTDate *requestTime __attribute__((swift_name("requestTime")));
@property (readonly) SOCKtor_utilsGMTDate *responseTime __attribute__((swift_name("responseTime")));
@property (readonly) SOCKtor_httpHttpStatusCode *status __attribute__((swift_name("status")));
@property (readonly) SOCKtor_httpHttpProtocolVersion *version __attribute__((swift_name("version")));
@end;

__attribute__((swift_name("KotlinFunction")))
@protocol SOCKotlinFunction
@required
@end;

__attribute__((swift_name("KotlinSuspendFunction1")))
@protocol SOCKotlinSuspendFunction1 <SOCKotlinFunction>
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeP1:(id _Nullable)p1 completionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(p1:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_jsonJsonElement.Companion")))
@interface SOCKotlinx_serialization_jsonJsonElementCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKotlinx_serialization_jsonJsonElementCompanion *shared __attribute__((swift_name("shared")));
- (id<SOCKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientEngine")))
@protocol SOCKtor_client_coreHttpClientEngine <SOCKotlinx_coroutines_coreCoroutineScope, SOCKtor_ioCloseable>
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)executeData:(SOCKtor_client_coreHttpRequestData *)data completionHandler:(void (^)(SOCKtor_client_coreHttpResponseData * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("execute(data:completionHandler:)")));
- (void)installClient:(SOCKtor_client_coreHttpClient *)client __attribute__((swift_name("install(client:)")));
@property (readonly) SOCKtor_client_coreHttpClientEngineConfig *config __attribute__((swift_name("config")));
@property (readonly) SOCKotlinx_coroutines_coreCoroutineDispatcher *dispatcher __attribute__((swift_name("dispatcher")));
@property (readonly) NSSet<id<SOCKtor_client_coreHttpClientEngineCapability>> *supportedCapabilities __attribute__((swift_name("supportedCapabilities")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientEngineConfig")))
@interface SOCKtor_client_coreHttpClientEngineConfig : SOCBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property BOOL pipelining __attribute__((swift_name("pipelining")));
@property SOCKtor_client_coreProxyConfig * _Nullable proxy __attribute__((swift_name("proxy")));
@property (readonly) SOCKotlinNothing *response __attribute__((swift_name("response"))) __attribute__((unavailable("Response config is deprecated. See [HttpPlainText] feature for charset configuration")));
@property int32_t threadsCount __attribute__((swift_name("threadsCount")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpClientConfig")))
@interface SOCKtor_client_coreHttpClientConfig<T> : SOCBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (SOCKtor_client_coreHttpClientConfig<T> *)clone __attribute__((swift_name("clone()")));
- (void)engineBlock:(void (^)(T))block __attribute__((swift_name("engine(block:)")));
- (void)installClient:(SOCKtor_client_coreHttpClient *)client __attribute__((swift_name("install(client:)")));
- (void)installFeature:(id<SOCKtor_client_coreHttpClientFeature>)feature configure:(void (^)(id))configure __attribute__((swift_name("install(feature:configure:)")));
- (void)installKey:(NSString *)key block:(void (^)(SOCKtor_client_coreHttpClient *))block __attribute__((swift_name("install(key:block:)")));
- (void)plusAssignOther:(SOCKtor_client_coreHttpClientConfig<T> *)other __attribute__((swift_name("plusAssign(other:)")));
@property BOOL developmentMode __attribute__((swift_name("developmentMode")));
@property BOOL expectSuccess __attribute__((swift_name("expectSuccess")));
@property BOOL followRedirects __attribute__((swift_name("followRedirects")));
@property BOOL useDefaultTransformers __attribute__((swift_name("useDefaultTransformers")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientCall")))
@interface SOCKtor_client_coreHttpClientCall : SOCBase <SOCKotlinx_coroutines_coreCoroutineScope>
@property (class, readonly, getter=companion) SOCKtor_client_coreHttpClientCallCompanion *companion __attribute__((swift_name("companion")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getResponseContentWithCompletionHandler:(void (^)(id<SOCKtor_ioByteReadChannel> _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getResponseContent(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)receiveInfo:(SOCKtor_client_coreTypeInfo *)info completionHandler:(void (^)(id _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("receive(info:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)receiveInfo:(id<SOCKtor_utilsTypeInfo>)info completionHandler_:(void (^)(id _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("receive(info:completionHandler_:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL allowDoubleReceive __attribute__((swift_name("allowDoubleReceive")));
@property (readonly) id<SOCKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) SOCKtor_client_coreHttpClient * _Nullable client __attribute__((swift_name("client")));
@property (readonly) id<SOCKotlinCoroutineContext> coroutineContext __attribute__((swift_name("coroutineContext")));
@property (readonly) id<SOCKtor_client_coreHttpRequest> request __attribute__((swift_name("request")));
@property (readonly) SOCKtor_client_coreHttpResponse *response __attribute__((swift_name("response")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientEngineCapability")))
@protocol SOCKtor_client_coreHttpClientEngineCapability
@required
@end;

__attribute__((swift_name("Ktor_utilsAttributes")))
@protocol SOCKtor_utilsAttributes
@required
- (id)computeIfAbsentKey:(SOCKtor_utilsAttributeKey<id> *)key block:(id (^)(void))block __attribute__((swift_name("computeIfAbsent(key:block:)")));
- (BOOL)containsKey:(SOCKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("contains(key:)")));
- (id)getKey_:(SOCKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("get(key_:)")));
- (id _Nullable)getOrNullKey:(SOCKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("getOrNull(key:)")));
- (void)putKey:(SOCKtor_utilsAttributeKey<id> *)key value:(id)value __attribute__((swift_name("put(key:value:)")));
- (void)removeKey:(SOCKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("remove(key:)")));
- (id)takeKey:(SOCKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("take(key:)")));
- (id _Nullable)takeOrNullKey:(SOCKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("takeOrNull(key:)")));
@property (readonly) NSArray<SOCKtor_utilsAttributeKey<id> *> *allKeys __attribute__((swift_name("allKeys")));
@end;

__attribute__((swift_name("KotlinAbstractCoroutineContextElement")))
@interface SOCKotlinAbstractCoroutineContextElement : SOCBase <SOCKotlinCoroutineContextElement>
- (instancetype)initWithKey:(id<SOCKotlinCoroutineContextKey>)key __attribute__((swift_name("init(key:)"))) __attribute__((objc_designated_initializer));
@property (readonly) id<SOCKotlinCoroutineContextKey> key __attribute__((swift_name("key")));
@end;

__attribute__((swift_name("KotlinContinuationInterceptor")))
@protocol SOCKotlinContinuationInterceptor <SOCKotlinCoroutineContextElement>
@required
- (id<SOCKotlinContinuation>)interceptContinuationContinuation:(id<SOCKotlinContinuation>)continuation __attribute__((swift_name("interceptContinuation(continuation:)")));
- (void)releaseInterceptedContinuationContinuation:(id<SOCKotlinContinuation>)continuation __attribute__((swift_name("releaseInterceptedContinuation(continuation:)")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreCoroutineDispatcher")))
@interface SOCKotlinx_coroutines_coreCoroutineDispatcher : SOCKotlinAbstractCoroutineContextElement <SOCKotlinContinuationInterceptor>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithKey:(id<SOCKotlinCoroutineContextKey>)key __attribute__((swift_name("init(key:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SOCKotlinx_coroutines_coreCoroutineDispatcherKey *companion __attribute__((swift_name("companion")));
- (void)dispatchContext:(id<SOCKotlinCoroutineContext>)context block:(id<SOCKotlinx_coroutines_coreRunnable>)block __attribute__((swift_name("dispatch(context:block:)")));
- (void)dispatchYieldContext:(id<SOCKotlinCoroutineContext>)context block:(id<SOCKotlinx_coroutines_coreRunnable>)block __attribute__((swift_name("dispatchYield(context:block:)")));
- (id<SOCKotlinContinuation>)interceptContinuationContinuation:(id<SOCKotlinContinuation>)continuation __attribute__((swift_name("interceptContinuation(continuation:)")));
- (BOOL)isDispatchNeededContext:(id<SOCKotlinCoroutineContext>)context __attribute__((swift_name("isDispatchNeeded(context:)")));
- (SOCKotlinx_coroutines_coreCoroutineDispatcher *)plusOther_:(SOCKotlinx_coroutines_coreCoroutineDispatcher *)other __attribute__((swift_name("plus(other_:)"))) __attribute__((unavailable("Operator '+' on two CoroutineDispatcher objects is meaningless. CoroutineDispatcher is a coroutine context element and `+` is a set-sum operator for coroutine contexts. The dispatcher to the right of `+` just replaces the dispatcher to the left.")));
- (void)releaseInterceptedContinuationContinuation:(id<SOCKotlinContinuation>)continuation __attribute__((swift_name("releaseInterceptedContinuation(continuation:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("Ktor_utilsPipeline")))
@interface SOCKtor_utilsPipeline<TSubject, TContext> : SOCBase
- (instancetype)initWithPhase:(SOCKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<SOCKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhases:(SOCKotlinArray<SOCKtor_utilsPipelinePhase *> *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer));
- (void)addPhasePhase:(SOCKtor_utilsPipelinePhase *)phase __attribute__((swift_name("addPhase(phase:)")));
- (void)afterIntercepted __attribute__((swift_name("afterIntercepted()")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)executeContext:(TContext)context subject:(TSubject)subject completionHandler:(void (^)(TSubject _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("execute(context:subject:completionHandler:)")));
- (void)insertPhaseAfterReference:(SOCKtor_utilsPipelinePhase *)reference phase:(SOCKtor_utilsPipelinePhase *)phase __attribute__((swift_name("insertPhaseAfter(reference:phase:)")));
- (void)insertPhaseBeforeReference:(SOCKtor_utilsPipelinePhase *)reference phase:(SOCKtor_utilsPipelinePhase *)phase __attribute__((swift_name("insertPhaseBefore(reference:phase:)")));
- (void)interceptPhase:(SOCKtor_utilsPipelinePhase *)phase block:(id<SOCKotlinSuspendFunction2>)block __attribute__((swift_name("intercept(phase:block:)")));
- (void)mergeFrom:(SOCKtor_utilsPipeline<TSubject, TContext> *)from __attribute__((swift_name("merge(from:)")));
@property (readonly) id<SOCKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) BOOL developmentMode __attribute__((swift_name("developmentMode")));
@property (readonly, getter=isEmpty_) BOOL isEmpty __attribute__((swift_name("isEmpty")));
@property (readonly) NSArray<SOCKtor_utilsPipelinePhase *> *items __attribute__((swift_name("items")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpReceivePipeline")))
@interface SOCKtor_client_coreHttpReceivePipeline : SOCKtor_utilsPipeline<SOCKtor_client_coreHttpResponse *, SOCKtor_client_coreHttpClientCall *>
- (instancetype)initWithDevelopmentMode:(BOOL)developmentMode __attribute__((swift_name("init(developmentMode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhase:(SOCKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<SOCKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(SOCKotlinArray<SOCKtor_utilsPipelinePhase *> *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SOCKtor_client_coreHttpReceivePipelinePhases *companion __attribute__((swift_name("companion")));
@property (readonly) BOOL developmentMode __attribute__((swift_name("developmentMode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestPipeline")))
@interface SOCKtor_client_coreHttpRequestPipeline : SOCKtor_utilsPipeline<id, SOCKtor_client_coreHttpRequestBuilder *>
- (instancetype)initWithDevelopmentMode:(BOOL)developmentMode __attribute__((swift_name("init(developmentMode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhase:(SOCKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<SOCKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(SOCKotlinArray<SOCKtor_utilsPipelinePhase *> *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SOCKtor_client_coreHttpRequestPipelinePhases *companion __attribute__((swift_name("companion")));
@property (readonly) BOOL developmentMode __attribute__((swift_name("developmentMode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpResponsePipeline")))
@interface SOCKtor_client_coreHttpResponsePipeline : SOCKtor_utilsPipeline<SOCKtor_client_coreHttpResponseContainer *, SOCKtor_client_coreHttpClientCall *>
- (instancetype)initWithDevelopmentMode:(BOOL)developmentMode __attribute__((swift_name("init(developmentMode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhase:(SOCKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<SOCKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(SOCKotlinArray<SOCKtor_utilsPipelinePhase *> *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SOCKtor_client_coreHttpResponsePipelinePhases *companion __attribute__((swift_name("companion")));
@property (readonly) BOOL developmentMode __attribute__((swift_name("developmentMode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpSendPipeline")))
@interface SOCKtor_client_coreHttpSendPipeline : SOCKtor_utilsPipeline<id, SOCKtor_client_coreHttpRequestBuilder *>
- (instancetype)initWithDevelopmentMode:(BOOL)developmentMode __attribute__((swift_name("init(developmentMode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhase:(SOCKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<SOCKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(SOCKotlinArray<SOCKtor_utilsPipelinePhase *> *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SOCKtor_client_coreHttpSendPipelinePhases *companion __attribute__((swift_name("companion")));
@property (readonly) BOOL developmentMode __attribute__((swift_name("developmentMode")));
@end;

__attribute__((swift_name("KotlinIterator")))
@protocol SOCKotlinIterator
@required
- (BOOL)hasNext __attribute__((swift_name("hasNext()")));
- (id _Nullable)next __attribute__((swift_name("next()")));
@end;

__attribute__((swift_name("KotlinByteIterator")))
@interface SOCKotlinByteIterator : SOCBase <SOCKotlinIterator>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (SOCByte *)next __attribute__((swift_name("next()")));
- (int8_t)nextByte __attribute__((swift_name("nextByte()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreCompositeEncoder")))
@protocol SOCKotlinx_serialization_coreCompositeEncoder
@required
- (void)encodeBooleanElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(BOOL)value __attribute__((swift_name("encodeBooleanElement(descriptor:index:value:)")));
- (void)encodeByteElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int8_t)value __attribute__((swift_name("encodeByteElement(descriptor:index:value:)")));
- (void)encodeCharElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(unichar)value __attribute__((swift_name("encodeCharElement(descriptor:index:value:)")));
- (void)encodeDoubleElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(double)value __attribute__((swift_name("encodeDoubleElement(descriptor:index:value:)")));
- (void)encodeFloatElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(float)value __attribute__((swift_name("encodeFloatElement(descriptor:index:value:)")));
- (id<SOCKotlinx_serialization_coreEncoder>)encodeInlineElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("encodeInlineElement(descriptor:index:)")));
- (void)encodeIntElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int32_t)value __attribute__((swift_name("encodeIntElement(descriptor:index:value:)")));
- (void)encodeLongElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int64_t)value __attribute__((swift_name("encodeLongElement(descriptor:index:value:)")));
- (void)encodeNullableSerializableElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<SOCKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeSerializableElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<SOCKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeShortElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int16_t)value __attribute__((swift_name("encodeShortElement(descriptor:index:value:)")));
- (void)encodeStringElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(NSString *)value __attribute__((swift_name("encodeStringElement(descriptor:index:value:)")));
- (void)endStructureDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
- (BOOL)shouldEncodeElementDefaultDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("shouldEncodeElementDefault(descriptor:index:)")));
@property (readonly) SOCKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("KotlinAnnotation")))
@protocol SOCKotlinAnnotation
@required
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerialKind")))
@interface SOCKotlinx_serialization_coreSerialKind : SOCBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreCompositeDecoder")))
@protocol SOCKotlinx_serialization_coreCompositeDecoder
@required
- (BOOL)decodeBooleanElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeBooleanElement(descriptor:index:)")));
- (int8_t)decodeByteElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeByteElement(descriptor:index:)")));
- (unichar)decodeCharElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeCharElement(descriptor:index:)")));
- (int32_t)decodeCollectionSizeDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeCollectionSize(descriptor:)")));
- (double)decodeDoubleElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeDoubleElement(descriptor:index:)")));
- (int32_t)decodeElementIndexDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeElementIndex(descriptor:)")));
- (float)decodeFloatElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeFloatElement(descriptor:index:)")));
- (id<SOCKotlinx_serialization_coreDecoder>)decodeInlineElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeInlineElement(descriptor:index:)")));
- (int32_t)decodeIntElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeIntElement(descriptor:index:)")));
- (int64_t)decodeLongElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeLongElement(descriptor:index:)")));
- (id _Nullable)decodeNullableSerializableElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<SOCKotlinx_serialization_coreDeserializationStrategy>)deserializer previousValue:(id _Nullable)previousValue __attribute__((swift_name("decodeNullableSerializableElement(descriptor:index:deserializer:previousValue:)")));
- (BOOL)decodeSequentially __attribute__((swift_name("decodeSequentially()")));
- (id _Nullable)decodeSerializableElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<SOCKotlinx_serialization_coreDeserializationStrategy>)deserializer previousValue:(id _Nullable)previousValue __attribute__((swift_name("decodeSerializableElement(descriptor:index:deserializer:previousValue:)")));
- (int16_t)decodeShortElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeShortElement(descriptor:index:)")));
- (NSString *)decodeStringElementDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeStringElement(descriptor:index:)")));
- (void)endStructureDescriptor:(id<SOCKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
@property (readonly) SOCKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinNothing")))
@interface SOCKotlinNothing : SOCBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreScope")))
@interface SOCKoin_coreScope : SOCBase
- (instancetype)initWithScopeQualifier:(id<SOCKoin_coreQualifier>)scopeQualifier id:(NSString *)id isRoot:(BOOL)isRoot _koin:(SOCKoin_coreKoin *)_koin __attribute__((swift_name("init(scopeQualifier:id:isRoot:_koin:)"))) __attribute__((objc_designated_initializer));
- (void)close __attribute__((swift_name("close()")));
- (id<SOCKoin_coreQualifier>)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (BOOL)component3 __attribute__((swift_name("component3()")));
- (SOCKoin_coreScope *)doCopyScopeQualifier:(id<SOCKoin_coreQualifier>)scopeQualifier id:(NSString *)id isRoot:(BOOL)isRoot _koin:(SOCKoin_coreKoin *)_koin __attribute__((swift_name("doCopy(scopeQualifier:id:isRoot:_koin:)")));
- (void)declareInstance:(id _Nullable)instance qualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier secondaryTypes:(NSArray<id<SOCKotlinKClass>> *)secondaryTypes allowOverride:(BOOL)allowOverride __attribute__((swift_name("declare(instance:qualifier:secondaryTypes:allowOverride:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (id _Nullable)getClazz:(id<SOCKotlinKClass>)clazz qualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier parameters:(SOCKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("get(clazz:qualifier:parameters:)")));
- (id)getQualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier parameters:(SOCKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("get(qualifier:parameters:)")));
- (NSArray<id> *)getAll __attribute__((swift_name("getAll()")));
- (NSArray<id> *)getAllClazz:(id<SOCKotlinKClass>)clazz __attribute__((swift_name("getAll(clazz:)")));
- (SOCKoin_coreKoin *)getKoin __attribute__((swift_name("getKoin()")));
- (id _Nullable)getOrNullClazz:(id<SOCKotlinKClass>)clazz qualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier parameters:(SOCKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("getOrNull(clazz:qualifier:parameters:)")));
- (id _Nullable)getOrNullQualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier parameters:(SOCKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("getOrNull(qualifier:parameters:)")));
- (NSString *)getPropertyKey:(NSString *)key __attribute__((swift_name("getProperty(key:)")));
- (NSString *)getPropertyKey:(NSString *)key defaultValue:(NSString *)defaultValue __attribute__((swift_name("getProperty(key:defaultValue:)")));
- (NSString * _Nullable)getPropertyOrNullKey:(NSString *)key __attribute__((swift_name("getPropertyOrNull(key:)")));
- (SOCKoin_coreScope *)getScopeScopeID:(NSString *)scopeID __attribute__((swift_name("getScope(scopeID:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (id<SOCKotlinLazy>)injectQualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier mode:(SOCKotlinLazyThreadSafetyMode *)mode parameters:(SOCKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("inject(qualifier:mode:parameters:)")));
- (id<SOCKotlinLazy>)injectOrNullQualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier mode:(SOCKotlinLazyThreadSafetyMode *)mode parameters:(SOCKoin_coreParametersHolder *(^ _Nullable)(void))parameters __attribute__((swift_name("injectOrNull(qualifier:mode:parameters:)")));
- (BOOL)isNotClosed __attribute__((swift_name("isNotClosed()")));
- (void)linkToScopes:(SOCKotlinArray<SOCKoin_coreScope *> *)scopes __attribute__((swift_name("linkTo(scopes:)")));
- (void)registerCallbackCallback:(id<SOCKoin_coreScopeCallback>)callback __attribute__((swift_name("registerCallback(callback:)")));
- (NSString *)description __attribute__((swift_name("description()")));
- (void)unlinkScopes:(SOCKotlinArray<SOCKoin_coreScope *> *)scopes __attribute__((swift_name("unlink(scopes:)")));
@property (readonly) NSMutableArray<SOCKoin_coreParametersHolder *> *_parameterStack __attribute__((swift_name("_parameterStack")));
@property id _Nullable _source __attribute__((swift_name("_source")));
@property (readonly) BOOL closed __attribute__((swift_name("closed")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) BOOL isRoot __attribute__((swift_name("isRoot")));
@property (readonly) SOCKoin_coreLogger *logger __attribute__((swift_name("logger")));
@property (readonly) id<SOCKoin_coreQualifier> scopeQualifier __attribute__((swift_name("scopeQualifier")));
@end;

__attribute__((swift_name("Koin_coreKoinScopeComponent")))
@protocol SOCKoin_coreKoinScopeComponent <SOCKoin_coreKoinComponent>
@required
- (void)closeScope __attribute__((swift_name("closeScope()")));
@property (readonly) SOCKoin_coreScope *scope __attribute__((swift_name("scope")));
@end;

__attribute__((swift_name("Koin_coreQualifier")))
@protocol SOCKoin_coreQualifier
@required
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((swift_name("Koin_coreParametersHolder")))
@interface SOCKoin_coreParametersHolder : SOCBase
- (instancetype)initWith_values:(NSMutableArray<id> *)_values __attribute__((swift_name("init(_values:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCKoin_coreParametersHolderCompanion *companion __attribute__((swift_name("companion")));
- (SOCKoin_coreParametersHolder *)addValue:(id)value __attribute__((swift_name("add(value:)")));
- (id _Nullable)component1 __attribute__((swift_name("component1()")));
- (id _Nullable)component2 __attribute__((swift_name("component2()")));
- (id _Nullable)component3 __attribute__((swift_name("component3()")));
- (id _Nullable)component4 __attribute__((swift_name("component4()")));
- (id _Nullable)component5 __attribute__((swift_name("component5()")));
- (id _Nullable)elementAtI:(int32_t)i clazz:(id<SOCKotlinKClass>)clazz __attribute__((swift_name("elementAt(i:clazz:)")));
- (id)get __attribute__((swift_name("get()")));
- (id _Nullable)getI:(int32_t)i __attribute__((swift_name("get(i:)")));
- (id _Nullable)getOrNull __attribute__((swift_name("getOrNull()")));
- (id _Nullable)getOrNullClazz:(id<SOCKotlinKClass>)clazz __attribute__((swift_name("getOrNull(clazz:)")));
- (SOCKoin_coreParametersHolder *)insertIndex:(int32_t)index value:(id)value __attribute__((swift_name("insert(index:value:)")));
- (BOOL)isEmpty __attribute__((swift_name("isEmpty()")));
- (BOOL)isNotEmpty __attribute__((swift_name("isNotEmpty()")));
- (void)setI:(int32_t)i t:(id _Nullable)t __attribute__((swift_name("set(i:t:)")));
- (int32_t)size __attribute__((swift_name("size()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<id> *values __attribute__((swift_name("values")));
@end;

__attribute__((swift_name("KotlinLazy")))
@protocol SOCKotlinLazy
@required
- (BOOL)isInitialized __attribute__((swift_name("isInitialized()")));
@property (readonly) id _Nullable value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinLazyThreadSafetyMode")))
@interface SOCKotlinLazyThreadSafetyMode : SOCKotlinEnum<SOCKotlinLazyThreadSafetyMode *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) SOCKotlinLazyThreadSafetyMode *synchronized __attribute__((swift_name("synchronized")));
@property (class, readonly) SOCKotlinLazyThreadSafetyMode *publication __attribute__((swift_name("publication")));
@property (class, readonly) SOCKotlinLazyThreadSafetyMode *none __attribute__((swift_name("none")));
+ (SOCKotlinArray<SOCKotlinLazyThreadSafetyMode *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((swift_name("Koin_coreLogger")))
@interface SOCKoin_coreLogger : SOCBase
- (instancetype)initWithLevel:(SOCKoin_coreLevel *)level __attribute__((swift_name("init(level:)"))) __attribute__((objc_designated_initializer));
- (void)debugMsg:(NSString *)msg __attribute__((swift_name("debug(msg:)")));
- (void)errorMsg:(NSString *)msg __attribute__((swift_name("error(msg:)")));
- (void)infoMsg:(NSString *)msg __attribute__((swift_name("info(msg:)")));
- (BOOL)isAtLvl:(SOCKoin_coreLevel *)lvl __attribute__((swift_name("isAt(lvl:)")));
- (void)logLvl:(SOCKoin_coreLevel *)lvl msg:(NSString *(^)(void))msg __attribute__((swift_name("log(lvl:msg:)")));
- (void)logLevel:(SOCKoin_coreLevel *)level msg:(NSString *)msg __attribute__((swift_name("log(level:msg:)")));
@property SOCKoin_coreLevel *level __attribute__((swift_name("level")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreInstanceRegistry")))
@interface SOCKoin_coreInstanceRegistry : SOCBase
- (instancetype)initWith_koin:(SOCKoin_coreKoin *)_koin __attribute__((swift_name("init(_koin:)"))) __attribute__((objc_designated_initializer));
- (void)saveMappingAllowOverride:(BOOL)allowOverride mapping:(NSString *)mapping factory:(SOCKoin_coreInstanceFactory<id> *)factory logWarning:(BOOL)logWarning __attribute__((swift_name("saveMapping(allowOverride:mapping:factory:logWarning:)")));
- (int32_t)size __attribute__((swift_name("size()")));
@property (readonly) SOCKoin_coreKoin *_koin __attribute__((swift_name("_koin")));
@property (readonly) NSDictionary<NSString *, SOCKoin_coreInstanceFactory<id> *> *instances __attribute__((swift_name("instances")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_corePropertyRegistry")))
@interface SOCKoin_corePropertyRegistry : SOCBase
- (instancetype)initWith_koin:(SOCKoin_coreKoin *)_koin __attribute__((swift_name("init(_koin:)"))) __attribute__((objc_designated_initializer));
- (void)close __attribute__((swift_name("close()")));
- (void)deletePropertyKey:(NSString *)key __attribute__((swift_name("deleteProperty(key:)")));
- (id _Nullable)getPropertyKey:(NSString *)key __attribute__((swift_name("getProperty(key:)")));
- (void)savePropertiesProperties:(NSDictionary<NSString *, id> *)properties __attribute__((swift_name("saveProperties(properties:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreScopeRegistry")))
@interface SOCKoin_coreScopeRegistry : SOCBase
- (instancetype)initWith_koin:(SOCKoin_coreKoin *)_koin __attribute__((swift_name("init(_koin:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCKoin_coreScopeRegistryCompanion *companion __attribute__((swift_name("companion")));
- (void)loadScopesModules:(NSArray<SOCKoin_coreModule *> *)modules __attribute__((swift_name("loadScopes(modules:)")));
@property (readonly) SOCKoin_coreScope *rootScope __attribute__((swift_name("rootScope")));
@property (readonly) NSSet<id<SOCKoin_coreQualifier>> *scopeDefinitions __attribute__((swift_name("scopeDefinitions")));
@end;

__attribute__((swift_name("Koin_coreInstanceFactory")))
@interface SOCKoin_coreInstanceFactory<T> : SOCBase
- (instancetype)initWithBeanDefinition:(SOCKoin_coreBeanDefinition<T> *)beanDefinition __attribute__((swift_name("init(beanDefinition:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCKoin_coreInstanceFactoryCompanion *companion __attribute__((swift_name("companion")));
- (T _Nullable)createContext:(SOCKoin_coreInstanceContext *)context __attribute__((swift_name("create(context:)")));
- (void)dropScope:(SOCKoin_coreScope * _Nullable)scope __attribute__((swift_name("drop(scope:)")));
- (void)dropAll __attribute__((swift_name("dropAll()")));
- (T _Nullable)getContext:(SOCKoin_coreInstanceContext *)context __attribute__((swift_name("get(context:)")));
- (BOOL)isCreatedContext:(SOCKoin_coreInstanceContext * _Nullable)context __attribute__((swift_name("isCreated(context:)")));
@property (readonly) SOCKoin_coreBeanDefinition<T> *beanDefinition __attribute__((swift_name("beanDefinition")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreScopeDSL")))
@interface SOCKoin_coreScopeDSL : SOCBase
- (instancetype)initWithScopeQualifier:(id<SOCKoin_coreQualifier>)scopeQualifier module:(SOCKoin_coreModule *)module __attribute__((swift_name("init(scopeQualifier:module:)"))) __attribute__((objc_designated_initializer));
- (SOCKotlinPair<SOCKoin_coreModule *, SOCKoin_coreInstanceFactory<id> *> *)factoryQualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier definition:(id _Nullable (^)(SOCKoin_coreScope *, SOCKoin_coreParametersHolder *))definition __attribute__((swift_name("factory(qualifier:definition:)")));
- (SOCKotlinPair<SOCKoin_coreModule *, SOCKoin_coreInstanceFactory<id> *> *)scopedQualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier definition:(id _Nullable (^)(SOCKoin_coreScope *, SOCKoin_coreParametersHolder *))definition __attribute__((swift_name("scoped(qualifier:definition:)")));
- (SOCKotlinPair<SOCKoin_coreModule *, SOCKoin_coreInstanceFactory<id> *> *)singleQualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier definition:(id _Nullable (^)(SOCKoin_coreScope *, SOCKoin_coreParametersHolder *))definition __attribute__((swift_name("single(qualifier:definition:)"))) __attribute__((unavailable("Can't use Single in a scope. Use Scoped instead")));
@property (readonly) SOCKoin_coreModule *module __attribute__((swift_name("module")));
@property (readonly) id<SOCKoin_coreQualifier> scopeQualifier __attribute__((swift_name("scopeQualifier")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreSingleInstanceFactory")))
@interface SOCKoin_coreSingleInstanceFactory<T> : SOCKoin_coreInstanceFactory<T>
- (instancetype)initWithBeanDefinition:(SOCKoin_coreBeanDefinition<T> *)beanDefinition __attribute__((swift_name("init(beanDefinition:)"))) __attribute__((objc_designated_initializer));
- (T _Nullable)createContext:(SOCKoin_coreInstanceContext *)context __attribute__((swift_name("create(context:)")));
- (void)dropScope:(SOCKoin_coreScope * _Nullable)scope __attribute__((swift_name("drop(scope:)")));
- (void)dropAll __attribute__((swift_name("dropAll()")));
- (T _Nullable)getContext:(SOCKoin_coreInstanceContext *)context __attribute__((swift_name("get(context:)")));
- (BOOL)isCreatedContext:(SOCKoin_coreInstanceContext * _Nullable)context __attribute__((swift_name("isCreated(context:)")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreParentJob")))
@protocol SOCKotlinx_coroutines_coreParentJob <SOCKotlinx_coroutines_coreJob>
@required
- (SOCKotlinCancellationException *)getChildJobCancellationCause __attribute__((swift_name("getChildJobCancellationCause()")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreSelectInstance")))
@protocol SOCKotlinx_coroutines_coreSelectInstance
@required
- (void)disposeOnSelectHandle:(id<SOCKotlinx_coroutines_coreDisposableHandle>)handle __attribute__((swift_name("disposeOnSelect(handle:)")));
- (id _Nullable)performAtomicTrySelectDesc:(SOCKotlinx_coroutines_coreAtomicDesc *)desc __attribute__((swift_name("performAtomicTrySelect(desc:)")));
- (void)resumeSelectWithExceptionException:(SOCKotlinThrowable *)exception __attribute__((swift_name("resumeSelectWithException(exception:)")));
- (BOOL)trySelect __attribute__((swift_name("trySelect()")));
- (id _Nullable)trySelectOtherOtherOp:(SOCKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp * _Nullable)otherOp __attribute__((swift_name("trySelectOther(otherOp:)")));
@property (readonly) id<SOCKotlinContinuation> completion __attribute__((swift_name("completion")));
@property (readonly) BOOL isSelected __attribute__((swift_name("isSelected")));
@end;

__attribute__((swift_name("KotlinSuspendFunction0")))
@protocol SOCKotlinSuspendFunction0 <SOCKotlinFunction>
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeWithCompletionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(completionHandler:)")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerializersModuleCollector")))
@protocol SOCKotlinx_serialization_coreSerializersModuleCollector
@required
- (void)contextualKClass:(id<SOCKotlinKClass>)kClass provider:(id<SOCKotlinx_serialization_coreKSerializer> (^)(NSArray<id<SOCKotlinx_serialization_coreKSerializer>> *))provider __attribute__((swift_name("contextual(kClass:provider:)")));
- (void)contextualKClass:(id<SOCKotlinKClass>)kClass serializer:(id<SOCKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("contextual(kClass:serializer:)")));
- (void)polymorphicBaseClass:(id<SOCKotlinKClass>)baseClass actualClass:(id<SOCKotlinKClass>)actualClass actualSerializer:(id<SOCKotlinx_serialization_coreKSerializer>)actualSerializer __attribute__((swift_name("polymorphic(baseClass:actualClass:actualSerializer:)")));
- (void)polymorphicDefaultBaseClass:(id<SOCKotlinKClass>)baseClass defaultSerializerProvider:(id<SOCKotlinx_serialization_coreDeserializationStrategy> _Nullable (^)(NSString * _Nullable))defaultSerializerProvider __attribute__((swift_name("polymorphicDefault(baseClass:defaultSerializerProvider:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinEnumCompanion")))
@interface SOCKotlinEnumCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKotlinEnumCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsGMTDate.Companion")))
@interface SOCKtor_utilsGMTDateCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_utilsGMTDateCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) SOCKtor_utilsGMTDate *START __attribute__((swift_name("START")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsWeekDay")))
@interface SOCKtor_utilsWeekDay : SOCKotlinEnum<SOCKtor_utilsWeekDay *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SOCKtor_utilsWeekDayCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) SOCKtor_utilsWeekDay *monday __attribute__((swift_name("monday")));
@property (class, readonly) SOCKtor_utilsWeekDay *tuesday __attribute__((swift_name("tuesday")));
@property (class, readonly) SOCKtor_utilsWeekDay *wednesday __attribute__((swift_name("wednesday")));
@property (class, readonly) SOCKtor_utilsWeekDay *thursday __attribute__((swift_name("thursday")));
@property (class, readonly) SOCKtor_utilsWeekDay *friday __attribute__((swift_name("friday")));
@property (class, readonly) SOCKtor_utilsWeekDay *saturday __attribute__((swift_name("saturday")));
@property (class, readonly) SOCKtor_utilsWeekDay *sunday __attribute__((swift_name("sunday")));
+ (SOCKotlinArray<SOCKtor_utilsWeekDay *> *)values __attribute__((swift_name("values()")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsMonth")))
@interface SOCKtor_utilsMonth : SOCKotlinEnum<SOCKtor_utilsMonth *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SOCKtor_utilsMonthCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) SOCKtor_utilsMonth *january __attribute__((swift_name("january")));
@property (class, readonly) SOCKtor_utilsMonth *february __attribute__((swift_name("february")));
@property (class, readonly) SOCKtor_utilsMonth *march __attribute__((swift_name("march")));
@property (class, readonly) SOCKtor_utilsMonth *april __attribute__((swift_name("april")));
@property (class, readonly) SOCKtor_utilsMonth *may __attribute__((swift_name("may")));
@property (class, readonly) SOCKtor_utilsMonth *june __attribute__((swift_name("june")));
@property (class, readonly) SOCKtor_utilsMonth *july __attribute__((swift_name("july")));
@property (class, readonly) SOCKtor_utilsMonth *august __attribute__((swift_name("august")));
@property (class, readonly) SOCKtor_utilsMonth *september __attribute__((swift_name("september")));
@property (class, readonly) SOCKtor_utilsMonth *october __attribute__((swift_name("october")));
@property (class, readonly) SOCKtor_utilsMonth *november __attribute__((swift_name("november")));
@property (class, readonly) SOCKtor_utilsMonth *december __attribute__((swift_name("december")));
+ (SOCKotlinArray<SOCKtor_utilsMonth *> *)values __attribute__((swift_name("values()")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestBuilder.Companion")))
@interface SOCKtor_client_coreHttpRequestBuilderCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_client_coreHttpRequestBuilderCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestData")))
@interface SOCKtor_client_coreHttpRequestData : SOCBase
- (instancetype)initWithUrl:(SOCKtor_httpUrl *)url method:(SOCKtor_httpHttpMethod *)method headers:(id<SOCKtor_httpHeaders>)headers body:(SOCKtor_httpOutgoingContent *)body executionContext:(id<SOCKotlinx_coroutines_coreJob>)executionContext attributes:(id<SOCKtor_utilsAttributes>)attributes __attribute__((swift_name("init(url:method:headers:body:executionContext:attributes:)"))) __attribute__((objc_designated_initializer));
- (id _Nullable)getCapabilityOrNullKey:(id<SOCKtor_client_coreHttpClientEngineCapability>)key __attribute__((swift_name("getCapabilityOrNull(key:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<SOCKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) SOCKtor_httpOutgoingContent *body __attribute__((swift_name("body")));
@property (readonly) id<SOCKotlinx_coroutines_coreJob> executionContext __attribute__((swift_name("executionContext")));
@property (readonly) id<SOCKtor_httpHeaders> headers __attribute__((swift_name("headers")));
@property (readonly) SOCKtor_httpHttpMethod *method __attribute__((swift_name("method")));
@property (readonly) SOCKtor_httpUrl *url __attribute__((swift_name("url")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpURLBuilder")))
@interface SOCKtor_httpURLBuilder : SOCBase
- (instancetype)initWithProtocol:(SOCKtor_httpURLProtocol *)protocol host:(NSString *)host port:(int32_t)port user:(NSString * _Nullable)user password:(NSString * _Nullable)password encodedPath:(NSString *)encodedPath parameters:(SOCKtor_httpParametersBuilder *)parameters fragment:(NSString *)fragment trailingQuery:(BOOL)trailingQuery __attribute__((swift_name("init(protocol:host:port:user:password:encodedPath:parameters:fragment:trailingQuery:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCKtor_httpURLBuilderCompanion *companion __attribute__((swift_name("companion")));
- (SOCKtor_httpUrl *)build __attribute__((swift_name("build()")));
- (NSString *)buildString __attribute__((swift_name("buildString()")));
- (SOCKtor_httpURLBuilder *)pathComponents:(SOCKotlinArray<NSString *> *)components __attribute__((swift_name("path(components:)")));
- (SOCKtor_httpURLBuilder *)pathComponents_:(NSArray<NSString *> *)components __attribute__((swift_name("path(components_:)")));
@property NSString *encodedPath __attribute__((swift_name("encodedPath")));
@property NSString *fragment __attribute__((swift_name("fragment")));
@property NSString *host __attribute__((swift_name("host")));
@property (readonly) SOCKtor_httpParametersBuilder *parameters __attribute__((swift_name("parameters")));
@property NSString * _Nullable password __attribute__((swift_name("password")));
@property int32_t port __attribute__((swift_name("port")));
@property SOCKtor_httpURLProtocol *protocol __attribute__((swift_name("protocol")));
@property BOOL trailingQuery __attribute__((swift_name("trailingQuery")));
@property NSString * _Nullable user __attribute__((swift_name("user")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpMethod")))
@interface SOCKtor_httpHttpMethod : SOCBase
- (instancetype)initWithValue:(NSString *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCKtor_httpHttpMethodCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (SOCKtor_httpHttpMethod *)doCopyValue:(NSString *)value __attribute__((swift_name("doCopy(value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((swift_name("Ktor_ioByteReadChannel")))
@protocol SOCKtor_ioByteReadChannel
@required
- (BOOL)cancelCause_:(SOCKotlinThrowable * _Nullable)cause __attribute__((swift_name("cancel(cause_:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)discardMax:(int64_t)max completionHandler:(void (^)(SOCLong * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("discard(max:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)peekToDestination:(SOCKtor_ioMemory *)destination destinationOffset:(int64_t)destinationOffset offset:(int64_t)offset min:(int64_t)min max:(int64_t)max completionHandler:(void (^)(SOCLong * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("peekTo(destination:destinationOffset:offset:min:max:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readAvailableDst:(SOCKtor_ioIoBuffer *)dst completionHandler:(void (^)(SOCInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readAvailable(dst:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readAvailableDst:(SOCKotlinByteArray *)dst offset:(int32_t)offset length:(int32_t)length completionHandler:(void (^)(SOCInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readAvailable(dst:offset:length:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readAvailableDst:(void *)dst offset:(int32_t)offset length:(int32_t)length completionHandler_:(void (^)(SOCInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readAvailable(dst:offset:length:completionHandler_:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readAvailableDst:(void *)dst offset:(int64_t)offset length:(int64_t)length completionHandler__:(void (^)(SOCInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readAvailable(dst:offset:length:completionHandler__:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readBooleanWithCompletionHandler:(void (^)(SOCBoolean * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readBoolean(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readByteWithCompletionHandler:(void (^)(SOCByte * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readByte(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readDoubleWithCompletionHandler:(void (^)(SOCDouble * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readDouble(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readFloatWithCompletionHandler:(void (^)(SOCFloat * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readFloat(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readFullyDst:(SOCKtor_ioIoBuffer *)dst n:(int32_t)n completionHandler:(void (^)(SOCKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readFully(dst:n:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readFullyDst:(SOCKotlinByteArray *)dst offset:(int32_t)offset length:(int32_t)length completionHandler:(void (^)(SOCKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readFully(dst:offset:length:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readFullyDst:(void *)dst offset:(int32_t)offset length:(int32_t)length completionHandler_:(void (^)(SOCKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readFully(dst:offset:length:completionHandler_:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readFullyDst:(void *)dst offset:(int64_t)offset length:(int64_t)length completionHandler__:(void (^)(SOCKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readFully(dst:offset:length:completionHandler__:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readIntWithCompletionHandler:(void (^)(SOCInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readInt(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readLongWithCompletionHandler:(void (^)(SOCLong * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readLong(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readPacketSize:(int32_t)size headerSizeHint:(int32_t)headerSizeHint completionHandler:(void (^)(SOCKtor_ioByteReadPacket * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readPacket(size:headerSizeHint:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readRemainingLimit:(int64_t)limit headerSizeHint:(int32_t)headerSizeHint completionHandler:(void (^)(SOCKtor_ioByteReadPacket * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readRemaining(limit:headerSizeHint:completionHandler:)")));
- (void)readSessionConsumer:(void (^)(id<SOCKtor_ioReadSession>))consumer __attribute__((swift_name("readSession(consumer:)"))) __attribute__((deprecated("Use read { } instead.")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readShortWithCompletionHandler:(void (^)(SOCShort * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readShort(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readSuspendableSessionConsumer:(id<SOCKotlinSuspendFunction1>)consumer completionHandler:(void (^)(SOCKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readSuspendableSession(consumer:completionHandler:)"))) __attribute__((deprecated("Use read { } instead.")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readUTF8LineLimit:(int32_t)limit completionHandler:(void (^)(NSString * _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("readUTF8Line(limit:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readUTF8LineToOut:(id<SOCKotlinAppendable>)out limit:(int32_t)limit completionHandler:(void (^)(SOCBoolean * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readUTF8LineTo(out:limit:completionHandler:)")));
@property (readonly) int32_t availableForRead __attribute__((swift_name("availableForRead")));
@property (readonly) SOCKotlinThrowable * _Nullable closedCause __attribute__((swift_name("closedCause")));
@property (readonly) BOOL isClosedForRead __attribute__((swift_name("isClosedForRead")));
@property (readonly) BOOL isClosedForWrite __attribute__((swift_name("isClosedForWrite")));
@property SOCKtor_ioByteOrder *readByteOrder __attribute__((swift_name("readByteOrder"))) __attribute__((unavailable("Setting byte order is no longer supported. Read/write in big endian and use reverseByteOrder() extensions.")));
@property (readonly) int64_t totalBytesRead __attribute__((swift_name("totalBytesRead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpStatusCode")))
@interface SOCKtor_httpHttpStatusCode : SOCBase
- (instancetype)initWithValue:(int32_t)value description:(NSString *)description __attribute__((swift_name("init(value:description:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCKtor_httpHttpStatusCodeCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (SOCKtor_httpHttpStatusCode *)doCopyValue:(int32_t)value description:(NSString *)description __attribute__((swift_name("doCopy(value:description:)")));
- (SOCKtor_httpHttpStatusCode *)descriptionValue:(NSString *)value __attribute__((swift_name("description(value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *description_ __attribute__((swift_name("description_")));
@property (readonly) int32_t value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpProtocolVersion")))
@interface SOCKtor_httpHttpProtocolVersion : SOCBase
- (instancetype)initWithName:(NSString *)name major:(int32_t)major minor:(int32_t)minor __attribute__((swift_name("init(name:major:minor:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCKtor_httpHttpProtocolVersionCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (SOCKtor_httpHttpProtocolVersion *)doCopyName:(NSString *)name major:(int32_t)major minor:(int32_t)minor __attribute__((swift_name("doCopy(name:major:minor:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t major __attribute__((swift_name("major")));
@property (readonly) int32_t minor __attribute__((swift_name("minor")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpResponseData")))
@interface SOCKtor_client_coreHttpResponseData : SOCBase
- (instancetype)initWithStatusCode:(SOCKtor_httpHttpStatusCode *)statusCode requestTime:(SOCKtor_utilsGMTDate *)requestTime headers:(id<SOCKtor_httpHeaders>)headers version:(SOCKtor_httpHttpProtocolVersion *)version body:(id)body callContext:(id<SOCKotlinCoroutineContext>)callContext __attribute__((swift_name("init(statusCode:requestTime:headers:version:body:callContext:)"))) __attribute__((objc_designated_initializer));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id body __attribute__((swift_name("body")));
@property (readonly) id<SOCKotlinCoroutineContext> callContext __attribute__((swift_name("callContext")));
@property (readonly) id<SOCKtor_httpHeaders> headers __attribute__((swift_name("headers")));
@property (readonly) SOCKtor_utilsGMTDate *requestTime __attribute__((swift_name("requestTime")));
@property (readonly) SOCKtor_utilsGMTDate *responseTime __attribute__((swift_name("responseTime")));
@property (readonly) SOCKtor_httpHttpStatusCode *statusCode __attribute__((swift_name("statusCode")));
@property (readonly) SOCKtor_httpHttpProtocolVersion *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreProxyConfig")))
@interface SOCKtor_client_coreProxyConfig : SOCBase
- (instancetype)initWithUrl:(SOCKtor_httpUrl *)url __attribute__((swift_name("init(url:)"))) __attribute__((objc_designated_initializer));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SOCKtor_httpUrl *url __attribute__((swift_name("url")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientFeature")))
@protocol SOCKtor_client_coreHttpClientFeature
@required
- (void)installFeature:(id)feature scope:(SOCKtor_client_coreHttpClient *)scope __attribute__((swift_name("install(feature:scope:)")));
- (id)prepareBlock:(void (^)(id))block __attribute__((swift_name("prepare(block:)")));
@property (readonly) SOCKtor_utilsAttributeKey<id> *key __attribute__((swift_name("key")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpClientCall.Companion")))
@interface SOCKtor_client_coreHttpClientCallCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_client_coreHttpClientCallCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) SOCKtor_utilsAttributeKey<id> *CustomResponse __attribute__((swift_name("CustomResponse"))) __attribute__((deprecated("This is going to be removed. Please file a ticket with clarification why and what for do you need it.")));
@end;

__attribute__((swift_name("Ktor_utilsTypeInfo")))
@protocol SOCKtor_utilsTypeInfo
@required
@property (readonly) id<SOCKotlinKType> _Nullable kotlinType __attribute__((swift_name("kotlinType")));
@property (readonly) id<SOCKotlinKType> reifiedType __attribute__((swift_name("reifiedType")));
@property (readonly) id<SOCKotlinKClass> type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreTypeInfo")))
@interface SOCKtor_client_coreTypeInfo : SOCBase <SOCKtor_utilsTypeInfo>
- (instancetype)initWithType:(id<SOCKotlinKClass>)type reifiedType:(id<SOCKotlinKType>)reifiedType kotlinType:(id<SOCKotlinKType> _Nullable)kotlinType __attribute__((swift_name("init(type:reifiedType:kotlinType:)"))) __attribute__((objc_designated_initializer)) __attribute__((deprecated("This was moved to another package.")));
- (id<SOCKotlinKClass>)component1 __attribute__((swift_name("component1()")));
- (id<SOCKotlinKType>)component2 __attribute__((swift_name("component2()")));
- (id<SOCKotlinKType> _Nullable)component3 __attribute__((swift_name("component3()")));
- (SOCKtor_client_coreTypeInfo *)doCopyType:(id<SOCKotlinKClass>)type reifiedType:(id<SOCKotlinKType>)reifiedType kotlinType:(id<SOCKotlinKType> _Nullable)kotlinType __attribute__((swift_name("doCopy(type:reifiedType:kotlinType:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<SOCKotlinKType> _Nullable kotlinType __attribute__((swift_name("kotlinType")));
@property (readonly) id<SOCKotlinKType> reifiedType __attribute__((swift_name("reifiedType")));
@property (readonly) id<SOCKotlinKClass> type __attribute__((swift_name("type")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpRequest")))
@protocol SOCKtor_client_coreHttpRequest <SOCKtor_httpHttpMessage, SOCKotlinx_coroutines_coreCoroutineScope>
@required
@property (readonly) id<SOCKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) SOCKtor_client_coreHttpClientCall *call __attribute__((swift_name("call")));
@property (readonly) SOCKtor_httpOutgoingContent *content __attribute__((swift_name("content")));
@property (readonly) SOCKtor_httpHttpMethod *method __attribute__((swift_name("method")));
@property (readonly) SOCKtor_httpUrl *url __attribute__((swift_name("url")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsAttributeKey")))
@interface SOCKtor_utilsAttributeKey<T> : SOCBase
- (instancetype)initWithName:(NSString *)name __attribute__((swift_name("init(name:)"))) __attribute__((objc_designated_initializer));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((swift_name("KotlinContinuation")))
@protocol SOCKotlinContinuation
@required
- (void)resumeWithResult:(id _Nullable)result __attribute__((swift_name("resumeWith(result:)")));
@property (readonly) id<SOCKotlinCoroutineContext> context __attribute__((swift_name("context")));
@end;

__attribute__((swift_name("KotlinAbstractCoroutineContextKey")))
@interface SOCKotlinAbstractCoroutineContextKey<B, E> : SOCBase <SOCKotlinCoroutineContextKey>
- (instancetype)initWithBaseKey:(id<SOCKotlinCoroutineContextKey>)baseKey safeCast:(E _Nullable (^)(id<SOCKotlinCoroutineContextElement>))safeCast __attribute__((swift_name("init(baseKey:safeCast:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_coroutines_coreCoroutineDispatcher.Key")))
@interface SOCKotlinx_coroutines_coreCoroutineDispatcherKey : SOCKotlinAbstractCoroutineContextKey<id<SOCKotlinContinuationInterceptor>, SOCKotlinx_coroutines_coreCoroutineDispatcher *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithBaseKey:(id<SOCKotlinCoroutineContextKey>)baseKey safeCast:(id<SOCKotlinCoroutineContextElement> _Nullable (^)(id<SOCKotlinCoroutineContextElement>))safeCast __attribute__((swift_name("init(baseKey:safeCast:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)key __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKotlinx_coroutines_coreCoroutineDispatcherKey *shared __attribute__((swift_name("shared")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreRunnable")))
@protocol SOCKotlinx_coroutines_coreRunnable
@required
- (void)run __attribute__((swift_name("run()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsPipelinePhase")))
@interface SOCKtor_utilsPipelinePhase : SOCBase
- (instancetype)initWithName:(NSString *)name __attribute__((swift_name("init(name:)"))) __attribute__((objc_designated_initializer));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((swift_name("KotlinSuspendFunction2")))
@protocol SOCKotlinSuspendFunction2 <SOCKotlinFunction>
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeP1:(id _Nullable)p1 p2:(id _Nullable)p2 completionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(p1:p2:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpReceivePipeline.Phases")))
@interface SOCKtor_client_coreHttpReceivePipelinePhases : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)phases __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_client_coreHttpReceivePipelinePhases *shared __attribute__((swift_name("shared")));
@property (readonly) SOCKtor_utilsPipelinePhase *After __attribute__((swift_name("After")));
@property (readonly) SOCKtor_utilsPipelinePhase *Before __attribute__((swift_name("Before")));
@property (readonly) SOCKtor_utilsPipelinePhase *State __attribute__((swift_name("State")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestPipeline.Phases")))
@interface SOCKtor_client_coreHttpRequestPipelinePhases : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)phases __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_client_coreHttpRequestPipelinePhases *shared __attribute__((swift_name("shared")));
@property (readonly) SOCKtor_utilsPipelinePhase *Before __attribute__((swift_name("Before")));
@property (readonly) SOCKtor_utilsPipelinePhase *Render __attribute__((swift_name("Render")));
@property (readonly) SOCKtor_utilsPipelinePhase *Send __attribute__((swift_name("Send")));
@property (readonly) SOCKtor_utilsPipelinePhase *State __attribute__((swift_name("State")));
@property (readonly) SOCKtor_utilsPipelinePhase *Transform __attribute__((swift_name("Transform")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpResponsePipeline.Phases")))
@interface SOCKtor_client_coreHttpResponsePipelinePhases : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)phases __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_client_coreHttpResponsePipelinePhases *shared __attribute__((swift_name("shared")));
@property (readonly) SOCKtor_utilsPipelinePhase *After __attribute__((swift_name("After")));
@property (readonly) SOCKtor_utilsPipelinePhase *Parse __attribute__((swift_name("Parse")));
@property (readonly) SOCKtor_utilsPipelinePhase *Receive __attribute__((swift_name("Receive")));
@property (readonly) SOCKtor_utilsPipelinePhase *State __attribute__((swift_name("State")));
@property (readonly) SOCKtor_utilsPipelinePhase *Transform __attribute__((swift_name("Transform")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpResponseContainer")))
@interface SOCKtor_client_coreHttpResponseContainer : SOCBase
- (instancetype)initWithExpectedType:(id<SOCKtor_utilsTypeInfo>)expectedType response:(id)response __attribute__((swift_name("init(expectedType:response:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithExpectedType:(SOCKtor_client_coreTypeInfo *)expectedType response_:(id)response __attribute__((swift_name("init(expectedType:response_:)"))) __attribute__((objc_designated_initializer));
- (SOCKtor_client_coreTypeInfo *)component1 __attribute__((swift_name("component1()")));
- (id)component2 __attribute__((swift_name("component2()")));
- (SOCKtor_client_coreHttpResponseContainer *)doCopyExpectedType:(SOCKtor_client_coreTypeInfo *)expectedType response:(id)response __attribute__((swift_name("doCopy(expectedType:response:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SOCKtor_client_coreTypeInfo *expectedType __attribute__((swift_name("expectedType")));
@property (readonly) id response __attribute__((swift_name("response")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpSendPipeline.Phases")))
@interface SOCKtor_client_coreHttpSendPipelinePhases : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)phases __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_client_coreHttpSendPipelinePhases *shared __attribute__((swift_name("shared")));
@property (readonly) SOCKtor_utilsPipelinePhase *Before __attribute__((swift_name("Before")));
@property (readonly) SOCKtor_utilsPipelinePhase *Engine __attribute__((swift_name("Engine")));
@property (readonly) SOCKtor_utilsPipelinePhase *Monitoring __attribute__((swift_name("Monitoring")));
@property (readonly) SOCKtor_utilsPipelinePhase *Receive __attribute__((swift_name("Receive")));
@property (readonly) SOCKtor_utilsPipelinePhase *State __attribute__((swift_name("State")));
@end;

__attribute__((swift_name("Koin_coreScopeCallback")))
@protocol SOCKoin_coreScopeCallback
@required
- (void)onScopeCloseScope:(SOCKoin_coreScope *)scope __attribute__((swift_name("onScopeClose(scope:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreParametersHolder.Companion")))
@interface SOCKoin_coreParametersHolderCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKoin_coreParametersHolderCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) int32_t MAX_PARAMS __attribute__((swift_name("MAX_PARAMS")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreLevel")))
@interface SOCKoin_coreLevel : SOCKotlinEnum<SOCKoin_coreLevel *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) SOCKoin_coreLevel *debug __attribute__((swift_name("debug")));
@property (class, readonly) SOCKoin_coreLevel *info __attribute__((swift_name("info")));
@property (class, readonly) SOCKoin_coreLevel *error __attribute__((swift_name("error")));
@property (class, readonly) SOCKoin_coreLevel *none __attribute__((swift_name("none")));
+ (SOCKotlinArray<SOCKoin_coreLevel *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreScopeRegistry.Companion")))
@interface SOCKoin_coreScopeRegistryCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKoin_coreScopeRegistryCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreBeanDefinition")))
@interface SOCKoin_coreBeanDefinition<T> : SOCBase
- (instancetype)initWithScopeQualifier:(id<SOCKoin_coreQualifier>)scopeQualifier primaryType:(id<SOCKotlinKClass>)primaryType qualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier definition:(T _Nullable (^)(SOCKoin_coreScope *, SOCKoin_coreParametersHolder *))definition kind:(SOCKoin_coreKind *)kind secondaryTypes:(NSArray<id<SOCKotlinKClass>> *)secondaryTypes __attribute__((swift_name("init(scopeQualifier:primaryType:qualifier:definition:kind:secondaryTypes:)"))) __attribute__((objc_designated_initializer));
- (id<SOCKoin_coreQualifier>)component1 __attribute__((swift_name("component1()")));
- (id<SOCKotlinKClass>)component2 __attribute__((swift_name("component2()")));
- (id<SOCKoin_coreQualifier> _Nullable)component3 __attribute__((swift_name("component3()")));
- (T _Nullable (^)(SOCKoin_coreScope *, SOCKoin_coreParametersHolder *))component4 __attribute__((swift_name("component4()")));
- (SOCKoin_coreKind *)component5 __attribute__((swift_name("component5()")));
- (NSArray<id<SOCKotlinKClass>> *)component6 __attribute__((swift_name("component6()")));
- (SOCKoin_coreBeanDefinition<T> *)doCopyScopeQualifier:(id<SOCKoin_coreQualifier>)scopeQualifier primaryType:(id<SOCKotlinKClass>)primaryType qualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier definition:(T _Nullable (^)(SOCKoin_coreScope *, SOCKoin_coreParametersHolder *))definition kind:(SOCKoin_coreKind *)kind secondaryTypes:(NSArray<id<SOCKotlinKClass>> *)secondaryTypes __attribute__((swift_name("doCopy(scopeQualifier:primaryType:qualifier:definition:kind:secondaryTypes:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (BOOL)hasTypeClazz:(id<SOCKotlinKClass>)clazz __attribute__((swift_name("hasType(clazz:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (BOOL)isClazz:(id<SOCKotlinKClass>)clazz qualifier:(id<SOCKoin_coreQualifier> _Nullable)qualifier scopeDefinition:(id<SOCKoin_coreQualifier>)scopeDefinition __attribute__((swift_name("is(clazz:qualifier:scopeDefinition:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property SOCKoin_coreCallbacks<T> *callbacks __attribute__((swift_name("callbacks")));
@property (readonly) T _Nullable (^definition)(SOCKoin_coreScope *, SOCKoin_coreParametersHolder *) __attribute__((swift_name("definition")));
@property (readonly) SOCKoin_coreKind *kind __attribute__((swift_name("kind")));
@property (readonly) id<SOCKotlinKClass> primaryType __attribute__((swift_name("primaryType")));
@property (readonly) id<SOCKoin_coreQualifier> _Nullable qualifier __attribute__((swift_name("qualifier")));
@property (readonly) id<SOCKoin_coreQualifier> scopeQualifier __attribute__((swift_name("scopeQualifier")));
@property NSArray<id<SOCKotlinKClass>> *secondaryTypes __attribute__((swift_name("secondaryTypes")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreInstanceFactoryCompanion")))
@interface SOCKoin_coreInstanceFactoryCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKoin_coreInstanceFactoryCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) NSString *ERROR_SEPARATOR __attribute__((swift_name("ERROR_SEPARATOR")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreInstanceContext")))
@interface SOCKoin_coreInstanceContext : SOCBase
- (instancetype)initWithKoin:(SOCKoin_coreKoin *)koin scope:(SOCKoin_coreScope *)scope parameters:(SOCKoin_coreParametersHolder * _Nullable)parameters __attribute__((swift_name("init(koin:scope:parameters:)"))) __attribute__((objc_designated_initializer));
@property (readonly) SOCKoin_coreKoin *koin __attribute__((swift_name("koin")));
@property (readonly) SOCKoin_coreParametersHolder * _Nullable parameters __attribute__((swift_name("parameters")));
@property (readonly) SOCKoin_coreScope *scope __attribute__((swift_name("scope")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreAtomicDesc")))
@interface SOCKotlinx_coroutines_coreAtomicDesc : SOCBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)completeOp:(SOCKotlinx_coroutines_coreAtomicOp<id> *)op failure:(id _Nullable)failure __attribute__((swift_name("complete(op:failure:)")));
- (id _Nullable)prepareOp:(SOCKotlinx_coroutines_coreAtomicOp<id> *)op __attribute__((swift_name("prepare(op:)")));
@property SOCKotlinx_coroutines_coreAtomicOp<id> *atomicOp __attribute__((swift_name("atomicOp")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreOpDescriptor")))
@interface SOCKotlinx_coroutines_coreOpDescriptor : SOCBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (BOOL)isEarlierThanThat:(SOCKotlinx_coroutines_coreOpDescriptor *)that __attribute__((swift_name("isEarlierThan(that:)")));
- (id _Nullable)performAffected:(id _Nullable)affected __attribute__((swift_name("perform(affected:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SOCKotlinx_coroutines_coreAtomicOp<id> * _Nullable atomicOp __attribute__((swift_name("atomicOp")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_coroutines_coreLockFreeLinkedListNode.PrepareOp")))
@interface SOCKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp : SOCKotlinx_coroutines_coreOpDescriptor
- (instancetype)initWithAffected:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)next desc:(SOCKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc *)desc __attribute__((swift_name("init(affected:next:desc:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)finishPrepare __attribute__((swift_name("finishPrepare()")));
- (id _Nullable)performAffected:(id _Nullable)affected __attribute__((swift_name("perform(affected:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) SOCKotlinx_coroutines_coreLockFreeLinkedListNode *affected __attribute__((swift_name("affected")));
@property (readonly) SOCKotlinx_coroutines_coreAtomicOp<id> *atomicOp __attribute__((swift_name("atomicOp")));
@property (readonly) SOCKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc *desc __attribute__((swift_name("desc")));
@property (readonly) SOCKotlinx_coroutines_coreLockFreeLinkedListNode *next __attribute__((swift_name("next")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsWeekDay.Companion")))
@interface SOCKtor_utilsWeekDayCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_utilsWeekDayCompanion *shared __attribute__((swift_name("shared")));
- (SOCKtor_utilsWeekDay *)fromOrdinal:(int32_t)ordinal __attribute__((swift_name("from(ordinal:)")));
- (SOCKtor_utilsWeekDay *)fromValue:(NSString *)value __attribute__((swift_name("from(value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsMonth.Companion")))
@interface SOCKtor_utilsMonthCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_utilsMonthCompanion *shared __attribute__((swift_name("shared")));
- (SOCKtor_utilsMonth *)fromOrdinal:(int32_t)ordinal __attribute__((swift_name("from(ordinal:)")));
- (SOCKtor_utilsMonth *)fromValue:(NSString *)value __attribute__((swift_name("from(value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpUrl")))
@interface SOCKtor_httpUrl : SOCBase
- (instancetype)initWithProtocol:(SOCKtor_httpURLProtocol *)protocol host:(NSString *)host specifiedPort:(int32_t)specifiedPort encodedPath:(NSString *)encodedPath parameters:(id<SOCKtor_httpParameters>)parameters fragment:(NSString *)fragment user:(NSString * _Nullable)user password:(NSString * _Nullable)password trailingQuery:(BOOL)trailingQuery __attribute__((swift_name("init(protocol:host:specifiedPort:encodedPath:parameters:fragment:user:password:trailingQuery:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCKtor_httpUrlCompanion *companion __attribute__((swift_name("companion")));
- (SOCKtor_httpURLProtocol *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (id<SOCKtor_httpParameters>)component5 __attribute__((swift_name("component5()")));
- (NSString *)component6 __attribute__((swift_name("component6()")));
- (NSString * _Nullable)component7 __attribute__((swift_name("component7()")));
- (NSString * _Nullable)component8 __attribute__((swift_name("component8()")));
- (BOOL)component9 __attribute__((swift_name("component9()")));
- (SOCKtor_httpUrl *)doCopyProtocol:(SOCKtor_httpURLProtocol *)protocol host:(NSString *)host specifiedPort:(int32_t)specifiedPort encodedPath:(NSString *)encodedPath parameters:(id<SOCKtor_httpParameters>)parameters fragment:(NSString *)fragment user:(NSString * _Nullable)user password:(NSString * _Nullable)password trailingQuery:(BOOL)trailingQuery __attribute__((swift_name("doCopy(protocol:host:specifiedPort:encodedPath:parameters:fragment:user:password:trailingQuery:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *encodedPath __attribute__((swift_name("encodedPath")));
@property (readonly) NSString *fragment __attribute__((swift_name("fragment")));
@property (readonly) NSString *host __attribute__((swift_name("host")));
@property (readonly) id<SOCKtor_httpParameters> parameters __attribute__((swift_name("parameters")));
@property (readonly) NSString * _Nullable password __attribute__((swift_name("password")));
@property (readonly) int32_t port __attribute__((swift_name("port")));
@property (readonly) SOCKtor_httpURLProtocol *protocol __attribute__((swift_name("protocol")));
@property (readonly) int32_t specifiedPort __attribute__((swift_name("specifiedPort")));
@property (readonly) BOOL trailingQuery __attribute__((swift_name("trailingQuery")));
@property (readonly) NSString * _Nullable user __attribute__((swift_name("user")));
@end;

__attribute__((swift_name("Ktor_httpOutgoingContent")))
@interface SOCKtor_httpOutgoingContent : SOCBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (id _Nullable)getPropertyKey:(SOCKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("getProperty(key:)")));
- (void)setPropertyKey:(SOCKtor_utilsAttributeKey<id> *)key value:(id _Nullable)value __attribute__((swift_name("setProperty(key:value:)")));
@property (readonly) SOCLong * _Nullable contentLength __attribute__((swift_name("contentLength")));
@property (readonly) SOCKtor_httpContentType * _Nullable contentType __attribute__((swift_name("contentType")));
@property (readonly) id<SOCKtor_httpHeaders> headers __attribute__((swift_name("headers")));
@property (readonly) SOCKtor_httpHttpStatusCode * _Nullable status __attribute__((swift_name("status")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpURLProtocol")))
@interface SOCKtor_httpURLProtocol : SOCBase
- (instancetype)initWithName:(NSString *)name defaultPort:(int32_t)defaultPort __attribute__((swift_name("init(name:defaultPort:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCKtor_httpURLProtocolCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (SOCKtor_httpURLProtocol *)doCopyName:(NSString *)name defaultPort:(int32_t)defaultPort __attribute__((swift_name("doCopy(name:defaultPort:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t defaultPort __attribute__((swift_name("defaultPort")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpParametersBuilder")))
@interface SOCKtor_httpParametersBuilder : SOCKtor_utilsStringValuesBuilder
- (instancetype)initWithSize:(int32_t)size urlEncodingOption:(SOCKtor_httpUrlEncodingOption *)urlEncodingOption __attribute__((swift_name("init(size:urlEncodingOption:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCaseInsensitiveName:(BOOL)caseInsensitiveName size:(int32_t)size __attribute__((swift_name("init(caseInsensitiveName:size:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (id<SOCKtor_httpParameters>)build __attribute__((swift_name("build()")));
@property SOCKtor_httpUrlEncodingOption *urlEncodingOption __attribute__((swift_name("urlEncodingOption")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpURLBuilder.Companion")))
@interface SOCKtor_httpURLBuilderCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_httpURLBuilderCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpMethod.Companion")))
@interface SOCKtor_httpHttpMethodCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_httpHttpMethodCompanion *shared __attribute__((swift_name("shared")));
- (SOCKtor_httpHttpMethod *)parseMethod:(NSString *)method __attribute__((swift_name("parse(method:)")));
@property (readonly) NSArray<SOCKtor_httpHttpMethod *> *DefaultMethods __attribute__((swift_name("DefaultMethods")));
@property (readonly) SOCKtor_httpHttpMethod *Delete __attribute__((swift_name("Delete")));
@property (readonly) SOCKtor_httpHttpMethod *Get __attribute__((swift_name("Get")));
@property (readonly) SOCKtor_httpHttpMethod *Head __attribute__((swift_name("Head")));
@property (readonly) SOCKtor_httpHttpMethod *Options __attribute__((swift_name("Options")));
@property (readonly) SOCKtor_httpHttpMethod *Patch __attribute__((swift_name("Patch")));
@property (readonly) SOCKtor_httpHttpMethod *Post __attribute__((swift_name("Post")));
@property (readonly) SOCKtor_httpHttpMethod *Put __attribute__((swift_name("Put")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioMemory")))
@interface SOCKtor_ioMemory : SOCBase
- (instancetype)initWithPointer:(void *)pointer size:(int64_t)size __attribute__((swift_name("init(pointer:size:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCKtor_ioMemoryCompanion *companion __attribute__((swift_name("companion")));
- (void)doCopyToDestination:(SOCKtor_ioMemory *)destination offset:(int32_t)offset length:(int32_t)length destinationOffset:(int32_t)destinationOffset __attribute__((swift_name("doCopyTo(destination:offset:length:destinationOffset:)")));
- (void)doCopyToDestination:(SOCKtor_ioMemory *)destination offset:(int64_t)offset length:(int64_t)length destinationOffset_:(int64_t)destinationOffset __attribute__((swift_name("doCopyTo(destination:offset:length:destinationOffset_:)")));
- (int8_t)loadAtIndex:(int32_t)index __attribute__((swift_name("loadAt(index:)")));
- (int8_t)loadAtIndex_:(int64_t)index __attribute__((swift_name("loadAt(index_:)")));
- (SOCKtor_ioMemory *)sliceOffset:(int32_t)offset length:(int32_t)length __attribute__((swift_name("slice(offset:length:)")));
- (SOCKtor_ioMemory *)sliceOffset:(int64_t)offset length_:(int64_t)length __attribute__((swift_name("slice(offset:length_:)")));
- (void)storeAtIndex:(int32_t)index value:(int8_t)value __attribute__((swift_name("storeAt(index:value:)")));
- (void)storeAtIndex:(int64_t)index value_:(int8_t)value __attribute__((swift_name("storeAt(index:value_:)")));
@property (readonly) void *pointer __attribute__((swift_name("pointer")));
@property (readonly) int64_t size __attribute__((swift_name("size")));
@property (readonly) int32_t size32 __attribute__((swift_name("size32")));
@end;

__attribute__((swift_name("Ktor_ioBuffer")))
@interface SOCKtor_ioBuffer : SOCBase
- (instancetype)initWithMemory:(SOCKtor_ioMemory *)memory __attribute__((swift_name("init(memory:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCKtor_ioBufferCompanion *companion __attribute__((swift_name("companion")));
- (void)commitWrittenCount:(int32_t)count __attribute__((swift_name("commitWritten(count:)")));
- (int32_t)discardCount:(int32_t)count __attribute__((swift_name("discard(count:)"))) __attribute__((unavailable("Use discardExact instead.")));
- (int64_t)discardCount_:(int64_t)count __attribute__((swift_name("discard(count_:)"))) __attribute__((unavailable("Use discardExact instead.")));
- (void)discardExactCount:(int32_t)count __attribute__((swift_name("discardExact(count:)")));
- (SOCKtor_ioBuffer *)duplicate __attribute__((swift_name("duplicate()")));
- (void)duplicateToCopy:(SOCKtor_ioBuffer *)copy __attribute__((swift_name("duplicateTo(copy:)")));
- (int8_t)readByte __attribute__((swift_name("readByte()")));
- (void)reserveEndGapEndGap:(int32_t)endGap __attribute__((swift_name("reserveEndGap(endGap:)")));
- (void)reserveStartGapStartGap:(int32_t)startGap __attribute__((swift_name("reserveStartGap(startGap:)")));
- (void)reset __attribute__((swift_name("reset()")));
- (void)resetForRead __attribute__((swift_name("resetForRead()")));
- (void)resetForWrite __attribute__((swift_name("resetForWrite()")));
- (void)resetForWriteLimit:(int32_t)limit __attribute__((swift_name("resetForWrite(limit:)")));
- (void)rewindCount:(int32_t)count __attribute__((swift_name("rewind(count:)")));
- (NSString *)description __attribute__((swift_name("description()")));
- (int32_t)tryPeekByte __attribute__((swift_name("tryPeekByte()")));
- (int32_t)tryReadByte __attribute__((swift_name("tryReadByte()")));
- (void)writeByteValue:(int8_t)value __attribute__((swift_name("writeByte(value:)")));
@property id _Nullable attachment __attribute__((swift_name("attachment"))) __attribute__((deprecated("Will be removed. Inherit Buffer and add required fields instead.")));
@property (readonly) int32_t capacity __attribute__((swift_name("capacity")));
@property (readonly) int32_t endGap __attribute__((swift_name("endGap")));
@property (readonly) int32_t limit __attribute__((swift_name("limit")));
@property (readonly) SOCKtor_ioMemory *memory __attribute__((swift_name("memory")));
@property (readonly) int32_t readPosition __attribute__((swift_name("readPosition")));
@property (readonly) int32_t readRemaining __attribute__((swift_name("readRemaining")));
@property (readonly) int32_t startGap __attribute__((swift_name("startGap")));
@property (readonly) int32_t writePosition __attribute__((swift_name("writePosition")));
@property (readonly) int32_t writeRemaining __attribute__((swift_name("writeRemaining")));
@end;

__attribute__((swift_name("Ktor_ioChunkBuffer")))
@interface SOCKtor_ioChunkBuffer : SOCKtor_ioBuffer
- (instancetype)initWithMemory:(SOCKtor_ioMemory *)memory origin:(SOCKtor_ioChunkBuffer * _Nullable)origin parentPool:(id<SOCKtor_ioObjectPool> _Nullable)parentPool __attribute__((swift_name("init(memory:origin:parentPool:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMemory:(SOCKtor_ioMemory *)memory __attribute__((swift_name("init(memory:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SOCKtor_ioChunkBufferCompanion *companion __attribute__((swift_name("companion")));
- (SOCKtor_ioChunkBuffer * _Nullable)cleanNext __attribute__((swift_name("cleanNext()")));
- (SOCKtor_ioChunkBuffer *)duplicate __attribute__((swift_name("duplicate()")));
- (void)releasePool:(id<SOCKtor_ioObjectPool>)pool __attribute__((swift_name("release(pool:)")));
- (void)reset __attribute__((swift_name("reset()")));
@property (getter=next_) SOCKtor_ioChunkBuffer * _Nullable next __attribute__((swift_name("next")));
@property (readonly) SOCKtor_ioChunkBuffer * _Nullable origin __attribute__((swift_name("origin")));
@property (readonly) int32_t referenceCount __attribute__((swift_name("referenceCount")));
@end;

__attribute__((swift_name("Ktor_ioInput")))
@protocol SOCKtor_ioInput <SOCKtor_ioCloseable>
@required
- (int64_t)discardN:(int64_t)n __attribute__((swift_name("discard(n:)")));
- (int64_t)peekToDestination:(SOCKtor_ioMemory *)destination destinationOffset:(int64_t)destinationOffset offset:(int64_t)offset min:(int64_t)min max:(int64_t)max __attribute__((swift_name("peekTo(destination:destinationOffset:offset:min:max:)")));
- (int8_t)readByte __attribute__((swift_name("readByte()")));
- (int32_t)tryPeek __attribute__((swift_name("tryPeek()")));
@property SOCKtor_ioByteOrder *byteOrder __attribute__((swift_name("byteOrder"))) __attribute__((unavailable("Not supported anymore. All operations are big endian by default. Use readXXXLittleEndian or readXXX then X.reverseByteOrder() instead.")));
@property (readonly) BOOL endOfInput __attribute__((swift_name("endOfInput")));
@end;

__attribute__((swift_name("KotlinAppendable")))
@protocol SOCKotlinAppendable
@required
- (id<SOCKotlinAppendable>)appendValue:(unichar)value __attribute__((swift_name("append(value:)")));
- (id<SOCKotlinAppendable>)appendValue_:(id _Nullable)value __attribute__((swift_name("append(value_:)")));
- (id<SOCKotlinAppendable>)appendValue:(id _Nullable)value startIndex:(int32_t)startIndex endIndex:(int32_t)endIndex __attribute__((swift_name("append(value:startIndex:endIndex:)")));
@end;

__attribute__((swift_name("Ktor_ioOutput")))
@protocol SOCKtor_ioOutput <SOCKotlinAppendable, SOCKtor_ioCloseable>
@required
- (id<SOCKotlinAppendable>)appendCsq:(SOCKotlinCharArray *)csq start:(int32_t)start end:(int32_t)end __attribute__((swift_name("append(csq:start:end:)")));
- (void)flush __attribute__((swift_name("flush()")));
- (void)writeByteV:(int8_t)v __attribute__((swift_name("writeByte(v:)")));
@property SOCKtor_ioByteOrder *byteOrder __attribute__((swift_name("byteOrder"))) __attribute__((deprecated("Write with writeXXXLittleEndian or do X.reverseByteOrder() and then writeXXX instead.")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioIoBuffer")))
@interface SOCKtor_ioIoBuffer : SOCKtor_ioChunkBuffer <SOCKtor_ioInput, SOCKtor_ioOutput>
- (instancetype)initWithMemory:(SOCKtor_ioMemory *)memory origin:(SOCKtor_ioChunkBuffer * _Nullable)origin __attribute__((swift_name("init(memory:origin:)"))) __attribute__((objc_designated_initializer)) __attribute__((deprecated("Use Buffer instead.")));
- (instancetype)initWithContent:(void *)content contentCapacity:(int32_t)contentCapacity __attribute__((swift_name("init(content:contentCapacity:)"))) __attribute__((objc_designated_initializer)) __attribute__((deprecated("Use Buffer instead.")));
- (instancetype)initWithMemory:(SOCKtor_ioMemory *)memory origin:(SOCKtor_ioChunkBuffer * _Nullable)origin parentPool:(id<SOCKtor_ioObjectPool> _Nullable)parentPool __attribute__((swift_name("init(memory:origin:parentPool:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SOCKtor_ioIoBufferCompanion *companion __attribute__((swift_name("companion")));
- (id<SOCKotlinAppendable>)appendValue:(unichar)c __attribute__((swift_name("append(value:)")));
- (id<SOCKotlinAppendable>)appendCsq:(SOCKotlinCharArray *)csq start:(int32_t)start end:(int32_t)end __attribute__((swift_name("append(csq:start:end:)")));
- (id<SOCKotlinAppendable>)appendValue_:(id _Nullable)csq __attribute__((swift_name("append(value_:)")));
- (id<SOCKotlinAppendable>)appendValue:(id _Nullable)csq startIndex:(int32_t)start endIndex:(int32_t)end __attribute__((swift_name("append(value:startIndex:endIndex:)")));
- (int32_t)appendCharsCsq:(SOCKotlinCharArray *)csq start:(int32_t)start end:(int32_t)end __attribute__((swift_name("appendChars(csq:start:end:)")));
- (int32_t)appendCharsCsq:(id)csq start:(int32_t)start end_:(int32_t)end __attribute__((swift_name("appendChars(csq:start:end_:)")));
- (void)close __attribute__((swift_name("close()")));
- (SOCKtor_ioIoBuffer *)duplicate __attribute__((swift_name("duplicate()")));
- (void)flush __attribute__((swift_name("flush()")));
- (SOCKtor_ioIoBuffer *)makeView __attribute__((swift_name("makeView()")));
- (int64_t)peekToDestination:(SOCKtor_ioMemory *)destination destinationOffset:(int64_t)destinationOffset offset:(int64_t)offset min:(int64_t)min max:(int64_t)max __attribute__((swift_name("peekTo(destination:destinationOffset:offset:min:max:)")));
- (int32_t)readDirectBlock:(SOCInt *(^)(id))block __attribute__((swift_name("readDirect(block:)")));
- (void)releasePool_:(id<SOCKtor_ioObjectPool>)pool __attribute__((swift_name("release(pool_:)")));
- (NSString *)description __attribute__((swift_name("description()")));
- (int32_t)tryPeek __attribute__((swift_name("tryPeek()")));
- (int32_t)writeDirectBlock:(SOCInt *(^)(id))block __attribute__((swift_name("writeDirect(block:)")));
@property SOCKtor_ioByteOrder *byteOrder __attribute__((swift_name("byteOrder"))) __attribute__((unavailable("Not supported anymore. All operations are big endian by default.")));
@property (readonly) BOOL endOfInput __attribute__((swift_name("endOfInput")));
@end;

__attribute__((swift_name("Ktor_ioAbstractInput")))
@interface SOCKtor_ioAbstractInput : SOCBase <SOCKtor_ioInput>
- (instancetype)initWithHead:(SOCKtor_ioChunkBuffer *)head remaining:(int64_t)remaining pool:(id<SOCKtor_ioObjectPool>)pool __attribute__((swift_name("init(head:remaining:pool:)"))) __attribute__((objc_designated_initializer)) __attribute__((deprecated("AbstractInput is deprecated and will be merged with Input in 2.0.0")));
@property (class, readonly, getter=companion) SOCKtor_ioAbstractInputCompanion *companion __attribute__((swift_name("companion")));
- (BOOL)canRead __attribute__((swift_name("canRead()")));
- (void)close __attribute__((swift_name("close()")));
- (void)closeSource __attribute__((swift_name("closeSource()")));
- (int32_t)discardN_:(int32_t)n __attribute__((swift_name("discard(n_:)")));
- (int64_t)discardN:(int64_t)n __attribute__((swift_name("discard(n:)")));
- (void)discardExactN:(int32_t)n __attribute__((swift_name("discardExact(n:)")));
- (SOCKtor_ioChunkBuffer * _Nullable)ensureNextHeadCurrent:(SOCKtor_ioChunkBuffer *)current __attribute__((swift_name("ensureNextHead(current:)")));
- (SOCKtor_ioChunkBuffer * _Nullable)fill __attribute__((swift_name("fill()")));
- (int32_t)fillDestination:(SOCKtor_ioMemory *)destination offset:(int32_t)offset length:(int32_t)length __attribute__((swift_name("fill(destination:offset:length:)")));
- (void)fixGapAfterReadCurrent:(SOCKtor_ioChunkBuffer *)current __attribute__((swift_name("fixGapAfterRead(current:)")));
- (BOOL)hasBytesN:(int32_t)n __attribute__((swift_name("hasBytes(n:)")));
- (void)markNoMoreChunksAvailable __attribute__((swift_name("markNoMoreChunksAvailable()")));
- (int64_t)peekToDestination:(SOCKtor_ioMemory *)destination destinationOffset:(int64_t)destinationOffset offset:(int64_t)offset min:(int64_t)min max:(int64_t)max __attribute__((swift_name("peekTo(destination:destinationOffset:offset:min:max:)")));
- (SOCKtor_ioChunkBuffer * _Nullable)prepareReadHeadMinSize:(int32_t)minSize __attribute__((swift_name("prepareReadHead(minSize:)")));
- (int8_t)readByte __attribute__((swift_name("readByte()")));
- (NSString *)readTextMin:(int32_t)min max:(int32_t)max __attribute__((swift_name("readText(min:max:)")));
- (int32_t)readTextOut:(id<SOCKotlinAppendable>)out min:(int32_t)min max:(int32_t)max __attribute__((swift_name("readText(out:min:max:)")));
- (NSString *)readTextExactExactCharacters:(int32_t)exactCharacters __attribute__((swift_name("readTextExact(exactCharacters:)")));
- (void)readTextExactOut:(id<SOCKotlinAppendable>)out exactCharacters:(int32_t)exactCharacters __attribute__((swift_name("readTextExact(out:exactCharacters:)")));
- (void)release_ __attribute__((swift_name("release()")));
- (int32_t)tryPeek __attribute__((swift_name("tryPeek()")));
- (void)updateHeadRemainingRemaining:(int32_t)remaining __attribute__((swift_name("updateHeadRemaining(remaining:)"))) __attribute__((unavailable("Not supported anymore.")));
@property SOCKtor_ioByteOrder *byteOrder __attribute__((swift_name("byteOrder"))) __attribute__((unavailable("Not supported anymore. All operations are big endian by default.")));
@property (readonly) BOOL endOfInput __attribute__((swift_name("endOfInput")));
@property (readonly) id<SOCKtor_ioObjectPool> pool __attribute__((swift_name("pool")));
@property (readonly) int64_t remaining __attribute__((swift_name("remaining")));
@end;

__attribute__((swift_name("Ktor_ioByteReadPacketBase")))
@interface SOCKtor_ioByteReadPacketBase : SOCKtor_ioAbstractInput
- (instancetype)initWithHead:(SOCKtor_ioChunkBuffer *)head remaining:(int64_t)remaining pool:(id<SOCKtor_ioObjectPool>)pool __attribute__((swift_name("init(head:remaining:pool:)"))) __attribute__((objc_designated_initializer)) __attribute__((deprecated("Will be removed in the future releases. Use Input or AbstractInput instead.")));
@property (class, readonly, getter=companion) SOCKtor_ioByteReadPacketBaseCompanion *companion __attribute__((swift_name("companion")));
@end;

__attribute__((swift_name("Ktor_ioByteReadPacketPlatformBase")))
@interface SOCKtor_ioByteReadPacketPlatformBase : SOCKtor_ioByteReadPacketBase
- (instancetype)initWithHead:(SOCKtor_ioChunkBuffer *)head remaining:(int64_t)remaining pool:(id<SOCKtor_ioObjectPool>)pool __attribute__((swift_name("init(head:remaining:pool:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable("Will be removed in future releases.")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioByteReadPacket")))
@interface SOCKtor_ioByteReadPacket : SOCKtor_ioByteReadPacketPlatformBase <SOCKtor_ioInput>
- (instancetype)initWithHead:(SOCKtor_ioChunkBuffer *)head pool:(id<SOCKtor_ioObjectPool>)pool __attribute__((swift_name("init(head:pool:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithHead:(SOCKtor_ioChunkBuffer *)head remaining:(int64_t)remaining pool:(id<SOCKtor_ioObjectPool>)pool __attribute__((swift_name("init(head:remaining:pool:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SOCKtor_ioByteReadPacketCompanion *companion __attribute__((swift_name("companion")));
- (void)closeSource __attribute__((swift_name("closeSource()")));
- (SOCKtor_ioByteReadPacket *)doCopy __attribute__((swift_name("doCopy()")));
- (SOCKtor_ioChunkBuffer * _Nullable)fill __attribute__((swift_name("fill()")));
- (int32_t)fillDestination:(SOCKtor_ioMemory *)destination offset:(int32_t)offset length:(int32_t)length __attribute__((swift_name("fill(destination:offset:length:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("Ktor_ioReadSession")))
@protocol SOCKtor_ioReadSession
@required
- (int32_t)discardN_:(int32_t)n __attribute__((swift_name("discard(n_:)")));
- (SOCKtor_ioIoBuffer * _Nullable)requestAtLeast:(int32_t)atLeast __attribute__((swift_name("request(atLeast:)")));
@property (readonly) int32_t availableForRead __attribute__((swift_name("availableForRead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioByteOrder")))
@interface SOCKtor_ioByteOrder : SOCKotlinEnum<SOCKtor_ioByteOrder *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SOCKtor_ioByteOrderCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) SOCKtor_ioByteOrder *bigEndian __attribute__((swift_name("bigEndian")));
@property (class, readonly) SOCKtor_ioByteOrder *littleEndian __attribute__((swift_name("littleEndian")));
+ (SOCKotlinArray<SOCKtor_ioByteOrder *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpStatusCode.Companion")))
@interface SOCKtor_httpHttpStatusCodeCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_httpHttpStatusCodeCompanion *shared __attribute__((swift_name("shared")));
- (SOCKtor_httpHttpStatusCode *)fromValueValue:(int32_t)value __attribute__((swift_name("fromValue(value:)")));
@property (readonly) SOCKtor_httpHttpStatusCode *Accepted __attribute__((swift_name("Accepted")));
@property (readonly) SOCKtor_httpHttpStatusCode *BadGateway __attribute__((swift_name("BadGateway")));
@property (readonly) SOCKtor_httpHttpStatusCode *BadRequest __attribute__((swift_name("BadRequest")));
@property (readonly) SOCKtor_httpHttpStatusCode *Conflict __attribute__((swift_name("Conflict")));
@property (readonly) SOCKtor_httpHttpStatusCode *Continue __attribute__((swift_name("Continue")));
@property (readonly) SOCKtor_httpHttpStatusCode *Created __attribute__((swift_name("Created")));
@property (readonly) SOCKtor_httpHttpStatusCode *ExpectationFailed __attribute__((swift_name("ExpectationFailed")));
@property (readonly) SOCKtor_httpHttpStatusCode *FailedDependency __attribute__((swift_name("FailedDependency")));
@property (readonly) SOCKtor_httpHttpStatusCode *Forbidden __attribute__((swift_name("Forbidden")));
@property (readonly) SOCKtor_httpHttpStatusCode *Found __attribute__((swift_name("Found")));
@property (readonly) SOCKtor_httpHttpStatusCode *GatewayTimeout __attribute__((swift_name("GatewayTimeout")));
@property (readonly) SOCKtor_httpHttpStatusCode *Gone __attribute__((swift_name("Gone")));
@property (readonly) SOCKtor_httpHttpStatusCode *InsufficientStorage __attribute__((swift_name("InsufficientStorage")));
@property (readonly) SOCKtor_httpHttpStatusCode *InternalServerError __attribute__((swift_name("InternalServerError")));
@property (readonly) SOCKtor_httpHttpStatusCode *LengthRequired __attribute__((swift_name("LengthRequired")));
@property (readonly) SOCKtor_httpHttpStatusCode *Locked __attribute__((swift_name("Locked")));
@property (readonly) SOCKtor_httpHttpStatusCode *MethodNotAllowed __attribute__((swift_name("MethodNotAllowed")));
@property (readonly) SOCKtor_httpHttpStatusCode *MovedPermanently __attribute__((swift_name("MovedPermanently")));
@property (readonly) SOCKtor_httpHttpStatusCode *MultiStatus __attribute__((swift_name("MultiStatus")));
@property (readonly) SOCKtor_httpHttpStatusCode *MultipleChoices __attribute__((swift_name("MultipleChoices")));
@property (readonly) SOCKtor_httpHttpStatusCode *NoContent __attribute__((swift_name("NoContent")));
@property (readonly) SOCKtor_httpHttpStatusCode *NonAuthoritativeInformation __attribute__((swift_name("NonAuthoritativeInformation")));
@property (readonly) SOCKtor_httpHttpStatusCode *NotAcceptable __attribute__((swift_name("NotAcceptable")));
@property (readonly) SOCKtor_httpHttpStatusCode *NotFound __attribute__((swift_name("NotFound")));
@property (readonly) SOCKtor_httpHttpStatusCode *NotImplemented __attribute__((swift_name("NotImplemented")));
@property (readonly) SOCKtor_httpHttpStatusCode *NotModified __attribute__((swift_name("NotModified")));
@property (readonly) SOCKtor_httpHttpStatusCode *OK __attribute__((swift_name("OK")));
@property (readonly) SOCKtor_httpHttpStatusCode *PartialContent __attribute__((swift_name("PartialContent")));
@property (readonly) SOCKtor_httpHttpStatusCode *PayloadTooLarge __attribute__((swift_name("PayloadTooLarge")));
@property (readonly) SOCKtor_httpHttpStatusCode *PaymentRequired __attribute__((swift_name("PaymentRequired")));
@property (readonly) SOCKtor_httpHttpStatusCode *PermanentRedirect __attribute__((swift_name("PermanentRedirect")));
@property (readonly) SOCKtor_httpHttpStatusCode *PreconditionFailed __attribute__((swift_name("PreconditionFailed")));
@property (readonly) SOCKtor_httpHttpStatusCode *Processing __attribute__((swift_name("Processing")));
@property (readonly) SOCKtor_httpHttpStatusCode *ProxyAuthenticationRequired __attribute__((swift_name("ProxyAuthenticationRequired")));
@property (readonly) SOCKtor_httpHttpStatusCode *RequestHeaderFieldTooLarge __attribute__((swift_name("RequestHeaderFieldTooLarge")));
@property (readonly) SOCKtor_httpHttpStatusCode *RequestTimeout __attribute__((swift_name("RequestTimeout")));
@property (readonly) SOCKtor_httpHttpStatusCode *RequestURITooLong __attribute__((swift_name("RequestURITooLong")));
@property (readonly) SOCKtor_httpHttpStatusCode *RequestedRangeNotSatisfiable __attribute__((swift_name("RequestedRangeNotSatisfiable")));
@property (readonly) SOCKtor_httpHttpStatusCode *ResetContent __attribute__((swift_name("ResetContent")));
@property (readonly) SOCKtor_httpHttpStatusCode *SeeOther __attribute__((swift_name("SeeOther")));
@property (readonly) SOCKtor_httpHttpStatusCode *ServiceUnavailable __attribute__((swift_name("ServiceUnavailable")));
@property (readonly) SOCKtor_httpHttpStatusCode *SwitchProxy __attribute__((swift_name("SwitchProxy")));
@property (readonly) SOCKtor_httpHttpStatusCode *SwitchingProtocols __attribute__((swift_name("SwitchingProtocols")));
@property (readonly) SOCKtor_httpHttpStatusCode *TemporaryRedirect __attribute__((swift_name("TemporaryRedirect")));
@property (readonly) SOCKtor_httpHttpStatusCode *TooManyRequests __attribute__((swift_name("TooManyRequests")));
@property (readonly) SOCKtor_httpHttpStatusCode *Unauthorized __attribute__((swift_name("Unauthorized")));
@property (readonly) SOCKtor_httpHttpStatusCode *UnprocessableEntity __attribute__((swift_name("UnprocessableEntity")));
@property (readonly) SOCKtor_httpHttpStatusCode *UnsupportedMediaType __attribute__((swift_name("UnsupportedMediaType")));
@property (readonly) SOCKtor_httpHttpStatusCode *UpgradeRequired __attribute__((swift_name("UpgradeRequired")));
@property (readonly) SOCKtor_httpHttpStatusCode *UseProxy __attribute__((swift_name("UseProxy")));
@property (readonly) SOCKtor_httpHttpStatusCode *VariantAlsoNegotiates __attribute__((swift_name("VariantAlsoNegotiates")));
@property (readonly) SOCKtor_httpHttpStatusCode *VersionNotSupported __attribute__((swift_name("VersionNotSupported")));
@property (readonly) NSArray<SOCKtor_httpHttpStatusCode *> *allStatusCodes __attribute__((swift_name("allStatusCodes")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpProtocolVersion.Companion")))
@interface SOCKtor_httpHttpProtocolVersionCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_httpHttpProtocolVersionCompanion *shared __attribute__((swift_name("shared")));
- (SOCKtor_httpHttpProtocolVersion *)fromValueName:(NSString *)name major:(int32_t)major minor:(int32_t)minor __attribute__((swift_name("fromValue(name:major:minor:)")));
- (SOCKtor_httpHttpProtocolVersion *)parseValue:(id)value __attribute__((swift_name("parse(value:)")));
@property (readonly) SOCKtor_httpHttpProtocolVersion *HTTP_1_0 __attribute__((swift_name("HTTP_1_0")));
@property (readonly) SOCKtor_httpHttpProtocolVersion *HTTP_1_1 __attribute__((swift_name("HTTP_1_1")));
@property (readonly) SOCKtor_httpHttpProtocolVersion *HTTP_2_0 __attribute__((swift_name("HTTP_2_0")));
@property (readonly) SOCKtor_httpHttpProtocolVersion *QUIC __attribute__((swift_name("QUIC")));
@property (readonly) SOCKtor_httpHttpProtocolVersion *SPDY_3 __attribute__((swift_name("SPDY_3")));
@end;

__attribute__((swift_name("KotlinKType")))
@protocol SOCKotlinKType
@required
@property (readonly) NSArray<SOCKotlinKTypeProjection *> *arguments __attribute__((swift_name("arguments")));
@property (readonly) id<SOCKotlinKClassifier> _Nullable classifier __attribute__((swift_name("classifier")));
@property (readonly) BOOL isMarkedNullable __attribute__((swift_name("isMarkedNullable")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreKind")))
@interface SOCKoin_coreKind : SOCKotlinEnum<SOCKoin_coreKind *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) SOCKoin_coreKind *singleton __attribute__((swift_name("singleton")));
@property (class, readonly) SOCKoin_coreKind *factory __attribute__((swift_name("factory")));
@property (class, readonly) SOCKoin_coreKind *scoped __attribute__((swift_name("scoped")));
+ (SOCKotlinArray<SOCKoin_coreKind *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Koin_coreCallbacks")))
@interface SOCKoin_coreCallbacks<T> : SOCBase
- (instancetype)initWithOnClose:(void (^ _Nullable)(T _Nullable))onClose __attribute__((swift_name("init(onClose:)"))) __attribute__((objc_designated_initializer));
- (void (^ _Nullable)(T _Nullable))component1 __attribute__((swift_name("component1()")));
- (SOCKoin_coreCallbacks<T> *)doCopyOnClose:(void (^ _Nullable)(T _Nullable))onClose __attribute__((swift_name("doCopy(onClose:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) void (^ _Nullable onClose)(T _Nullable) __attribute__((swift_name("onClose")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreAtomicOp")))
@interface SOCKotlinx_coroutines_coreAtomicOp<__contravariant T> : SOCKotlinx_coroutines_coreOpDescriptor
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)completeAffected:(T _Nullable)affected failure:(id _Nullable)failure __attribute__((swift_name("complete(affected:failure:)")));
- (id _Nullable)decideDecision:(id _Nullable)decision __attribute__((swift_name("decide(decision:)")));
- (id _Nullable)performAffected:(id _Nullable)affected __attribute__((swift_name("perform(affected:)")));
- (id _Nullable)prepareAffected:(T _Nullable)affected __attribute__((swift_name("prepare(affected:)")));
@property (readonly) SOCKotlinx_coroutines_coreAtomicOp<id> *atomicOp __attribute__((swift_name("atomicOp")));
@property (readonly) id _Nullable consensus __attribute__((swift_name("consensus")));
@property (readonly) BOOL isDecided __attribute__((swift_name("isDecided")));
@property (readonly) int64_t opSequence __attribute__((swift_name("opSequence")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreLockFreeLinkedListNode")))
@interface SOCKotlinx_coroutines_coreLockFreeLinkedListNode : SOCBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)addLastNode:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)node __attribute__((swift_name("addLast(node:)")));
- (BOOL)addLastIfNode:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)node condition:(SOCBoolean *(^)(void))condition __attribute__((swift_name("addLastIf(node:condition:)")));
- (BOOL)addLastIfPrevNode:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)node predicate:(SOCBoolean *(^)(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *))predicate __attribute__((swift_name("addLastIfPrev(node:predicate:)")));
- (BOOL)addLastIfPrevAndIfNode:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)node predicate:(SOCBoolean *(^)(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *))predicate condition:(SOCBoolean *(^)(void))condition __attribute__((swift_name("addLastIfPrevAndIf(node:predicate:condition:)")));
- (BOOL)addOneIfEmptyNode:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)node __attribute__((swift_name("addOneIfEmpty(node:)")));
- (SOCKotlinx_coroutines_coreLockFreeLinkedListNodeAddLastDesc<SOCKotlinx_coroutines_coreLockFreeLinkedListNode *> *)describeAddLastNode:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)node __attribute__((swift_name("describeAddLast(node:)")));
- (SOCKotlinx_coroutines_coreLockFreeLinkedListNodeRemoveFirstDesc<SOCKotlinx_coroutines_coreLockFreeLinkedListNode *> *)describeRemoveFirst __attribute__((swift_name("describeRemoveFirst()")));
- (void)helpRemove __attribute__((swift_name("helpRemove()")));
- (SOCKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)nextIfRemoved __attribute__((swift_name("nextIfRemoved()")));
- (BOOL)remove __attribute__((swift_name("remove()")));
- (id _Nullable)removeFirstIfIsInstanceOfOrPeekIfPredicate:(SOCBoolean *(^)(id _Nullable))predicate __attribute__((swift_name("removeFirstIfIsInstanceOfOrPeekIf(predicate:)")));
- (SOCKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)removeFirstOrNull __attribute__((swift_name("removeFirstOrNull()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isRemoved __attribute__((swift_name("isRemoved")));
@property (readonly, getter=next_) id _Nullable next __attribute__((swift_name("next")));
@property (readonly) SOCKotlinx_coroutines_coreLockFreeLinkedListNode *nextNode __attribute__((swift_name("nextNode")));
@property (readonly) SOCKotlinx_coroutines_coreLockFreeLinkedListNode *prevNode __attribute__((swift_name("prevNode")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreLockFreeLinkedListNode.AbstractAtomicDesc")))
@interface SOCKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc : SOCKotlinx_coroutines_coreAtomicDesc
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)completeOp:(SOCKotlinx_coroutines_coreAtomicOp<id> *)op failure:(id _Nullable)failure __attribute__((swift_name("complete(op:failure:)")));
- (id _Nullable)failureAffected:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)affected __attribute__((swift_name("failure(affected:)")));
- (void)finishOnSuccessAffected:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("finishOnSuccess(affected:next:)")));
- (void)finishPreparePrepareOp:(SOCKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp *)prepareOp __attribute__((swift_name("finishPrepare(prepareOp:)")));
- (id _Nullable)onPreparePrepareOp:(SOCKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp *)prepareOp __attribute__((swift_name("onPrepare(prepareOp:)")));
- (void)onRemovedAffected:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)affected __attribute__((swift_name("onRemoved(affected:)")));
- (id _Nullable)prepareOp:(SOCKotlinx_coroutines_coreAtomicOp<id> *)op __attribute__((swift_name("prepare(op:)")));
- (BOOL)retryAffected:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(id)next __attribute__((swift_name("retry(affected:next:)")));
- (SOCKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)takeAffectedNodeOp:(SOCKotlinx_coroutines_coreOpDescriptor *)op __attribute__((swift_name("takeAffectedNode(op:)")));
- (id)updatedNextAffected:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("updatedNext(affected:next:)")));
@property (readonly) SOCKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable affectedNode __attribute__((swift_name("affectedNode")));
@property (readonly) SOCKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable originalNext __attribute__((swift_name("originalNext")));
@end;

__attribute__((swift_name("Ktor_httpParameters")))
@protocol SOCKtor_httpParameters <SOCKtor_utilsStringValues>
@required
@property (readonly) SOCKtor_httpUrlEncodingOption *urlEncodingOption __attribute__((swift_name("urlEncodingOption")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpUrl.Companion")))
@interface SOCKtor_httpUrlCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_httpUrlCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((swift_name("Ktor_httpHeaderValueWithParameters")))
@interface SOCKtor_httpHeaderValueWithParameters : SOCBase
- (instancetype)initWithContent:(NSString *)content parameters:(NSArray<SOCKtor_httpHeaderValueParam *> *)parameters __attribute__((swift_name("init(content:parameters:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCKtor_httpHeaderValueWithParametersCompanion *companion __attribute__((swift_name("companion")));
- (NSString * _Nullable)parameterName:(NSString *)name __attribute__((swift_name("parameter(name:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *content __attribute__((swift_name("content")));
@property (readonly) NSArray<SOCKtor_httpHeaderValueParam *> *parameters __attribute__((swift_name("parameters")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpContentType")))
@interface SOCKtor_httpContentType : SOCKtor_httpHeaderValueWithParameters
- (instancetype)initWithContentType:(NSString *)contentType contentSubtype:(NSString *)contentSubtype parameters:(NSArray<SOCKtor_httpHeaderValueParam *> *)parameters __attribute__((swift_name("init(contentType:contentSubtype:parameters:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithContent:(NSString *)content parameters:(NSArray<SOCKtor_httpHeaderValueParam *> *)parameters __attribute__((swift_name("init(content:parameters:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) SOCKtor_httpContentTypeCompanion *companion __attribute__((swift_name("companion")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (BOOL)matchPattern:(SOCKtor_httpContentType *)pattern __attribute__((swift_name("match(pattern:)")));
- (BOOL)matchPattern_:(NSString *)pattern __attribute__((swift_name("match(pattern_:)")));
- (SOCKtor_httpContentType *)withParameterName:(NSString *)name value:(NSString *)value __attribute__((swift_name("withParameter(name:value:)")));
- (SOCKtor_httpContentType *)withoutParameters __attribute__((swift_name("withoutParameters()")));
@property (readonly) NSString *contentSubtype __attribute__((swift_name("contentSubtype")));
@property (readonly) NSString *contentType __attribute__((swift_name("contentType")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpURLProtocol.Companion")))
@interface SOCKtor_httpURLProtocolCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_httpURLProtocolCompanion *shared __attribute__((swift_name("shared")));
- (SOCKtor_httpURLProtocol *)createOrDefaultName:(NSString *)name __attribute__((swift_name("createOrDefault(name:)")));
@property (readonly) SOCKtor_httpURLProtocol *HTTP __attribute__((swift_name("HTTP")));
@property (readonly) SOCKtor_httpURLProtocol *HTTPS __attribute__((swift_name("HTTPS")));
@property (readonly) SOCKtor_httpURLProtocol *SOCKS __attribute__((swift_name("SOCKS")));
@property (readonly) SOCKtor_httpURLProtocol *WS __attribute__((swift_name("WS")));
@property (readonly) SOCKtor_httpURLProtocol *WSS __attribute__((swift_name("WSS")));
@property (readonly) NSDictionary<NSString *, SOCKtor_httpURLProtocol *> *byName __attribute__((swift_name("byName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpUrlEncodingOption")))
@interface SOCKtor_httpUrlEncodingOption : SOCKotlinEnum<SOCKtor_httpUrlEncodingOption *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) SOCKtor_httpUrlEncodingOption *default_ __attribute__((swift_name("default_")));
@property (class, readonly) SOCKtor_httpUrlEncodingOption *keyOnly __attribute__((swift_name("keyOnly")));
@property (class, readonly) SOCKtor_httpUrlEncodingOption *valueOnly __attribute__((swift_name("valueOnly")));
@property (class, readonly) SOCKtor_httpUrlEncodingOption *noEncoding __attribute__((swift_name("noEncoding")));
+ (SOCKotlinArray<SOCKtor_httpUrlEncodingOption *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioMemory.Companion")))
@interface SOCKtor_ioMemoryCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_ioMemoryCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) SOCKtor_ioMemory *Empty __attribute__((swift_name("Empty")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioBuffer.Companion")))
@interface SOCKtor_ioBufferCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_ioBufferCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) SOCKtor_ioBuffer *Empty __attribute__((swift_name("Empty")));
@property (readonly) int32_t ReservedSize __attribute__((swift_name("ReservedSize")));
@end;

__attribute__((swift_name("Ktor_ioObjectPool")))
@protocol SOCKtor_ioObjectPool <SOCKtor_ioCloseable>
@required
- (id)borrow __attribute__((swift_name("borrow()")));
- (void)dispose __attribute__((swift_name("dispose()")));
- (void)recycleInstance:(id)instance __attribute__((swift_name("recycle(instance:)")));
@property (readonly) int32_t capacity __attribute__((swift_name("capacity")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioChunkBuffer.Companion")))
@interface SOCKtor_ioChunkBufferCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_ioChunkBufferCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) SOCKtor_ioChunkBuffer *Empty __attribute__((swift_name("Empty")));
@property (readonly) id<SOCKtor_ioObjectPool> EmptyPool __attribute__((swift_name("EmptyPool")));
@property (readonly) id<SOCKtor_ioObjectPool> Pool __attribute__((swift_name("Pool")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinCharArray")))
@interface SOCKotlinCharArray : SOCBase
+ (instancetype)arrayWithSize:(int32_t)size __attribute__((swift_name("init(size:)")));
+ (instancetype)arrayWithSize:(int32_t)size init:(id (^)(SOCInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (unichar)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (SOCKotlinCharIterator *)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(unichar)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioIoBuffer.Companion")))
@interface SOCKtor_ioIoBufferCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_ioIoBufferCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) SOCKtor_ioIoBuffer *Empty __attribute__((swift_name("Empty")));
@property (readonly) id<SOCKtor_ioObjectPool> EmptyPool __attribute__((swift_name("EmptyPool")));
@property (readonly) id<SOCKtor_ioObjectPool> NoPool __attribute__((swift_name("NoPool")));
@property (readonly) id<SOCKtor_ioObjectPool> Pool __attribute__((swift_name("Pool")));
@property (readonly) int32_t ReservedSize __attribute__((swift_name("ReservedSize")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioAbstractInput.Companion")))
@interface SOCKtor_ioAbstractInputCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_ioAbstractInputCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioByteReadPacketBase.Companion")))
@interface SOCKtor_ioByteReadPacketBaseCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_ioByteReadPacketBaseCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) SOCKtor_ioByteReadPacket *Empty __attribute__((swift_name("Empty"))) __attribute__((unavailable("Use ByteReadPacket.Empty instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioByteReadPacket.Companion")))
@interface SOCKtor_ioByteReadPacketCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_ioByteReadPacketCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) SOCKtor_ioByteReadPacket *Empty __attribute__((swift_name("Empty")));
@property (readonly) int32_t ReservedSize __attribute__((swift_name("ReservedSize")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioByteOrder.Companion")))
@interface SOCKtor_ioByteOrderCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_ioByteOrderCompanion *shared __attribute__((swift_name("shared")));
- (SOCKtor_ioByteOrder *)nativeOrder __attribute__((swift_name("nativeOrder()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinKTypeProjection")))
@interface SOCKotlinKTypeProjection : SOCBase
- (instancetype)initWithVariance:(SOCKotlinKVariance * _Nullable)variance type:(id<SOCKotlinKType> _Nullable)type __attribute__((swift_name("init(variance:type:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) SOCKotlinKTypeProjectionCompanion *companion __attribute__((swift_name("companion")));
- (SOCKotlinKVariance * _Nullable)component1 __attribute__((swift_name("component1()")));
- (id<SOCKotlinKType> _Nullable)component2 __attribute__((swift_name("component2()")));
- (SOCKotlinKTypeProjection *)doCopyVariance:(SOCKotlinKVariance * _Nullable)variance type:(id<SOCKotlinKType> _Nullable)type __attribute__((swift_name("doCopy(variance:type:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<SOCKotlinKType> _Nullable type __attribute__((swift_name("type")));
@property (readonly) SOCKotlinKVariance * _Nullable variance __attribute__((swift_name("variance")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreLockFreeLinkedListNodeAddLastDesc")))
@interface SOCKotlinx_coroutines_coreLockFreeLinkedListNodeAddLastDesc<T> : SOCKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc
- (instancetype)initWithQueue:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)queue node:(T)node __attribute__((swift_name("init(queue:node:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)finishOnSuccessAffected:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("finishOnSuccess(affected:next:)")));
- (void)finishPreparePrepareOp:(SOCKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp *)prepareOp __attribute__((swift_name("finishPrepare(prepareOp:)")));
- (BOOL)retryAffected:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(id)next __attribute__((swift_name("retry(affected:next:)")));
- (SOCKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)takeAffectedNodeOp:(SOCKotlinx_coroutines_coreOpDescriptor *)op __attribute__((swift_name("takeAffectedNode(op:)")));
- (id)updatedNextAffected:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("updatedNext(affected:next:)")));
@property (readonly) SOCKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable affectedNode __attribute__((swift_name("affectedNode")));
@property (readonly) T node __attribute__((swift_name("node")));
@property (readonly) SOCKotlinx_coroutines_coreLockFreeLinkedListNode *originalNext __attribute__((swift_name("originalNext")));
@property (readonly) SOCKotlinx_coroutines_coreLockFreeLinkedListNode *queue __attribute__((swift_name("queue")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreLockFreeLinkedListNodeRemoveFirstDesc")))
@interface SOCKotlinx_coroutines_coreLockFreeLinkedListNodeRemoveFirstDesc<T> : SOCKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc
- (instancetype)initWithQueue:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)queue __attribute__((swift_name("init(queue:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (id _Nullable)failureAffected:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)affected __attribute__((swift_name("failure(affected:)")));
- (void)finishOnSuccessAffected:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("finishOnSuccess(affected:next:)")));
- (void)finishPreparePrepareOp:(SOCKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp *)prepareOp __attribute__((swift_name("finishPrepare(prepareOp:)")));
- (BOOL)retryAffected:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(id)next __attribute__((swift_name("retry(affected:next:)")));
- (SOCKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)takeAffectedNodeOp:(SOCKotlinx_coroutines_coreOpDescriptor *)op __attribute__((swift_name("takeAffectedNode(op:)")));
- (id)updatedNextAffected:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(SOCKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("updatedNext(affected:next:)")));
@property (readonly) SOCKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable affectedNode __attribute__((swift_name("affectedNode")));
@property (readonly) SOCKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable originalNext __attribute__((swift_name("originalNext")));
@property (readonly) SOCKotlinx_coroutines_coreLockFreeLinkedListNode *queue __attribute__((swift_name("queue")));
@property (readonly) T _Nullable result __attribute__((swift_name("result")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHeaderValueParam")))
@interface SOCKtor_httpHeaderValueParam : SOCBase
- (instancetype)initWithName:(NSString *)name value:(NSString *)value __attribute__((swift_name("init(name:value:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (SOCKtor_httpHeaderValueParam *)doCopyName:(NSString *)name value:(NSString *)value __attribute__((swift_name("doCopy(name:value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHeaderValueWithParameters.Companion")))
@interface SOCKtor_httpHeaderValueWithParametersCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_httpHeaderValueWithParametersCompanion *shared __attribute__((swift_name("shared")));
- (id _Nullable)parseValue:(NSString *)value init:(id _Nullable (^)(NSString *, NSArray<SOCKtor_httpHeaderValueParam *> *))init __attribute__((swift_name("parse(value:init:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpContentType.Companion")))
@interface SOCKtor_httpContentTypeCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKtor_httpContentTypeCompanion *shared __attribute__((swift_name("shared")));
- (SOCKtor_httpContentType *)parseValue:(NSString *)value __attribute__((swift_name("parse(value:)")));
@property (readonly) SOCKtor_httpContentType *Any __attribute__((swift_name("Any")));
@end;

__attribute__((swift_name("KotlinCharIterator")))
@interface SOCKotlinCharIterator : SOCBase <SOCKotlinIterator>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (id)next __attribute__((swift_name("next()")));
- (unichar)nextChar __attribute__((swift_name("nextChar()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinKVariance")))
@interface SOCKotlinKVariance : SOCKotlinEnum<SOCKotlinKVariance *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) SOCKotlinKVariance *invariant __attribute__((swift_name("invariant")));
@property (class, readonly) SOCKotlinKVariance *in __attribute__((swift_name("in")));
@property (class, readonly) SOCKotlinKVariance *out __attribute__((swift_name("out")));
+ (SOCKotlinArray<SOCKotlinKVariance *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinKTypeProjection.Companion")))
@interface SOCKotlinKTypeProjectionCompanion : SOCBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) SOCKotlinKTypeProjectionCompanion *shared __attribute__((swift_name("shared")));
- (SOCKotlinKTypeProjection *)contravariantType:(id<SOCKotlinKType>)type __attribute__((swift_name("contravariant(type:)")));
- (SOCKotlinKTypeProjection *)covariantType:(id<SOCKotlinKType>)type __attribute__((swift_name("covariant(type:)")));
- (SOCKotlinKTypeProjection *)invariantType:(id<SOCKotlinKType>)type __attribute__((swift_name("invariant(type:)")));
@property (readonly) SOCKotlinKTypeProjection *STAR __attribute__((swift_name("STAR")));
@end;

#pragma pop_macro("_Nullable_result")
#pragma clang diagnostic pop
NS_ASSUME_NONNULL_END
